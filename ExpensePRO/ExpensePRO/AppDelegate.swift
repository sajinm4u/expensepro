//
//  AppDelegate.swift
//  ExpensePRO
//
//  Created by Sajin M on 5/16/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SDWebImageSVGCoder
import OneSignal
import GoogleSignIn


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
         IQKeyboardManager.shared.enable = true
        let SVGCoder = SDImageSVGCoder.shared
        SDImageCodersManager.shared.addCoder(SVGCoder)
        Switcher.updateRootVC()
        GIDSignIn.sharedInstance().clientID = googleUserKey
        
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

          //START OneSignal initialization code
        //  let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
          
          // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
//          OneSignal.initWithLaunchOptions(launchOptions,
//            appId: oneSignalId,
//            handleNotificationAction: nil,
 //           settings: onesignalInitSettings)
        
        OneSignal.initWithLaunchOptions(launchOptions)
        OneSignal.setAppId(oneSignalId)
        
        

        //  OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

          // promptForPushNotifications will show the native iOS notification permission prompt.
          // We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 8)
          OneSignal.promptForPushNotifications(userResponse: { accepted in
              
              if let deviceState = OneSignal.getDeviceState() {
                  
                // print(deviceState.pushToken)
                  print(deviceState.userId)
                  Defaults.set(deviceState.userId, forKey:"deviceToken")
              }
              
           // print("User accepted notifications: \(accepted)")
          })
        
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
           let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02x", $1)})
       // Defaults.set(deviceTokenString, forKey:"deviceToken")
       }


    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

