//
//  CategoriesListViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 09/10/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import TextFieldEffects
import Alertift

class CategoriesListViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    
    @IBOutlet weak var categoryColletionView: UICollectionView!
    @IBOutlet weak var iconCollectionView: UICollectionView!
    @IBOutlet weak var txtCategoryName: HoshiTextField!
    @IBOutlet weak var newCategtoryView: UIView!
    
    
    var ApiRequest = ResponseHandler()
    var categories:[CategoryData]?
    var catIcons:[String]?
    var catIcon:String = ""
    var selectedIndex = 0
    var organisationId:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newCategtoryView.isHidden = true
        getCategoryList()
        getCategoryIcons()
        setupLongGestureRecognizerOnCollection()
        // Do any additional setup after loading the view.
    }
    
    
    private func setupLongGestureRecognizerOnCollection() {
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        self.categoryColletionView?.addGestureRecognizer(longPressedGesture)
    }

    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if (gestureRecognizer.state != .began) {
            return
        }

        let gesture = gestureRecognizer.location(in: self.categoryColletionView)

        if let indexPath = categoryColletionView?.indexPathForItem(at: gesture) {
            
            Alertift.alert(title:"Delete Category", message: confirmMessage )
                .action(.destructive("Continue")){_,_,_ in
                                 
                         
                    guard let Id = self.categories?[indexPath.row]._id else{
                                     return
                                                                                   
                                    }
                                 self.removeCategory(id: Id)
                               
                                  
                          }.action(.cancel("Cancel"))
                              .show(on: self)
           
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
    
    @IBAction func addNewCategoryPressed(_ sender: Any) {
        
        self.newCategtoryView.isHidden = false
    }
    
    @IBAction func cancelCategoryPressed(_ sender: Any) {
        
        newCategtoryView.isHidden = true
    }
    
    
    @IBAction func savePressed(_ sender: Any) {
        
        
        guard let category = self.txtCategoryName.text, !category.isEmpty else {
            
            
            Alertift.alert(title: AppTitle, message:"Enter a category name")
            .action(.default("OK"))
            .show(on: self)
            
            return
        }
        
        
        self.saveCategory(categroyName: category, icon: self.catIcon)
        
    }
    
    
    
    func saveCategory(categroyName:String,icon:String){
        
        guard let orgID = self.organisationId else{                    //Defaults.string(forKey: "primeID") else{
            return
        }
        
        ApiRequest.addCategories(withParameter:["name":categroyName,"icon":icon,"organisation":orgID]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                self.newCategtoryView.isHidden = true
                self.getCategoryList()
                
            }else{
                self.newCategtoryView.isHidden = true
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
            }
         
    }
    
    }
    
    
    
    func getCategoryIcons(){
       
        
        SVProgressHUD.show()
        
       
        
        ApiRequest.getCatIcons(withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.ApiRequest.CategoryIconResponse?.data{
                    
                    if info.count > 0 {
                        
                      
                        self.catIcons = info

                        self.iconCollectionView.delegate = self
                        self.iconCollectionView.dataSource = self
                        self.iconCollectionView.reloadData()

                        
                        
                    }
                    
                   
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    private func removeCategory(id:String){
        
        let url = Api.Categories + id
        SVProgressHUD.show()
        ApiRequest.removeCategories(withUrl:url,withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                self.getCategoryList()
                
            }else{
                
                Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
            
            }
    }
    }
    

    func getCategoryList(){
        
        guard let orgID = Defaults.string(forKey: "primaryID") else{
            
            return
            
        }
        
        let url = Api.Categories + orgID
        
        SVProgressHUD.show()
        ApiRequest.getCategories(withUrl:url,withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.ApiRequest.CategoryResponse?.data{
                    
                    if info.count > 0 {
                        
                      
                        self.categories = info

                        self.categoryColletionView.delegate = self
                        self.categoryColletionView.dataSource = self
                        self.categoryColletionView.reloadData()

                        
                        
                    }
                    
                   
                }
                
            }
            
            
            
        }
        
        
    }

}




extension CategoriesListViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == iconCollectionView{
            
            return self.catIcons?.count ?? 0
        }
    
        return self.categories?.count ?? 0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == iconCollectionView{
            
            if let icon = self.catIcons?[indexPath.row]{
                
                self.catIcon = icon
            }
            
            self.selectedIndex = indexPath.row
            self.iconCollectionView.reloadData()
            
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == iconCollectionView{
            
            
            let cell = self.iconCollectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell, for: indexPath) as! CategoryViewCell
            
            if let info = self.catIcons?[indexPath.row]{
                
               
                 let icon = info
                    
                    let url = URL(string:getIcon(iconName:icon))
                    
                cell.imgIcon.sd_setImage(with: url)
                cell.imgIcon.image = cell.imgIcon.image?.withRenderingMode(.automatic)
                if #available(iOS 13.0, *) {
                    cell.imgIcon.image = cell.imgIcon.image?.withTintColor(UIColor.darkGray)
                } else {
                    // Fallback on earlier versions
                }
             
            }
            
            cell.backgroundColor = selectedIndex == indexPath.row ? #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1): UIColor.white
            
            
            return cell
            
        }
            
  
        let cell = self.categoryColletionView.dequeueReusableCell(withReuseIdentifier: CategoryCell, for: indexPath) as! CategoryViewCell
        
        if let info = self.categories?[indexPath.row]{
            
            if let title = info.name{
                
                 cell.lblTitle.text = title
            }
            
            if let icon = info.icon{
                
                let url = URL(string:getIcon(iconName:icon))
                
                cell.imgIcon.sd_setImage(with: url)
                cell.imgIcon.image = cell.imgIcon.image?.withRenderingMode(.automatic)
                if #available(iOS 13.0, *) {
                    cell.imgIcon.image = cell.imgIcon.image?.withTintColor(UIColor.darkGray)
                } else {
                    // Fallback on earlier versions
                }
                    
                    
                
            }
            
           
        }
        
        return cell
        
    }
        
    }
    

