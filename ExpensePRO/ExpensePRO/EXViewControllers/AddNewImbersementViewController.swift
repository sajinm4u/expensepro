//
//  AddNewImbersementViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 14/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import VisionKit
import SVProgressHUD
import TextFieldEffects
import Alertift
import ImageSlideShowSwift
import MobileCoreServices


class AddNewImbersementViewController: BaseViewController {
    
    
    @IBOutlet weak var txtDate: UITextField!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var txtAmount: HoshiTextField!
    @IBOutlet weak var txtPurpose: HoshiTextField!
    @IBOutlet weak var txtCategory: HoshiTextField!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var scannedImage: UIImageView!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var categoryColletionView: UICollectionView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var btnCategoryClose: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnSaveDraft: signButton!
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var statusStack: UIStackView!
    @IBOutlet weak var chatTableView: UITableView!

    @IBOutlet weak var imageSquare: UIImageView!
    @IBOutlet weak var btnRemovePdf: UIButton!
    
    var datePicker = UIDatePicker()

    @IBOutlet weak var segmentView: UISegmentedControl!
    
    var imbersement:reimberseArrayData?
    
     var ApiRequest = ResponseHandler()
     var categories:[CategoryData]?
     let imagePicker = UIImagePickerController()
     var isEdit:Bool? = false
     var isInbox:Bool = false
     var expenseId:String?
     var images:[Image] = []
     var localImages:[Image] = []
    
    var categoryId:String?
    var teamId:String = ""
    var currency:String?
    var imberseId:String?
    var amount:String?
    var comments:[commentsStruct]?
    var activities:[articleData]?
    var isComments:Bool = true
    
    @IBOutlet weak var lblClaimId: UILabel!
    @IBOutlet weak var lblSubmittedOn: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblSubmittedBy: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var claimView: UIView!
    @IBOutlet weak var teamView: UIView!
    
    @IBOutlet weak var claimDetailView: UIView!
    
    @IBOutlet weak var claaimDetailViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnSubmit: signButton!
    @IBOutlet weak var btnApprove: UIButton!
    var imageInfo:[String]?
    var finalImageArray:[UIImage] = []
    var currentImageArray:[UIImage] = []
    var userType:String?
    var expStatus = false
    
    @IBOutlet weak var txtComments: UITextField!
    
    let dateFormatter: DateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()

        // Do any additional setup after loading the view.
    }
    
    func initialSetup(){
        self.segmentView.isHidden = true
        self.chatTableView.isHidden = true
        self.btnRemovePdf.isHidden = true
        if let primary = Defaults.string(forKey:"primary") {
            if primary == "personal"{
                self.btnSaveDraft.isHidden = false
            }else{
                self.btnSaveDraft.isHidden = false
            }
        }
        
        self.btnSubmit.setTitle("SUBMIT", for:.normal)
        self.claimDetailView.isHidden = true
        self.claaimDetailViewHeightConstraint.constant = 10
        self.txtAmount.delegate = self
        
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels // Replace .inline with .compact
           
                    }
   
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.txtDate.inputView = self.datePicker
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(self.datePickerFromValueChanged(_:)), for: .valueChanged)

        
        datePicker.datePickerMode = .date
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        //let cancelButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton], animated: false)
        
        self.txtDate.inputAccessoryView = toolbar
        
        if isInbox {
            
            self.segmentView.isHidden = false
            self.chatTableView.isHidden = false
            self.btnSaveDraft.isHidden = true
            self.claimDetailView.isHidden = false
            self.claaimDetailViewHeightConstraint.constant = 208

            
            guard let id = self.expenseId else {
                return
            }
            self.btnSubmit.isHidden = true
            self.btnSaveDraft.isHidden = true
            setImberseMentDetails(id: id)
            self.getCommentsList(id: id)
            self.getActivityList(id: id)
        }
        
        
        
        if isEdit ?? false {
            
            self.segmentView.isHidden = false
            self.chatTableView.isHidden = false
            self.btnSaveDraft.isHidden = true
            self.btnDelete.isHidden = false
            self.btnSubmit.setTitle("UPDATE", for:.normal)
            
            self.claimDetailView.isHidden = false
            self.claaimDetailViewHeightConstraint.constant = 208
            
           
            if let info = self.imbersement {
            
                if let id = info._id{
                    
                    getImberseMentDetails(id: id)
                    self.getCommentsList(id: id)
                    self.getActivityList(id: id)
                    
                }
                
         
                if let status = self.imbersement?.reimbursementStatus{
                    
                    if status == "Draft"{
                        self.btnSubmit.setTitle("SUBMIT", for:.normal)
                    }
                    
                }
                
                
                if let currency = info.currency{
                    
                    if let amount = info.amount{
                        
                        self.amount = "\(amount)"
                        self.currency = currency
                      
                        self.txtAmount.text = currency.getCurrencySymbol(currencyCode: currency) + " \(amount)"
                        
                        if let purpose = info.title{
                            
                            self.txtPurpose.text = purpose
                        }
                        
                        if let category = info.category?.name{
                            self.txtCategory.text = category
                        }
                        
                        if let date = info.incurredDate{
                            self.txtDate.text = date.getDate()
                        }
                        
                        if let claimId = info.claimId{
                            
                            self.lblClaimId.text = claimId
                            
                        }
                        
                        if let catId = info.category?._id{
                            
                            self.categoryId = catId
                        }
                        
                        if let subOn = info.created_at?.getDate(){
                            
                            self.lblSubmittedOn.text = subOn
                        }
                        

                        
                        if let team = info.team?.name{
                            
                            
                            if team == "NA"{
                                self.teamView.isHidden = true
                                self.teamId = ""
                            }else{
                                
                                self.teamView.isHidden = false
                                if let teamId = info.team?._id{
                                    
                                    self.teamId = teamId
                                }
                            }
                            
                            self.lblTeam.text = team
                        }
                        
                        if let subBy = info.user?.name{
                            
                            
                            self.lblSubmittedBy.text = subBy
                        }
                        
                        if let status = info.reimbursementStatus{
                         
                            self.lblStatus.text = status
                            self.lblStatus.textColor = Utils.setType(type:status)
                        }
                        
                        if let imberseId = info._id{
                            
                            self.imberseId = imberseId
                        }
                        
                        
                    }
  
                    
                }
                
    
            }
            
            
            
        }else{
            

             let dateFormatter: DateFormatter = DateFormatter()
             let currentDate = Date()
              dateFormatter.dateFormat = "dd-MM-yyyy"
             let fromDate:String = dateFormatter.string(from: currentDate)
        
             self.txtDate.text = fromDate
             self.btnDelete.isHidden = true
        }
        
        self.categoryView.isHidden = true
        
        if isConnected(){
            
             getCategoryList()
        }
        
   
    }
    
    
    @IBAction func removePdfPressed(_ sender: Any) {
    }
    
    
    @IBAction func sendPressed(_ sender: Any) {
        
        guard let comments = self.txtComments.text, !comments.isEmpty else{
            
            Alertift.alert(title: AppTitle, message:"Please enter a comment")
            .action(.default("OK"))
            .show(on: self)
            return
        }
        let params = ["comment":comments,"image":""]
        guard let id = self.imbersement?._id else{
            return
        }
        self.postComments(id: id, params: params)
        
    }
    
    
    @IBAction func segmentPressed(_ sender:UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            
            self.isComments = true
            self.setupChatTableView()
            break
        case 1:
            self.isComments = false
            self.setupChatTableView()
            break
        default:
            break
        }
    }
    
 
    @IBAction func statusApprove(_ sender: UIButton) {
        
        switch sender.tag {
            
            
        case 0:
            
            var id = ""
            if let expid = self.imbersement?._id {
                id = expid
            }else{
                if let expid = self.expenseId{
                    id = expid
                }
            }
            guard let status = sender.titleLabel?.text else{
                return
            }
            changeImberseMentStatus(id:id, status: status)
        default:
            
            var id = ""
            if let expid = self.imbersement?._id {
                id = expid
            }else{
                if let expid = self.expenseId{
                    id = expid
                }
            }

            changeImberseMentStatus(id: id, status: "Reject")
        }
        
    }
    
    func setupChatTableView(){
        
        DispatchQueue.main.async {
            
            self.chatTableView.delegate = self
            self.chatTableView.dataSource = self
            self.chatTableView.reloadData()
            self.chatTableView.setContentOffset(CGPoint(x: 0, y: self.chatTableView.contentSize.height + 60), animated: false)
        }
    }
    
    func changeImberseMentStatus(id:String,status:String){
        
        SVProgressHUD.show()
        
        let url = Api.ChangeStatusImbersement + "/" + id

        ApiRequest.updateImbersmentStatus(urlString:url, withParameter:["status":status]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                self.statusStack.isHidden = true
                self.backNavigation()
                
            }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
            }
           
               
            }
            
            
            
        }
    
    
     func saveImage(imageName: String, image: UIImage) {

            guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

            let fileName = imageName
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            guard let data = image.jpegData(compressionQuality: 1) else { return }

            //Checks if file exists, removes it if so.
            if FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    try FileManager.default.removeItem(atPath: fileURL.path)
                    print("Removed old image")
                } catch let removeError {
                    print("couldn't remove file at path", removeError)
                }

            }

            do {
                try data.write(to: fileURL)
                let images = Image(title:"", url: fileURL)
                self.images.append(images)
                
            } catch let error {
                print("error saving file with error", error)
            }

        }
    
      func getImagePathFromDiskWith(fileName: String) -> URL? {

            let documentDirectory = FileManager.SearchPathDirectory.documentDirectory

            let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
            let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)

            if let dirPath = paths.first {
                let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
                return imageUrl
            }

            return nil
        }
    
    
    
    func getActivityList(id:String){
        
        SVProgressHUD.show()
        
        let url = Api.activities + id

        ApiRequest.getArticles(urlString:url, withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                if let info = self.ApiRequest.ArticleResponse?.data{
                    
                    self.activities = info
                    
                  
                }
        
                
               }
           
               
            }
            
            
            
        }
    
    
    
    func postComments(id:String,params:[String:String]){
        
        SVProgressHUD.show()
        
        let url = Api.comments + id

        ApiRequest.postComments(urlString:url, withParameter:params) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.ApiRequest.CommentResponse?.data?.comments{
                    
                    self.comments = info
                    if self.comments?.count ?? 0 > 0 {
                        self.txtComments.text = ""
                        self.setupChatTableView()
                    }
                  
                }
             
                //self.getCommentsList(id: id)
        
                
               }
           
               
            }
            
            
            
        }
    
    
    func getCommentsList(id:String){
        
        SVProgressHUD.show()
        
        let url = Api.comments + id

        ApiRequest.getComments(urlString:url, withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                if let info = self.ApiRequest.CommentResponse?.data?.comments{
                    
                    self.comments = info
                    if self.comments?.count ?? 0 > 0 {
                        self.setupChatTableView()
                    }
                  
                }
        
                
               }
           
               
            }

        }
    
    
    func setImberseMentDetails(id:String){
        
        SVProgressHUD.show()
        
        let url = Api.CreateImbersement + "/" + id

        ApiRequest.ImbersementDetails(urlString:url, withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                if let info = self.ApiRequest.ImberseDetailResponse?.data?.request{
                    
                    if let currency = info.currency{
                        
                        if let amount = info.amount{
                            
                            self.amount = "\(amount)"
                            self.currency = currency
                          
                            self.txtAmount.text = currency.getCurrencySymbol(currencyCode: currency) + " \(amount)"
                            
                            if let purpose = info.title{
                                
                                self.txtPurpose.text = purpose
                            }
                            
                            if let category = info.category?.name{
                                self.txtCategory.text = category
                            }
                            
                            if let date = info.incurredDate{
                                self.txtDate.text = date.getDate()
                            }
                            
                            if let claimId = info.claimId{
                                
                                self.lblClaimId.text = claimId
                                
                            }
                            
                            if let catId = info.category?._id{
                                
                                self.categoryId = catId
                            }
                            
                            if let subOn = info.created_at?.getDate(){
                                
                                self.lblSubmittedOn.text = subOn
                            }
                            

                            
                            if let team = info.team?.name{
                                
                                
                                if team == "NA"{
                                    self.teamView.isHidden = true
                                    self.teamId = ""
                                }else{
                                    
                                    self.teamView.isHidden = false
                                    if let teamId = info.team?._id{
                                        
                                        self.teamId = teamId
                                    }
                                }
                                
                                self.lblTeam.text = team
                            }
                            
                            if let subBy = info.user?.name{
                                
                                
                                self.lblSubmittedBy.text = subBy
                            }
                            
                            if let status = info.reimbursementStatus{
                             
                                self.lblStatus.text = status
                                self.lblStatus.textColor = Utils.setType(type:status)
                            }
                            
                            if let imberseId = info._id{
                                
                                self.imberseId = imberseId
                            }
                            
                            
                        }
      
                        
                    }
                    
                    if self.ApiRequest.ImberseDetailResponse?.data?.status?.count ?? 0 > 0{
                        if let status = self.ApiRequest.ImberseDetailResponse?.data?.status{
                            
                            if (status[0] == "Approve" || status[0] == "Pay" || status[0] == "Review"){
                                self.btnApprove.setTitle(status[0], for: .normal)
                                self.statusStack.isHidden = false
                            }else{
                                
                                self.statusStack.isHidden = true
                            }
                            
                        }
                       
                    }else{
                        
                        self.statusStack.isHidden = true
                    }
                 
                    if info.images?.count ?? 0 > 0{
                        
                        self.imageInfo = info.images
                        if let images = info.images{
                            
                            for image in images {
                                
                                let url = URL(string:Utils.getImage(id: image))!
                                self.downloadImage(from: url)
                                
                            }
                            
                            
                        }
                        
                       
                        
                    }
                }
                
          
               }
           
               
            }
            
            
            
        }
    
    func getImberseMentDetails(id:String){
        
        SVProgressHUD.show()
        
        let url = Api.CreateImbersement + "/" + id

        ApiRequest.ImbersementDetails(urlString:url, withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                if let info = self.ApiRequest.ImberseDetailResponse?.data?.request{
                
                    
                    if self.ApiRequest.ImberseDetailResponse?.data?.status?.count ?? 0 > 0{
                        if let status = self.ApiRequest.ImberseDetailResponse?.data?.status{
                            
                            if (status[0] == "Approve" || status[0] == "Pay" || status[0] == "Review"){
                                self.btnApprove.setTitle(status[0], for: .normal)
                                self.statusStack.isHidden = false
                            }else{
                                
                                self.statusStack.isHidden = true
                            }
                            
                        }
                       
                    }else{
                        
                        self.statusStack.isHidden = true
                    }
                 
                    if info.images?.count ?? 0 > 0{
                        
                        self.imageInfo = info.images
                        if let images = info.images{
                            
                            for image in images {
                                
                                let url = URL(string:Utils.getImage(id: image))!
                                self.downloadImage(from: url)
                                
                            }
                            
                            
                        }
                        
                       
                        
                    }
                }
                
          
               }
           
               
            }
            
            
            
        }
        
   
    
    @objc func datePickerFromValueChanged(_ sender: UIDatePicker){
        
        
        let selectedDate: String = dateFormatter.string(from: sender.date)
        self.txtDate.text = selectedDate
    
    }
    
    @IBAction func deletePressed(_ sender: Any) {
        
        
        Alertift.alert(title:"Delete", message: confirmMessage )
            .action(.destructive("Continue")){_,_,_ in 
                            
                             guard let Id = self.imberseId else{
                                 return
                                                                               
                                }
                             self.deleteImbersement(id: Id)
                           
                              
                      }.action(.cancel("Cancel"))
                          .show(on: self)

        
        
    }
    
    
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
      //  print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
//            print(response?.suggestedFilename ?? url.lastPathComponent)
//            print("Download Finished")
            
            print(url.lastPathComponent)
            
            let filename = url.lastPathComponent
            let splitName = filename.split(separator: ".")
            let name = splitName.last
            
            if name == "pdf"{
                
                let image = self.drawPDFfromURL(url: url)
                
                guard let pdfImage = image else{
                    return
                }
                
                self.currentImageArray.append(pdfImage)
                self.finalImageArray.append(pdfImage)
                //let images = Image(title:"", url: url)
    
                self.saveImage(imageName: filename, image: pdfImage)
            }
            
            if let image = UIImage(data: data){
                
                self.currentImageArray.append(image)
                self.finalImageArray.append(image)
                let images = Image(title:"", url: url)
                
                self.images.append(images)
            }
            
    
            DispatchQueue.main.async() { [weak self] in
                
                if self?.finalImageArray.count ?? 0 > 0{
                    
                    self?.imageCollectionView.delegate = self
                    self?.imageCollectionView.dataSource = self
                    self?.imageCollectionView.reloadData()
                   
                }
                
            }
        }
        
       
    }
    
    
    @objc func donedatePicker(){

        self.view.endEditing(true)
        
    }
    

    @IBAction func scanPressed(_ sender: Any) {
        
        chooseUploadOption()
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
   
    @IBAction func categoryPressed(_ sender: Any) {
        
        
        self.txtAmount.resignFirstResponder()
        self.txtPurpose.resignFirstResponder()
        self.categoryView.isHidden = false

        
    }
    

  
    func imberseSubmit(submit:Bool){
        
        
        if isEdit ?? false {
            
            var submit = submit
            guard var textAmount = self.txtAmount.text, let textPurpose = self.txtPurpose.text, let categroyId = self.categoryId,!textAmount.isEmpty,!textPurpose.isEmpty,!categroyId.isEmpty  else{
                
                Alertift.alert(title: AppTitle, message:mandatoryFields)
                .action(.default("OK"))
                .show(on: self)
                
                return
            }
            
            
            guard let dateStr = self.txtDate.text else{
                return
            }
            
            if let status = self.imbersement?.reimbursementStatus{
                
                if status == "Draft"{
                   
                    submit = true
                }
                
            }
        
            
            let remove = (self.currency?.getCurrencySymbol(currencyCode: self.currency ?? "") ?? "") + " "
            
            if let range = textAmount.range(of: remove) {
                textAmount.removeSubrange(range)
            }
            
            guard let orgId = Defaults.string(forKey: "primaryID") else{
                return
            }
           
            let params =  NewImberseParams(title: textPurpose, description: textPurpose, amount:textAmount, category:categroyId, team: self.teamId, isSubmitted: submit, incurredDate:dateStr, receiptCount:"\(self.finalImageArray.count)",organisation:orgId).Values
            
            guard let imberseId = self.imberseId else {
                
                return
            }
            
            
            self.updateImbersement(id:imberseId, params: params)
           
        
        }else{
            
            guard var textAmount = self.txtAmount.text, let textPurpose = self.txtPurpose.text, let categroyId = self.categoryId,!textAmount.isEmpty,!textPurpose.isEmpty,!categroyId.isEmpty  else{
                
            
                Alertift.alert(title: AppTitle, message:mandatoryFields)
                .action(.default("OK"))
                .show(on: self)
                return
            }
            
            
            guard let dateStr = self.txtDate.text else{
                return
            }
            
            let remove = (self.currency?.getCurrencySymbol(currencyCode: self.currency ?? "") ?? "") + " "
            
            if let range = textAmount.range(of: remove) {
                textAmount.removeSubrange(range)
            }
            

             
            guard let orgId = Defaults.string(forKey: "primaryID") else{
                return
            }
            
            
            let params =  NewImberseParams(title: textPurpose, description: textPurpose, amount: textAmount, category:categroyId, team: self.teamId, isSubmitted: submit, incurredDate:dateStr, receiptCount:"\(self.finalImageArray.count)",organisation:orgId).Values
            
          // print(params)
            
            self.submitNewImbersement(params: params)
    
        }
 
    }
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        self.btnSubmit.isEnabled = false
        imberseSubmit(submit: true)
       
    }
    
    @IBAction func saveDraftPressed(_ sender: Any) {
        
        imberseSubmit(submit: false)
    }
    

    @IBAction func changeDatePressed(_ sender: Any) {
        

    }
    
    
    func submitNewImbersement(params:[String:Any]){
        
        SVProgressHUD.show()

        ApiRequest.AddNewImbersement(withParameter:params) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
               if self.finalImageArray.count > 0 {
                
                if let id = self.ApiRequest.ImbersementAddedResponse?.data?._id{
                    
                    self.uploadBills(id:id, images: self.finalImageArray[0])
                }
                    
                   
               }else if self.finalImageArray.count == 0{
                
                self.backNavigation()
                
               }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
                
               }
           
                self.btnSubmit.isEnabled = true
            }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
                self.btnSubmit.isEnabled = true
            }
            
        }
        
    }
    
    func removeImage(image:String,id:String){
        
        SVProgressHUD.show()
        
        let url = Api.removeReceipts + id

        ApiRequest.DeleteReceipt(urlString:url, withParameter:["image":image]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                print("removed")
                
                
               }
 
            }
  
        }
    
    
    func deleteImbersement(id:String){
        
        SVProgressHUD.show()
        
        let url = Api.CreateImbersement + "/" + id

        ApiRequest.DeleteNewImbersement(urlString:url, withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                self.backNavigation()
                
                
               }
      
            }
 
        }
        

    func updateImbersement(id:String,params:[String:Any]){
        
        SVProgressHUD.show()
        
        let url = Api.CreateImbersement + "/" + id

        ApiRequest.UpdateNewImbersement(urlString:url, withParameter:params) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
               if self.currentImageArray.count > 0 {
                
                        if let id = self.ApiRequest.ImbersementAddedResponse?.data?._id{
                            
                           for image in self.imageInfo!{
                            self.removeImage(image: image, id: self.imberseId!)
                            }
                            
                            if self.finalImageArray.count > 0{
                                
                                self.uploadBills(id:id, images: self.finalImageArray[0])
                            }
                            
                           
                        
                    }else{
                        
                        if let id = self.ApiRequest.ImbersementAddedResponse?.data?._id{
                            
                            self.uploadBills(id:id, images: self.finalImageArray[0])
                        }
                        
                    }
                    
         
               }else if self.finalImageArray.count > 0{
                
                if let id = self.ApiRequest.ImbersementAddedResponse?.data?._id{
                    
                    self.uploadBills(id:id, images: self.finalImageArray[0])
                }
                
               }
               
               else if self.finalImageArray.count == 0{
                
                self.backNavigation()
                
               }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
                
               }
           
                self.btnSubmit.isEnabled = true

               
            }else{
                
                self.btnSubmit.isEnabled = true
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
               
                
            }
            
            
            
        }

        
    }
    
    
    
    func uploadBills(id:String,images:UIImage){
        
        SVProgressHUD.show()

        let url = Api.UploadImage + id                 //Api.UploadImage + id
        
        ApiRequest.UploadImbersementImage(urlString:url,image: images) { (isSuccess, message)  in
            SVProgressHUD.dismiss()
            if (isSuccess){
             
                self.finalImageArray.remove(at: 0)
                
                if self.finalImageArray.count > 0 {
                
                 if let id = self.ApiRequest.ImbersementAddedResponse?.data?._id{
                     
                     self.uploadBills(id:id, images: self.finalImageArray[0])
                 }
                     
                    
                }else{
                    self.backNavigation()
                }
               
            }else{
                
                self.backNavigation()
               
            }
    
        }
        
        
    }
    
    @IBAction func sendComments(_ sender: Any) {
        
        
    }
    
    @IBAction func closeCategorySelection(_ sender: Any) {
           self.categoryView.isHidden = true
        
    }
    
    
    func getCategoryList(){
        
        guard let organisation = Defaults.string(forKey: "primaryID") else{
            
            return
            
        }
        
        SVProgressHUD.show()
        
        let url = Api.Categories + organisation
       
        
        ApiRequest.getCategories(withUrl:url,withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.ApiRequest.CategoryResponse?.data{
                    
                    if info.count > 0 {
                        
                      
                        self.categories = info

                        self.categoryColletionView.delegate = self
                        self.categoryColletionView.dataSource = self
                        self.categoryColletionView.reloadData()

                        
                        
                    }
                    
                   
                }
                
            }
            
            
            
        }
        
        
    }
    
    
        func drawPDFfromURL(url: URL) -> UIImage? {
            guard let document = CGPDFDocument(url as CFURL) else { return nil }
            guard let page = document.page(at: 1) else { return nil }

            let pageRect = page.getBoxRect(.mediaBox)
            let renderer = UIGraphicsImageRenderer(size: pageRect.size)
            let img = renderer.image { ctx in
                UIColor.white.set()
                ctx.fill(pageRect)

                ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
                ctx.cgContext.scaleBy(x: 1.0, y: -1.0)

                ctx.cgContext.drawPDFPage(page)
            }

            return img
        }
    
    
    
    func chooseUploadOption() {

        let optionMenu = UIAlertController(title: nil, message: "Choose Receipt", preferredStyle: .actionSheet)

        let fileAction = UIAlertAction(title: "Scan Document", style: .default) { _ in
            
            self.goForscan()
        

     }

            let galleryAction = UIAlertAction(title: "Gallery", style: .default) { _ in

                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)

            }
        
            
            let cloudAction = UIAlertAction(title: "iCloud", style: .default) { _ in

                self.getFileFromDrive()


        }
        
        

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

            // 4
            optionMenu.addAction(fileAction)
            optionMenu.addAction(galleryAction)
            optionMenu.addAction(cloudAction)
            optionMenu.addAction(cancelAction)

            // 5
            self.present(optionMenu, animated: true, completion: nil)

    }
    
    
    
    
    func goForscan(){
          
                      if #available(iOS 13.0, *) {
                          let scannerViewController = VNDocumentCameraViewController()
                          scannerViewController.delegate = self
                          present(scannerViewController, animated: true)

                      } else {

                       imagePicker.allowsEditing = true
                       imagePicker.sourceType = .camera
                       present(imagePicker, animated: true, completion: nil)
                       
                         
                   }
                       
           
           
       }
    
    func getFileFromDrive(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
                    importMenu.delegate = self
                    importMenu.modalPresentationStyle = .formSheet
                    self.present(importMenu, animated: true, completion: nil)
       
    }
    
}

@available(iOS 13.0, *)
extension AddNewImbersementViewController:VNDocumentCameraViewControllerDelegate{
    
     func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
         // Process the scanned pages
         
         var scannedImage:UIImage?
         
         
         for pageNumber in 0..<scan.pageCount {
            
             let image = scan.imageOfPage(at: pageNumber)
        
             self.finalImageArray.append(image)
             self.saveImage(imageName:"scan" + "\(pageNumber)", image: image)
            

     }
        
        if self.finalImageArray.count > 0{
            
            self.imageCollectionView.delegate = self
            self.imageCollectionView.dataSource = self
            self.imageCollectionView.reloadData()
            
        }
        
        controller.dismiss(animated: true)
        
     }
     
     func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
    
         controller.dismiss(animated: true)
     }
     
     func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
         // You should handle errors appropriately in your app.
     
         // You are responsible for dismissing the controller.
         controller.dismiss(animated: true)
     }

    
}

extension AddNewImbersementViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
                  let myURL = url as URL
                 
        do {
            let data = try NSData(contentsOf: myURL, options: NSData.ReadingOptions())

            let filename = myURL.lastPathComponent
            let splitName = filename.split(separator: ".")
            let name = splitName.last
        
            
           let pdfImage = drawPDFfromURL(url:myURL)
            
            if let image = pdfImage{
              
                        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                        let localPath = documentDirectory?.appending(filename)
                    let data = image.pngData()! as NSData
                        data.write(toFile: localPath!, atomically: true)
                        //let imageData = NSData(contentsOfFile: localPath!)!
                    let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
                    let images = Image(title:"", url: photoURL)
                    self.images.append(images)

    
                self.finalImageArray.append(image)
                
                if self.finalImageArray.count > 0{
                    
                    self.imageCollectionView.delegate = self
                    self.imageCollectionView.dataSource = self
                    self.imageCollectionView.reloadData()
                    self.imageCollectionView.isHidden = false
                    
                }
                }
            
            } catch {
            print(error)
           }
                    
        }


    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
        }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
                print("view was cancelled")
                dismiss(animated: true, completion: nil)
        }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        var selectedImage: UIImage?
        
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
           
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
           
            picker.dismiss(animated: true, completion: nil)
        }
        
        if let image = selectedImage{
            
            if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
                    let imgName = imgUrl.lastPathComponent
                    let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                    let localPath = documentDirectory?.appending(imgName)

                let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
                let data = image.pngData()! as NSData
                    data.write(toFile: localPath!, atomically: true)
                    //let imageData = NSData(contentsOfFile: localPath!)!
                let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
                let images = Image(title:"", url: photoURL)
                self.images.append(images)

                }


            self.finalImageArray.append(image)
            
            if self.finalImageArray.count > 0{
                
                self.imageCollectionView.delegate = self
                self.imageCollectionView.dataSource = self
                self.imageCollectionView.reloadData()
                self.imageCollectionView.isHidden = false
                
            }
            
        }
        
     
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
               
               dismiss(animated: true, completion: nil)
    }
    
    
}
extension AddNewImbersementViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == imageCollectionView{
            
            
            return self.finalImageArray.count
        }
        
       return self.categories?.count ?? 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
         if collectionView == categoryColletionView {
             
            let width = (self.categoryColletionView.frame.size.width) / 4
             let height = width
             return CGSize(width: width, height: height)
             
         }
        
        if collectionView == imageCollectionView{
            
            return CGSize(width: 114.0, height: 114.0)
        }
        
        return CGSize(width: 0, height: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.categoryColletionView{
        
        if let info = categories?[indexPath.row]{
            
            if let title = info.name{
                
                self.txtCategory.text = title
                
                if let catId = info._id {
                    
                    self.categoryId = catId
                    
                    
                }
                
            }
            
           
        }
           self.categoryView.isHidden = true
        }else{
            
            if isEdit ?? false || isInbox {
            
            ImageSlideShowViewController.presentFrom(self){ [weak self] controller in

                controller.dismissOnPanGesture = true
                guard let images = self?.images else{
                    return
                }
                controller.slides = images
                controller.enableZoom = true
                controller.initialIndex = indexPath.row
                controller.controllerDidDismiss = {
                    debugPrint("Controller Dismissed")
                    
                    debugPrint("last index viewed: \(controller.currentIndex)")
                }
                
                controller.slideShowViewDidLoad = {
                    debugPrint("Did Load")
                }
                
                controller.slideShowViewWillAppear = { animated in
                    debugPrint("Will Appear Animated: \(animated)")
                }
                
                controller.slideShowViewDidAppear = { animated in
                    debugPrint("Did Appear Animated: \(animated)")
                }
                
            }
            
            }
            
            
        }
    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == imageCollectionView{
            
            let cell = self.imageCollectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionCell
            
            cell.itemPic.image = self.finalImageArray[indexPath.row]
            cell.index = indexPath.row
            cell.delegate = self
            
            return cell
        }
        
        
        
        let cell = self.categoryColletionView.dequeueReusableCell(withReuseIdentifier: CategoryCell, for: indexPath) as! CategoryViewCell
        
        if let info = self.categories?[indexPath.row]{
            
            if let title = info.name{
                
                 cell.lblTitle.text = title
            }
            
            if let icon = info.icon{
                
                let url = URL(string:getIcon(iconName:icon))
                
                cell.imgIcon.sd_setImage(with: url)
                cell.imgIcon.image = cell.imgIcon.image?.withRenderingMode(.automatic)
                if #available(iOS 13.0, *) {
                    cell.imgIcon.image = cell.imgIcon.image?.withTintColor(UIColor.darkGray)
                } else {
                    // Fallback on earlier versions
                }
                
            }
            
           
        }
        
        return cell
        
    }

}

extension AddNewImbersementViewController:imageCellDelegate{
    func removeImage(at index: Int) {

        if isEdit ?? false{
            if self.imageInfo?.count ?? 0 > index {
            if let image = self.imageInfo?[index]{
                self.removeImage(image: image,id: self.imberseId!)
                if self.currentImageArray[index] != nil{
                    self.currentImageArray.remove(at: index)
                }
            }
            }
        }
        
        if finalImageArray.count > 0{
            
            self.finalImageArray.remove(at: index)
            self.imageCollectionView.reloadData()
            
        }
   
    }
    
}

extension AddNewImbersementViewController:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtAmount{
       
            self.txtAmount.text = (self.currency?.getCurrencySymbol(currencyCode: self.currency ?? "") ?? "") + " "
            //print(self.txtAmount.text)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        // Allow to remove character (Backspace)
        if string == "" {
            return true
        }

       // Block multiple dot
        if (textField.text?.contains("."))! && string == "." {
            return false
        }

        // Check here decimal places
        if (textField.text?.contains("."))! {
            let limitDecimalPlace = 2
            let decimalPlace = textField.text?.components(separatedBy: ".").last
            if (decimalPlace?.count)! < limitDecimalPlace {
                return true
            }
            else {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    
    if textField == txtAmount{
    
               self.amount = textField.text
        
        }


        return true
    }
    
    
}

extension AddNewImbersementViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isComments{
           return self.comments?.count ?? 0}
        else{
           return self.activities?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = chatTableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatCell
        
        if isComments{
        
        if let info = comments?[indexPath.row]{
            if let name = info.user?.name{
                cell.name.text = name
            }
            if let message = info.comment{
                cell.lblMessage.text = message
            }
            if let time =  info.createdAt{
                cell.lblTime.text = time.getDate()
            }
            
        }
        }else{
            

            if let info = activities?[indexPath.row]{
                cell.name.text = ""
                if let message = info.log{
                cell.lblMessage.text = message
            }
            if let time =  info.logged_at{
                cell.lblTime.text = time.getDate()
            }
        
            }
        }
        return cell
    }
    
    
}

//extension AddNewImbersementViewController: {
//
//    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
//
//        documentPicker.delegate = self
//        present(documentPicker, animated: true, completion: nil)
//
//    }
//
//
//    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
//
//
//            let cico = url as URL
//            print("The Url is : /(cico)", cico)
//
//
//            do {
//                let weatherData = try NSData(contentsOf: cico, options: NSData.ReadingOptions())
//                print(weatherData)
////                let activityItems = [weatherData]
////                let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
////                if UI_USER_INTERFACE_IDIOM() == .phone {
////                    self.present(activityController, animated: true, completion: {  })
////                }
////                else {
////                    let popup = UIPopoverController(contentViewController: activityController)
////                    popup.present(from: CGRect(x: CGFloat(self.view.frame.size.width / 2), y: CGFloat(self.view.frame.size.height / 4), width: CGFloat(0), height: CGFloat(0)), in: self.view, permittedArrowDirections: .any, animated: true)
//      //          }
//
//            } catch {
//                print(error)
//            }
//
//
//            //optional, case PDF -> render
//            //displayPDFweb.loadRequest(NSURLRequest(url: cico) as URLRequest)
//
//
//
//
//        }
//
//
//
//}
