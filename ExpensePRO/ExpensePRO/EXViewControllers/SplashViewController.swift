//
//  SplashViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 21/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var splashViewController: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    var splashArray = GlobalData.SplashArray()
    var splashHeaderArray = GlobalData.SplashHeaderArray()
    var splashDescArray = GlobalData.SplashTextArray()
    var currentPage = 0
    
   

    override func viewDidLoad() {
        super.viewDidLoad()

        
        initialLoad()
       
    }
    
    
    func initialLoad(){
        
        self.splashViewController.delegate = self
        self.splashViewController.dataSource = self

        
    }
    
    
    @IBAction func skipPressed(_ sender: Any) {
        
        
        Defaults.set(true, forKey: "splashSkip")
        
         let AccountScene = SignInViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = navigationController {

                navigator.pushViewController(AccountScene, animated: true)

            }
            
            
    
        
    }
    
   

}

extension SplashViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.splashArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = (self.splashViewController.dequeueReusableCell(withReuseIdentifier: "splashCell", for: indexPath) as? SplashCell)!
        
        
    cell.bgImage.image = UIImage(named:(self.splashArray?[indexPath.row])!)
    cell.lblHeader.text = self.splashHeaderArray?[indexPath.row]
    cell.lblDescription.text = self.splashDescArray?[indexPath.row]
        
        return cell
        
    
    }
    
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        
        if self.currentPage == 2{
            self.btnSkip.setTitle("Continue", for: .normal)
        }
        self.pageControl.currentPage = self.currentPage
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           
        let itemSizeWidth = self.view.frame.width
        let itemSizeHeight = collectionView.frame.size.height
           
    
           
           return CGSize(width: itemSizeWidth, height: itemSizeHeight)
       }
       
    
    
    

}
