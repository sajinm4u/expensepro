//
//  NewOrganizationSetupVC.swift
//  ExpensePRO
//
//  Created by Sajin M on 17/11/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alertift
import SVProgressHUD
import MRCountryPicker

class NewOrganizationSetupVC: BaseViewController {
    
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var txtPhone: HoshiTextField!
    @IBOutlet weak var txtCurrency: HoshiTextField!
    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtOtp: HoshiTextField!
    @IBOutlet weak var txtConfirmPwd: HoshiTextField!
    @IBOutlet weak var txtCompanyName: HoshiTextField!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    var Register = ResponseHandler()
    
    var codePicker = MRCountryPicker()
    var tokenStr:String? = nil
    var registerParams = [String:Any]()
    var name:String?
    var email:String?
    var logoImage:UIImage? = nil
    
    
    @IBOutlet weak var otpView: UIView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgLogo?.layer.cornerRadius = imgLogo.frame.height/2
        imgLogo?.contentMode = .scaleAspectFill
        otpView.isHidden = true
        codePicker.countryPickerDelegate = self
        codePicker.showPhoneNumbers = false
        self.txtCurrency.inputView = codePicker

        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitPressed(_ sender: Any)
    {
        
        guard let txtCompanyName = self.txtCompanyName.text, !txtCompanyName.isEmpty else{

            self.showNotification(message: companyName)

        return
        }
        
        guard let txtCurrency = self.txtCurrency.text, !txtCurrency.isEmpty else{
            
            self.showNotification(message: "Please select currency")
            
        return
        }
        
        
        
        
        
        

        
        if isConnected(){
            
           
                   
            guard let txtCurrency = self.txtCurrency.text,let txtCompany = self.txtCompanyName.text, !txtCurrency.isEmpty,!txtCompany.isEmpty else {
                      
             self.showNotification(message:mandatoryFields)
                      
                       return
                   }
            

             
             let params = AccountRegistration(name:txtCompanyName, currency:txtCurrency).Values
             
             self.registerParams = params
            
             self.doRegister(param: params)
             
            // self.updateProfile()
    
            }
    
            
    }
    
    @IBAction func logoUpdate(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    
    
    func updateProfile(){
  
                
        guard let email = self.email else {
                    
                    return
                    
                }
         
                self.generateOtp(email: email)
                
            }
    
    @IBAction func submitOtpPressed(_ sender: Any) {
        
        
        guard let otp = self.txtOtp.text, !otp.isEmpty else{
            
            Alertift.alert(title: AppTitle, message:otpNotValid)
            .action(.default("OK"))
            .show(on: self)
            return
            
        }
        
        guard let email = self.email else {
            
            return
            
        }
   
        self.verifyOtp(email: email, otp: otp)
    }
    
    @IBAction func resendOtpPressed(_ sender: Any) {
        
        guard let email = self.email else {
            
            return
            
        }
        
        self.generateOtp(email: email)
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    func generateOtp(email:String){
        
   
        
        SVProgressHUD.show()
        
        Register.SendOtp(withParameter:["email":email]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
            self.otpView.isHidden = false
                    
               
            }else{
                
                
                
                if let isRegistered = self.Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        DispatchQueue.main.async {
                            
                            Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
                            
                            
                            
                        }
                        
                        self.backNavigation()
                       
                    }
                        
                    }else{
                        
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                    }
                
               
                

            }
            
            
                
            }
        
    }
    
    
    func verifyOtp(email:String,otp:String){
        
      
        
        SVProgressHUD.show()
        
        Register.VerifyOtp(withParameter:["email":email,"otp":otp]) { [self] (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
                self.txtOtp.text = ""
                self.otpView.isHidden = true
                self.doRegister(param: self.registerParams)
               
            }else{
                
             
                
              if let isRegistered = Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        self.txtOtp.text = ""
                        self.otpView.isHidden = true
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                        
                        self.backNavigation()
                        
                    }
                    
                    
              }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
                
              }
                

            }
            
            
                
            }
        
    }
    
    func uploadProfileImage(images:UIImage,id:String){
        
        SVProgressHUD.show()

        let url = Api.organisationLogo + id
        
        Register.UploadProfileImage(urlString:url,image: images) { (isSuccess, message)  in
            SVProgressHUD.dismiss()
            if (isSuccess){
               
                DispatchQueue.main.async {
                    
                    Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                }
                
                self.backNavigation()
                //self.getUserProfile()
               
            }else{
                
                
                 DispatchQueue.main.async {
                     
                     Alertift.alert(title: AppTitle, message:message)
                     .action(.default("OK"))
                     .show(on: self)
                 }
               
               
            }
            
            
            
        }
        
        
    }
    
    
    
    func doRegister(param:[String:Any]){
        
        
                SVProgressHUD.show()
        
                Register.RegisterBusiness(withParameter:param) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
        
                    if isSuccess{
                        
                                        if let info = self.Register.RegisterBusinessResponse?.data{
                        
                                            if self.logoImage != nil{
                                                
                                                self.uploadProfileImage(images: self.logoImage!,id: info._id)
                                            }else{
                                                
                                                DispatchQueue.main.async {
                                                    
                                                    Alertift.alert(title: AppTitle, message:message)
                                                    .action(.default("OK"))
                                                    .show(on: self)
                                                }
                                                
                                                self.backNavigation()
                                                
                                            }
                                            
                                           
 
                        
                                        }
          
                                 }
                        else{
                    
                                    Alertift.alert(title: AppTitle, message:message)
                                    .action(.default("OK"))
                                    .show(on: self)
                    
                    
                                }
                    
                                
                    
                                }
                    
                        }
    
    

    

}

extension NewOrganizationSetupVC: MRCountryPickerDelegate {
    

      func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
       
        
        if let currency = Locale.currency[countryCode]?.code{
            
            
            self.txtCurrency.text = currency
            
        }
        
      
             
         }
         
    
    
}


extension NewOrganizationSetupVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //print("\(info)")
        if let image = info[.originalImage] as? UIImage {
            
           
            self.logoImage = image
            self.imgLogo.image = image
                
           // self.uploadProfileImage(images:image,userId:id)
            
            
            
           
            dismiss(animated: true, completion: nil)
        }
    }
}
