//
//  DashBoardViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 04/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import PieCharts
//import Charts
import CoreCharts
import Alertift
class DashBoardViewController: BaseViewController,CoreChartViewDataSource {
    
    var DashBoard = ResponseHandler()
    var dashboardTypes:[String] = []
    var currentRole:String?
    
    @IBOutlet weak var lblRequestCount: UILabel!
    @IBOutlet weak var lblRequestAmount: UILabel!
    
    @IBOutlet weak var lblPaidCount: UILabel!
    
    @IBOutlet weak var lblPaidAmount: UILabel!
    @IBOutlet weak var lblApprovedCount: UILabel!
    
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var lblReviewAmount: UILabel!
    
    @IBOutlet weak var lblRejectCount: UILabel!
    @IBOutlet weak var lblRejectAmount: UILabel!
    
    @IBOutlet weak var lblApprovedAmount: UILabel!
    
    @IBOutlet weak var categoryChartView: PieChart!
    @IBOutlet weak var teamChartView: PieChart!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDatePeriod: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var teamBarChartView: VCoreBarChart!
    
    @IBOutlet weak var filterView: UIView!
    
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var filterTableView: UITableView!
    
    @IBOutlet weak var teamExpView: UIView!
    
    @IBOutlet weak var rejectedView: CurvedView!
    @IBOutlet weak var approvedView: CurvedView!
    @IBOutlet weak var paidView: CurvedView!
    
    @IBOutlet weak var lblRequestTxt: UILabel!
    @IBOutlet weak var emptyLblOne: UILabel!
    @IBOutlet weak var emptyLblTwo: UILabel!
    
    @IBOutlet weak var dateRangeView: UIView!
    
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    
    var datePicker = UIDatePicker()
    var endDatePicker = UIDatePicker()
    let dateFormatter: DateFormatter = DateFormatter()
    let startDateFormatter: DateFormatter = DateFormatter()
    let endDateFormatter: DateFormatter = DateFormatter()
    var currency:String = ""
    
    var startDate:String?
    var endDate:String?
    var typeDashboard:String?
    
    var stDate:String?
    var edDate:String?
    var months = [String]()
    var expValues = [Double]()
    
    var filterArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateRangeView.isHidden = true
  
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        initialSetup()
    }
    
    fileprivate static let alpha: CGFloat = 1
       let colors = [
           
        
           UIColor.purple.withAlphaComponent(alpha),
           UIColor.red.withAlphaComponent(alpha),
           UIColor.green.withAlphaComponent(alpha),
           UIColor.orange.withAlphaComponent(alpha),
           UIColor.blue.withAlphaComponent(alpha),
           UIColor.darkGray.withAlphaComponent(alpha),
           UIColor.magenta.withAlphaComponent(alpha),
           UIColor.yellow.withAlphaComponent(alpha),
           UIColor.brown.withAlphaComponent(alpha),
           UIColor.lightGray.withAlphaComponent(alpha),
           UIColor.gray.withAlphaComponent(alpha),
       ]
       fileprivate var currentColorIndex = 0
    
    
    func initialSetup(){
       
        self.filterView.isHidden = true
        self.filterTableView.tableFooterView = UIView(frame: .zero)
        self.emptyLblOne.isHidden = true
        self.emptyLblTwo.isHidden = true
        
        
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
            self.endDatePicker.preferredDatePickerStyle = .wheels
           
                    }
   
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.txtStartDate.inputView = self.datePicker
        self.txtEndDate.inputView = self.endDatePicker
        
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -5, to: Date())
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(self.datePickerFromValueChanged(_:)), for: .valueChanged)
        
        endDatePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        endDatePicker.maximumDate = Date()
        endDatePicker.addTarget(self, action: #selector(self.dateEndPickerFromValueChanged(_:)), for: .valueChanged)

        
        datePicker.datePickerMode = .date
        endDatePicker.datePickerMode = .date
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        //let cancelButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton], animated: false)
        
        self.txtStartDate.inputAccessoryView = toolbar
        self.txtEndDate.inputAccessoryView = toolbar
        
        teamBarChartView.displayConfig.titleFont = UIFont (name: "HelveticaNeue-Light", size: 13)
        teamBarChartView.displayConfig.titleLength = 14
        self.categoryChartView.clear()
        self.teamChartView.clear()
        
        
        let nextMonth = Calendar.current.date(byAdding: .month, value: -6, to: Date())
       
        
        if let startDate = nextMonth?.toString(dateFormat: "yyyy-MM-dd"){
            
            self.startDate = startDate
            self.stDate = nextMonth!.toString(dateFormat: "MMM d, yyyy")
            
        }
        
         let endDt = Date()
            
        if endDt.toString(dateFormat: "yyyy-MM-dd") != nil{
            
            self.endDate = endDt.toString(dateFormat: "yyyy-MM-dd")
            
            self.edDate = endDt.toString(dateFormat: "MMM d, yyyy")
            
            self.lblDate.text = self.stDate! + " to " + self.edDate!
            
        }
        
 
            
        if let primary = Defaults.string(forKey: "primary"){
            
            if primary == "personal"{
                
                self.teamExpView.isHidden = true
                self.btnFilter.isHidden = true
                self.rejectedView.isHidden = true
                self.approvedView.isHidden = true
                self.paidView.isHidden = true
                self.lblRequestTxt.text = "Expenses"
                
                if let currency = Defaults.string(forKey: "personalCurrency"){
                    self.currency = currency
                }
                
                
            }else{
                
                self.btnFilter.isHidden = false
                self.teamExpView.isHidden = false
                self.rejectedView.isHidden = false
                self.approvedView.isHidden = false
                self.paidView.isHidden = false
                self.lblRequestTxt.text = "Requests"
                if let currency = Defaults.string(forKey: "teamCurrency"){
                    self.currency = currency
                }
            }
            
            
        }
            
 
        getDashBoards()

    }
    
    @objc func donedatePicker(){

        self.view.endEditing(true)
        
    }
    
    @objc func datePickerFromValueChanged(_ sender: UIDatePicker){
        
        
        
        let selectedDate: String = dateFormatter.string(from: sender.date)
        self.txtStartDate.text = selectedDate
        
        let startDateFormatter: DateFormatter = DateFormatter()
        startDateFormatter.dateFormat = "yyyy-MM-dd"
        let startingDate: String = startDateFormatter.string(from: sender.date)
        self.startDate = startingDate
        
        let stDateFormatter: DateFormatter = DateFormatter()
        stDateFormatter.dateFormat = "MMM d, yyyy"
        self.stDate = stDateFormatter.string(from: sender.date)
    
    }
    
    @objc func dateEndPickerFromValueChanged(_ sender: UIDatePicker){
    
        let selectedDate: String = dateFormatter.string(from: sender.date)
        self.txtEndDate.text = selectedDate
        
        let startDateFormatter: DateFormatter = DateFormatter()
        startDateFormatter.dateFormat = "yyyy-MM-dd"
        let startingDate: String = startDateFormatter.string(from: sender.date)
        self.endDate = startingDate
        
        let stDateFormatter: DateFormatter = DateFormatter()
        stDateFormatter.dateFormat = "MMM d, yyyy"
        self.edDate = stDateFormatter.string(from: sender.date)
       
    }
    
    
    @IBAction func cancelRangePressed(_ sender: Any) {
        self.dateRangeView.isHidden = true
        self.txtEndDate.text = ""
        self.txtStartDate.text = ""
    }
    
    @IBAction func dateRangeSelection(_ sender: Any) {
        
        self.dateRangeView.isHidden = false
    }
    
    
    func loadCoreChartData() -> [CoreChartEntry] {
        
        return getTurkeyFamouseCityList()
        
    }
    
    
    func getTurkeyFamouseCityList()->[CoreChartEntry] {
        var expData = [CoreChartEntry]()

        for index in 0..<self.months.count {
            
            print(self.expValues[index])
            let newEntry = CoreChartEntry(id: "\(self.expValues[index])", barTitle: self.months[index], barHeight:self.expValues[index], barColor: #colorLiteral(red: 0, green: 0.4078314304, blue: 0.4290987253, alpha: 1))
            expData.append(newEntry)


        }
        
        return expData
        
    }
    
    
    @IBAction func rangeSubmit(_ sender: Any) {
        
        guard let txtDate = self.txtStartDate.text, let txtEndDate = self.txtEndDate.text else{
            
            Alertift.alert(title: AppTitle, message:validDate)
            .action(.default("OK"))
            .show(on: self)
            return
        }
    
        guard let stDate = self.startDate, let edDate = self.endDate else{
    
            return
        }
    
        guard let orgId = Defaults.string(forKey: "primaryID") else{
            return
        }
        
        self.lblDate.text = self.stDate! + " to " + self.edDate!
        self.getDashbord(startDate:stDate, endDate: edDate, type: self.currentRole!,organisation: orgId)
        self.dateRangeView.isHidden = true
        self.txtEndDate.text = ""
        self.txtStartDate.text = ""
        self.donedatePicker()
    }
    
    
    @IBAction func filterPressed(_ sender: Any) {
        
        self.filterView.isHidden = false
    }
    
    @IBAction func filterCancelPressed(_ sender: Any) {
        
        self.filterView.isHidden = true
    }
    
    
    func getDashBoards(){
        
        guard let orgId = Defaults.string(forKey: "primaryID") else{
            return
        }
        
        SVProgressHUD.show()
    
        
        DashBoard.getMyDashBoard(withParameter: ["organisation":orgId]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.MyDashBoardResponse?.data{
                    
                    if info.count > 0 {
                        
                        self.dashboardTypes = info
                        var titleStr = ""
                        if let title =  Defaults.string(forKey: "primaryName"){
                            titleStr = title
                        }
                     
                        if self.filterArray.count > 0{
                            self.filterArray.removeAll()
                        }
                        
                        
                        for data in self.dashboardTypes {
                        
                        if (data.contains("accountant")){
                        
                            self.currentRole = "accountant"
                            self.lblTitle.text = titleStr + " - As Accountant"
                            self.filterArray.append("As Accountant")
                        
                        }else if ((data.contains("supervisor"))){
                        
                            self.currentRole = "supervisor"
                            self.lblTitle.text = titleStr + " - As Approver"
                            self.filterArray.append("As Approver")
                        
                        }
                        else if ((data.contains("reviewer"))){
                        
                            self.currentRole = "reviewer"
                            self.lblTitle.text = titleStr + " - As Reviewer"
                            self.filterArray.append("As Reviewer")
                        }
                        else if ((data.contains("employee"))){
                        
                            self.currentRole = "employee"
                            self.lblTitle.text = titleStr + " - My Expenses"
                            self.filterArray.append("My Expenses")
                        }
                        else{
                                                            
                            self.lblTitle.text = "Personal"
                            self.currentRole = "personal"
                                                            
                        }
                    }
                        
                        if self.filterArray.count > 1{
                            self.reloadFilter()
                            self.btnFilter.isHidden = false
                        }else{
                            self.btnFilter.isHidden = true
                        }
                        
                        
              
                        guard let stDate = self.startDate, let edDate = self.endDate else{
                            
                            return
                        }
                    
                        guard let orgId = Defaults.string(forKey: "primaryID") else{
                            return
                        }
                        
                        self.getDashbord(startDate:stDate, endDate: edDate, type: self.currentRole!,organisation: orgId)
                        
                        
                    }
                    
                   
                }
                
            }
            
            
            
        }
        
        
    }
    
    func reloadFilter(){
        
        
        DispatchQueue.main.async {
        
            self.filterTableView.delegate = self
            self.filterTableView.dataSource = self
            self.filterTableView.reloadData()
            
        }
        
        
        
    }
    

    
    
    func getDashbord(startDate:String,endDate:String,type:String,organisation:String){
        
        SVProgressHUD.show()
        
        let param = DashBoardParams(startDate: startDate, endDate: endDate, type: type,organisation: organisation).Values
        
        DashBoard.getDashBoard(withParameter:param) { (isSuccess, message) in
            
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.DashBoardResponse?.data{
                
                    
                    if let primary = Defaults.string(forKey: "primary"){
                        
                        if primary == "personal"{
                            
                            self.lblRequestCount.text = "\(info.totalReimbursements ?? 0)"
                            self.lblRequestAmount.text = self.currency.getCurrencySymbol(currencyCode: self.currency) + "\(info.totalExpense ?? 0)"                             //self.currency + "\(info.totalExpense ?? 0)"
                            
                        }
                        else{
                    
                    if let reimberse = info.reimbursements{
               
                        
                        self.lblRequestCount.text = "\(reimberse.requested?.items ?? 0)"
                        self.lblRequestAmount.text = self.currency.getCurrencySymbol(currencyCode: self.currency) + "\(reimberse.requested?.amount ?? 0)"
                        
                        self.lblPaidCount.text =  "\(reimberse.paid?.items ?? 0)"
                        self.lblPaidAmount.text = self.currency.getCurrencySymbol(currencyCode: self.currency) + "\(reimberse.paid?.amount ?? 0)"
                        
                        self.lblReviewCount.text =  "\(reimberse.reviewed?.items ?? 0)"
                        self.lblReviewAmount.text = self.currency.getCurrencySymbol(currencyCode: self.currency) + "\(reimberse.reviewed?.amount ?? 0)"
                       
                    
                        if let appCount = reimberse.approved?.items{
                            self.lblApprovedCount.text =  "\(appCount)"
                        }else{
                            
                            self.lblApprovedCount.text =  "\(0)"
                        }
                        
                      
                        self.lblApprovedAmount.text = self.currency.getCurrencySymbol(currencyCode: self.currency) + "\(reimberse.approved?.amount ?? 0)"
                        
                        
                        self.lblRejectCount.text =  "\(reimberse.rejected?.items ?? 0)"
                        self.lblRejectAmount.text = self.currency.getCurrencySymbol(currencyCode: self.currency) + "\(reimberse.rejected?.amount ?? 0)"
                        
                    }
                        }
                }
                    
                    if let teamOverView = info.teamOverview{

                        var categories = [String]()
                        var catValues = [Double]()

                        for data in teamOverView{
                            
                            if Double(data.spend!) ?? 0 > 0{

                            categories.append(data.name)
                            catValues.append(Double(data.spend!) ?? 0)
                            }

                        }
                       
                   
                        if categories.count > 0{
                            
                            DispatchQueue.main.async {
                                
                                if catValues.sum() == 0{
                                   
                                    categories = ["No Data"]
                                    catValues = [1]
                                }
                               
                                self.teamChartView.layers = [self.createTextWithLinesLayer()]
                                self.teamChartView.models = self.createModels(item:categories,value: catValues, chartTag: 1)
                                                            }

                           
                        }else{
                            
                            self.emptyLblTwo.isHidden = false
                        }


                    }
                    
                    
                    if let categoryView = info.categoryOverview{

                        var categories = [String]()
                        var catValues = [Double]()

                        for data in categoryView{
                            
                            
                            if Double(data.spend!) ?? 0 > 0{

                            categories.append(data.categoryName ?? "")
                            catValues.append(Double(data.amount!) ?? 0)
                            }

                        }

               
                        if categories.count > 0{
                            
                            DispatchQueue.main.async {
                             
                                if catValues.sum() == 0{
                                    categories = ["No Data"]
                                    catValues = [1]
                                }

                             
                                self.categoryChartView.layers = [self.createTextWithLinesLayer()]
                                self.categoryChartView.models = self.createModels(item:categories,value: catValues, chartTag: 0)
                               // self.categoryChartView.reloadInputViews()
                            }

                            

                        }else{
                            
                            self.emptyLblOne.isHidden = false
                        }

                    }
                    

                    if let monthlyExpenses = info.monthlyExpense{

                        var months = [String]()
                        var expValues = [Double]()

                        for data in monthlyExpenses{

                            months.append(data.xlabel1)
                            expValues.append(Double(data.spend) ?? 0)


                        }

                        self.months = months
                        self.expValues = expValues

                        if self.months.count > 0{

                            DispatchQueue.main.async {
                                
                                DispatchQueue.main.async {
                                
                                    self.teamBarChartView.dataSource = self
                                    self.teamBarChartView.reload()
                                }

                               

                            }


                        }



                    }
                    
                  
                }
                
               
                
            }
            
        }
        
        
    }
    
    
    
    
    
    
    fileprivate func createModels(item:[String],value:[Double],chartTag:Int) -> [PieSliceModel] {


        switch chartTag {
        case 0:

            var model = [PieSliceModel]()

            for i in 0...item.count - 1 {

                model.append(PieSliceModel(value: abs(value[i]), color: colors[i], obj: item[i]))

            }


            currentColorIndex = model.count
            return model


        case 1:

            
            

            var model = [PieSliceModel]()

            for i in 0...item.count - 1 {

                model.append(PieSliceModel(value: abs(value[i]), color: colors[i], obj: item[i]))

            }


            currentColorIndex = model.count
            return model



        default:
            break
        }


        let model = [PieSliceModel]()

        let models = [
            PieSliceModel(value: abs(value[0]), color: colors[4], obj: item[0]),
            PieSliceModel(value: abs(value[1]), color: colors[1], obj: item[1]),

              ]

              currentColorIndex = models.count
              return model



    }
    
    
    fileprivate func createPlainTextLayer() -> PiePlainTextLayer {
          
          let textLayerSettings = PiePlainTextLayerSettings()
          textLayerSettings.viewRadius = 50
          textLayerSettings.hideOnOverflow = true
          textLayerSettings.label.textColor = UIColor.black
          textLayerSettings.label.font = UIFont.systemFont(ofSize: 10)
          
          let formatter = NumberFormatter()
          formatter.maximumFractionDigits = 1
          textLayerSettings.label.textGenerator = {slice in
              return  (slice.data.model.obj as? String ?? "") //formatter.string(from: slice.data.percentage * 100 as NSNumber).map{"\($0)%"} ?? ""
          }
          
          let textLayer = PiePlainTextLayer()
          textLayer.settings = textLayerSettings
          return textLayer
      }
      
      fileprivate func createTextWithLinesLayer() -> PieLineTextLayer {
          let lineTextLayer = PieLineTextLayer()
          var lineTextLayerSettings = PieLineTextLayerSettings()
        lineTextLayerSettings.lineColor = UIColor.black
        lineTextLayerSettings.label.textColor = UIColor.black
        lineTextLayerSettings.label.bgColor = .white
          let formatter = NumberFormatter()
          formatter.maximumFractionDigits = 1
          lineTextLayerSettings.label.font = UIFont.systemFont(ofSize: 10)
        lineTextLayerSettings.label.textGenerator = {slice in
        
            return  (slice.data.model.obj as? String ?? "0")  //formatter.string(from: slice.data.model.obj as String).map{"\($0)"} ?? ""
          }
          
          lineTextLayer.settings = lineTextLayerSettings
          return lineTextLayer
      }
    


}

extension DashBoardViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       // if tableView == filterTableView{
            
            let cell = self.filterTableView.dequeueReusableCell(withIdentifier: FilterTableCell, for: indexPath) as! FilterCell
            
             cell.lblTitle.text = self.filterArray[indexPath.row]
            
            
            return cell
       // }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let stDate = self.startDate, let edDate = self.endDate else{
            
            return
        }
        
        
        self.categoryChartView.clear()
        self.teamChartView.clear()
        
        if filterArray[indexPath.row] != nil{

        let role = filterArray[indexPath.row]

            self.currentRole = role
            
            guard let id = Defaults.string(forKey: "primaryID") else{
                return
            }

            self.getDashbord(startDate:stDate, endDate: edDate, type: self.currentRole!,organisation: id)


        }
        
        self.lblTitle.text = self.filterArray[indexPath.row]
        
        self.filterView.isHidden = true
        
        
       
        
    }
    
    
    
    
    
}

extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}
