//
//  MembersListViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 23/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD

class MembersListViewController: BaseViewController {
    
    var DashBoard = ResponseHandler()
    var MembersList:[MemberData]?
    var organisationId:String?

    @IBOutlet weak var teamTableView: UITableView!{
        didSet {
            teamTableView.tableFooterView = UIView(frame: .zero)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getMemberList()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    func getMemberList(){
        
        guard let id = organisationId else {
           return
        }
        SVProgressHUD.show()
        
        let urlStr = Api.organisationDetails + id
        DashBoard.getMembersList(url:urlStr,withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.MembersResponse{
                    
                    if self.MembersList?.count ?? 0 > 0{
                        self.MembersList?.removeAll()
                    }
                    
                    if info.members?.count ?? 0 > 0 {
                        self.MembersList = info.members
                        
                    
                        self.teamTableView.delegate = self
                        self.teamTableView.dataSource = self
                        self.teamTableView.reloadData()
                    

                    }
                   
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    

}
extension MembersListViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.MembersList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.teamTableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! CompanyTableViewCell
        if let info = self.MembersList?[indexPath.row]{
            
            cell.lblTitle.text = info.name
        
    }
    return cell
    }
}
