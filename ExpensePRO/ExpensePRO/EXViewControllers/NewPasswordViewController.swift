//
//  NewPasswordViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 20/10/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alertift
import TextFieldEffects

class NewPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtOtp: HoshiTextField!
    
    @IBOutlet weak var txtPassword: HoshiTextField!
    
    var Register = ResponseHandler()
    
    var email:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        guard  let token = self.txtOtp.text, let password = self.txtPassword.text, !token.isEmpty,!password.isEmpty else{
            
            self.showNotification(message:mandatoryFields)
            
            return
        }
        
        guard let email = self.email else{
            
            return
        }
        
        updatePassword(email: email, token: token, password: password)
        
    }
    
    
    
    
    func updatePassword(email:String,token:String,password:String) {
        
        
        SVProgressHUD.show()
        
        Register.UpdatePasswordToken(withParameter:["email":email,"token":token,"password":password,"confirm_password":password]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
                
                DispatchQueue.main.async {
                    
                    Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                    
                }
                
         
                let NewScene = SignInViewController.instantiate(fromAppStoryboard: .Main)
                
               
                                 
                                 if let navigator = self.navigationController {
                                
                                 
                                     navigator.pushViewController(NewScene, animated: true)
                                 
                          
                              }

                       
               
            }else{
                
           
                            Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
              
                    }
                        
                    }
            }
            
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
        
        
        
    }
    

   

