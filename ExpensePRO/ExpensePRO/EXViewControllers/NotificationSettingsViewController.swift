//
//  NotificationSettingsViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 29/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alertift

class NotificationSettingsViewController: BaseViewController {
    
    @IBOutlet weak var notificationTableView: UITableView!
    
    var data = ["Email ON","Mobile ON"]
    var Inbox = ResponseHandler()
    
    var notification:Bool = true
    var email:Bool = true
    var mobStatus = "true"
    var emailStatus = "true"
     

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.notificationTableView.tableFooterView = UIView(frame: .zero)
        self.notificationTableView.delegate = self
        self.notificationTableView.dataSource = self
        
        if Defaults.bool(forKey: "emailNotification") != nil{
            self.email = Defaults.bool(forKey: "emailNotification")
        }
        
        if Defaults.bool(forKey: "mobNotification") != nil{
            self.notification = Defaults.bool(forKey: "mobNotification")
        }
        
       // print(self.notification,self.email)

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    

    func enableNotify(notification:String,email:String){
        
   
            
            SVProgressHUD.show()
            
             Inbox.EnableNotify(withParameter:["notification":self.notification,"email":self.email]) { (isSuccess, message) in
                   SVProgressHUD.dismiss()
                   if (isSuccess){
                       
                   // self.backNavigation()
                       
                   }else{
                    
                    Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                    
                   }
            
        }
            
            
     
        
        
    }

}
extension NotificationSettingsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.notificationTableView.dequeueReusableCell(withIdentifier: "NotificationSetting", for: indexPath) as! NotificationSettingCell
        
        cell.lblTitle.text = data[indexPath.row]
        cell.btnSwitch.tag = indexPath.row
        if indexPath.row == 0{
            
            cell.btnSwitch.isOn = self.email
            
        }
        
        if indexPath.row == 1{
            
            cell.btnSwitch.isOn = self.notification
            
        }
       
        cell.delegate = self
        
        return  cell
        
        
    }
    
    
    
    
    
    
}
extension NotificationSettingsViewController:NotificationSettingDelegate{
    func swtichSelected(tag: Int, isEnable:Bool) {
        
        if tag == 0{
            
            if isEnable{
                
                self.email = true
                self.emailStatus = "true"
                Defaults.set(true, forKey: "emailNotification")
            }else{
                
                self.email = false
                self.emailStatus = "false"
                Defaults.set(false, forKey: "emailNotification")
            }
            
            
        }
        
        if tag == 1{
            
            if isEnable{
                
               
                self.notification = true
                self.mobStatus = "true"
                Defaults.set(true, forKey: "mobNotification")
                
               
            }else{
                
                self.notification = false
                self.mobStatus = "false"
                Defaults.set(false, forKey: "mobNotification")
            }
            
            
        }
        
        self.enableNotify(notification: self.mobStatus, email: self.emailStatus)
       
    }
    
    
    
    
}
