//
//  PackageDetailsViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 13/10/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import Razorpay

class PackageDetailsViewController: UIViewController {
    
    var razorpay: RazorpayCheckout!
    
    
    
    @IBOutlet weak var lblPackageName: UILabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblUsers: UILabel!
    @IBOutlet weak var lblReceipts: UILabel!
    @IBOutlet weak var lblTrialPeriod: UILabel!
    @IBOutlet weak var lblDasboard: UILabel!
    @IBOutlet weak var lblNotifications: UILabel!
    @IBOutlet weak var lblExReport: UILabel!
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var lblTeamFlow: UILabel!
    @IBOutlet weak var lblTeams: UILabel!
    @IBOutlet weak var lblReimbursement: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblSupport: UILabel!
    
    
    var packages:PlanStruct?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        razorpay = RazorpayCheckout.initWithKey(RazorPayKey, andDelegate: self)
    
       
        if let info = self.packages{
            
            if let title = info.name{
                
                self.lblPackageName.text = title
            }
            
            
            if let price = info.price?.text{
                
                self.lblPrice.text = price
            }
            
            
            if let users = info.users?.text{
            
                self.lblUsers.text = users
            }
            
            if let receipts = info.receipts?.text{
            
                self.lblReceipts.text = receipts
            }
            
            if let trials = info.trial?.text{
            
                self.lblTrialPeriod.text = trials
            }
            
            if let dash = info.dashboard{
            
                self.lblDasboard.text = dash
            }
            
            
            if let notification = info.notifications{
            
                self.lblNotifications.text = notification
            }
            
            if let reportexp = info.report{
            
                self.lblExReport.text = reportexp
            }
            
            if let cat = info.categories?.text{
            
                self.lblCategories.text = cat
            }
            
            if let flow = info.teamFlow{
            
                self.lblTeamFlow.text = flow
            }
            
            if let noTeams = info.teams?.text{
            
                self.lblTeams.text = noTeams
            }
            
            if let imburseFlow = info.reimbursementFlow{
            
                self.lblReimbursement.text = imburseFlow
            }
            
            if let comments = info.comments{
            
                self.lblComments.text = comments
            }
            
            if let support = info.support{
            
                self.lblSupport.text = support
            }
            
            
            
            
            
        }

       
    }
    
    
    
    @IBAction func payPressed(_ sender: Any) {
        
        if let info = self.packages{
            
            guard let price = info.price?.value else{
                
                return
                
            }
            
            guard let package = info.name else {
                
                return
            }
            
            let priceStr = String(format: "%.2f", (Double(price) * 100))
                   
                       
                           if let priceNumber = NumberFormatter().number(from: priceStr) {
                               let priceValue = priceNumber.intValue
                            
                            self.showPaymentForm(price:"\(priceValue)" , package: package)
                           }
            
            
            
            
            
        }
        
        
    }
    
    
    internal func showPaymentForm(price:String,package:String){
        
    
        let logo = UIImage(named: "logo_color")
        
        let options: [String:Any] = [
                    "amount": price, //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "USD",//We support more that 92 international currencies.
                    "description":package,
                    "image": logo,
                    "name": AppTitle,
                    "theme": [
                    "color": "#04666C"
                    ]
                ]
        razorpay.open(options)
    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    

}


extension PackageDetailsViewController:RazorpayPaymentCompletionProtocol{
    
    public func onPaymentError(_ code: Int32, description str: String){
        
        
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }

    public func onPaymentSuccess(_ payment_id: String){
        
        
       print(payment_id)
        
        
//        let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: .alert)
//        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    
}
