//
//  SignUpViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 5/17/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import UIKit
import TextFieldEffects
import MRCountryPicker
import SVProgressHUD
import Alertift

class SignUpViewController: BaseViewController {
    
    
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtComapny: HoshiTextField!
    @IBOutlet weak var txtPhone: HoshiTextField!
    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var txtCurrency: HoshiTextField!
    @IBOutlet weak var txtOtp: HoshiTextField!
    
    
    @IBOutlet weak var otpView: UIView!
    var Register = ResponseHandler()
    
    var codePicker = MRCountryPicker()
    var tokenStr:String? = nil
    var registerParams = [String:Any]()
    var name:String?
    var email:String?
    var appleId:String?
    var isApple:Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
       // listCountriesAndCurrencies()

    }
    
    func initialSetup(){
        
    
        
        if let name = self.name{
            self.txtName.text = name
        }
        
        if let email = self.email{
            
            self.txtEmail.text = email
        }
        
        
        otpView.isHidden = true
        codePicker.countryPickerDelegate = self
        codePicker.showPhoneNumbers = false
        self.txtCurrency.inputView = codePicker
       // listCountriesAndCurrencies()
    }
    
    
    func listCountriesAndCurrencies() {
        let localeIds = Locale.availableIdentifiers
        var countryCurrency = [String: String]()
        for localeId in localeIds {
            let locale = Locale(identifier: localeId)
            
          //  print(localeId)
                       
            if let country = locale.currencyCode{
                if let currency = locale.currencySymbol {
                    countryCurrency[country] = currency
                }
            }
        }

        let sorted = countryCurrency.keys.sorted()
        for country in sorted {
            let currency = countryCurrency[country]!

            print("country: \(country), currency: \(currency)")
        }
    }
    
    @IBAction func closeOtpPressed(_ sender: Any) {
        self.otpView.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitOtpPressed(_ sender: Any) {
        
        
        guard let otp = self.txtOtp.text, !otp.isEmpty else{
            
            Alertift.alert(title: AppTitle, message:otpNotValid)
            .action(.default("OK"))
            .show(on: self)
            return
            
        }
        
        guard let email = self.txtEmail.text else {
            
            return
            
        }
        
   
        
        self.verifyOtp(email: email, otp: otp)
    }
    
    @IBAction func resendOtpPressed(_ sender: Any) {
        
        guard let email = self.txtEmail.text else {
            
            return
            
        }
        
        self.generateOtp(email: email)
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    
    @IBAction func signUpPressed(_ sender: Any) {
        
        
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
        txtPhone.resignFirstResponder()
        txtComapny.resignFirstResponder()
        txtName.resignFirstResponder()
        txtCurrency.resignFirstResponder()
        
        
               if isConnected(){
                   
                  
                          
                guard let textCurrency = self.txtCurrency.text,let textCompany = self.txtComapny.text,let textPhone = self.txtPhone.text,let textEmail = self.txtEmail.text, let textPwd = self.txtPassword.text,let textName = self.txtName.text,!textPhone.isEmpty,!textName.isEmpty,!textEmail.isEmpty,!textPwd.isEmpty,!textCompany.isEmpty,!textCurrency.isEmpty else {
                             
                    self.showNotification(message:mandatoryFields)
                             
                              return
                          }
                   
                if textEmail.isValidEmail(){
                    
                    if textPhone.count < 6{
                                   
                                   self.showNotification(message: notValidPhone)
                                   return
                               }
                               
                    if textPwd.count < 6{
                                              
                                   self.showNotification(message: passwordMessage)
                                   return
                    }
                    
                    
                   
                   
                    let params = ProfileParams(name: textName, email: textEmail, mobile: textPhone, password: textPwd, role: "", notifyToken: Defaults.string(forKey: "deviceToken")  ?? "", device: "ios", organisation: textCompany).Values
                    
                    self.registerParams = params
                    
                    self.updateProfile()
           
                   }else{
                       
                    self.showNotification(message:validEmail)
                       
                   }
                   
                   
                   
               }
               
        
   
    }
    
    
    
    
    
    
    
    func updateProfile(){
  
                
                guard let email = self.txtEmail.text else {
                    
                    return
                    
                }
         
                self.generateOtp(email: email)
                
            }
    

    
    
    
    func generateOtp(email:String){
        
   
        
        SVProgressHUD.show()
        
        Register.SendOtp(withParameter:["email":email]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
                        self.otpView.isHidden = false
                    
               
            }else{
                
                
                
                if let isRegistered = self.Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        DispatchQueue.main.async {
                            
                            Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
                            
                            
                            
                        }
                        
                        self.backNavigation()
                       
                    }
                        
                    }else{
                        
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                    }
                
               
                

            }
            
            
                
            }
        
    }
    
    
    
    
    func doRegister(param:[String:Any]){
        
        
                SVProgressHUD.show()
        
                Register.RegisterUserProfile(withParameter:param) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
        
                    if isSuccess{
                        
                                        if let info = self.Register.loginResponse?.data{
                        
                                            if let token = info.token{
                        
                        
                                                self.tokenStr = token
                                                Defaults.set(self.tokenStr,forKey:UserToken)
                                                Switcher.updateRootVC()

                        
                        
                                            }
                        
                        
                                        }
          
                                 }
                        else{
                    
                                    Alertift.alert(title: AppTitle, message:message)
                                    .action(.default("OK"))
                                    .show(on: self)
                    
                    
                                }
                    
                                
                    
                                }
                    
                        }
    
    
    func verifyOtp(email:String,otp:String){
        
      
        
        SVProgressHUD.show()
        
        Register.VerifyOtp(withParameter:["email":email,"otp":otp]) { [self] (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
            
                self.doRegister(param: self.registerParams)
               
            }else{
                
            
                
              if let isRegistered = Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                        
                        self.backNavigation()
                        
                    }
                    
                    
              }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
                
              }
                

            }
            
            
                
            }
        
    }
    
    
    

   
}

extension SignUpViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= 10
    }
}

extension SignUpViewController: MRCountryPickerDelegate {
    

      func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
       
        
        if let currency = Locale.currency[countryCode]?.code{
        
            self.txtCurrency.text = currency
            
        }
        
      
             
         }
         
    
    
}

