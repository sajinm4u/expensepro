//
//  SignInViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 5/17/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import TextFieldEffects
import Alertift
import GoogleSignIn
import AuthenticationServices


class SignInViewController: BaseViewController {
    
    
    var loginModel = ResponseHandler()
    
    @IBOutlet weak var btnAppleSignIn: signButton!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtPassword: HoshiTextField!
    var googleUser:GIDGoogleUser?
    var userID:String = ""
    var userEmail:String = ""
    var userName:String = ""
    var deviceString:String = "iOS"
    var notifyString:String = Defaults.string(forKey: "deviceToken")  ?? ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    

    
    
    @IBAction func appleSignInPressed(_ sender: Any) {
        
        if #available(iOS 13.0, *) {
            
            handleAuthorizationAppleID()
                   
//                    self.btnAppleSignIn.addTarget(self, action: #selector(handleAuthorizationAppleID), for: .touchUpInside)
                   
               }
        
    }
    
     func handleAuthorizationAppleID() {
        
        
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName,.email]
                          
                          let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                          authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
                          authorizationController.performRequests()
        } else {
            
            Alertift.alert(title:AppTitle , message: "iOS version not supported" )
                           .action(.default("OK"))
                           .show(on: self)
           
        }
              
    }
    
    
    @IBAction func googleSignInPressed(_ sender: Any) {
        
        if let googleUserID = self.googleUser?.userID {


            self.userID = googleUserID



            if let googleUserEmail = googleUser?.profile.email{

                self.userEmail = googleUserEmail

            }

            if let googleUserName = googleUser?.profile.name {

                self.userName = googleUserName

            }

            if let authToken = googleUser?.authentication.accessToken{

                
                let NewScene = SetupPersonalVC.instantiate(fromAppStoryboard: .Main)
                
                NewScene.name = self.userName
                NewScene.email = self.userEmail
                                 
                                 if let navigator = self.navigationController {
                                
                                 
                                     navigator.pushViewController(NewScene, animated: true)
                                 
                          
                              }
                


                       }



           }


        else{

            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance()?.presentingViewController = self
            GIDSignIn.sharedInstance()?.signIn()

    }

        
        
    }
    
    
    
    
    
    @IBAction func forgotPressed(_ sender: Any) {
        
        let NewScene = ForgotPasswordViewController.instantiate(fromAppStoryboard: .Main)
        
       
                         
                         if let navigator = self.navigationController {
                        
                         
                             navigator.pushViewController(NewScene, animated: true)
                         
                  
                      }
        
        
    }
    
    
    func signWithApple(userId:String,email:String){
        
        SVProgressHUD.show()
        
        let params = ["appleId":userId]
        
        loginModel.LoginWithApple(withParameter: params) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.loginModel.loginResponse?.data{
                    
                    if info.isRegistered ?? false{
                        
                        if let token = info.token{
                            
                            Defaults.set(token,forKey:UserToken)
                            
                        }
                        
                    
                         Switcher.updateRootVC()
                    }
                    
                    else{


                        let NewScene = SetupPersonalVC.instantiate(fromAppStoryboard: .Main)

                        NewScene.name = self.userName
                        NewScene.email = self.userEmail
                        NewScene.appleId = userId
                        NewScene.isApple = true
                        


                        DispatchQueue.main.async {


                            Alertift.alert(title: AppTitle, message:notRegister)
                            .action(.default("OK"))
                            .show(on: self)

                        }

                                         if let navigator = self.navigationController {

                                             navigator.pushViewController(NewScene, animated: true)


                                      }




                    }
                    
                    
                }

                    
                }
                
            }
        
        
        
    }
    
    
    
    
    @IBAction func signUpPressed(_ sender: Any) {
        
        
        let NewScene = SetupPersonalVC.instantiate(fromAppStoryboard: .Main)
        NewScene.isApple = false

                         if let navigator = self.navigationController {
                        
                         
                             navigator.pushViewController(NewScene, animated: true)
                         
                      }
        

    }
    
    
    

    @IBAction func signInPressed(_ sender: Any) {
        
        txtPassword.resignFirstResponder()
        txtEmail.resignFirstResponder()
        
        if  Connectivity.isConnectedToInternet {
            
            guard let txtEmail = self.txtEmail.text, let txtPassword = self.txtPassword.text, !txtEmail.isEmpty, !txtPassword.isEmpty else {
                
                Alertift.alert(title: AppTitle, message:validUsername)
                    .action(.default("OK"))
                    .show(on: self)
                
                return
            }
                
              
            if !txtEmail.isValidEmail(){
                
                Alertift.alert(title: AppTitle, message:validEmail)
                .action(.default("OK"))
                .show(on: self)
                
            }
                
                
                
            else{
                
                let params = [
                    "email": txtEmail,
                    "password": txtPassword,
                    
                    ] as [String : String]
                
                SVProgressHUD.show()
                
                loginModel.doLogin(withParameter: params) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
                    if (isSuccess){
                        
                        if let info = self.loginModel.loginResponse?.data{
                            
                            if let token = info.token{
                                
                                Defaults.set(token,forKey:UserToken)
                                
                            }
                            
             
                             Switcher.updateRootVC()
                            

                            
                        }
                        
                    }else{
                        
                        
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                        
                        
                    }
                    
                    
                    
                }
                
                
                
            }
            
            
        }else{
            
            Alertift.alert(title: AppTitle, message: noInternet)
                .action(.default("OK"))
                .show(on: self)
            
        }

        
        
        
        
    }
    
}

extension SignInViewController: GIDSignInDelegate{

func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
       
       
       
       if let error = error {
           if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
               print("The user has not signed in before or they have since signed out.")
           } else {
               print("\(error.localizedDescription)")
           }
           return
       }
    
    
    
    
    self.googleUser = user
    
    
    
    
        if let googleUserID = self.googleUser?.userID {
            
    
            self.userID = googleUserID
            
            
            
            if let googleUserEmail = googleUser?.profile.email{
                
                self.userEmail = googleUserEmail
                
            }
            
            if let googleUserName = googleUser?.profile.name {
                
                self.userName = googleUserName
                
            }
            
            
          
            
         //   googleUser?.authentication.aut
            
    
            
            if let authToken = googleUser?.authentication.idToken{

                SVProgressHUD.show()
                
                let params = ["email":self.userEmail,"name":self.userName]
                
                loginModel.LoginWithGoogle(withParameter: params) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
                    if (isSuccess){
                        
                        if let info = self.loginModel.loginResponse?.data{
                            
                            if info.isRegistered ?? false{
                                
                                if let token = info.token{
                                    
                                    Defaults.set(token,forKey:UserToken)
                                    
                                }
                                
                                 Switcher.updateRootVC()
                            }else{
                                
                                
                                let NewScene = SetupPersonalVC.instantiate(fromAppStoryboard: .Main)
                                
                                NewScene.name = self.userName
                                NewScene.email = self.userEmail
                                NewScene.isApple = false
                                
                                DispatchQueue.main.async {
                                    
                                    
                                    Alertift.alert(title: AppTitle, message:notRegister)
                                    .action(.default("OK"))
                                    .show(on: self)
                                    
                                }
                                                 
                                                 if let navigator = self.navigationController {
                
                                                     navigator.pushViewController(NewScene, animated: true)
                
                
                                              }
                                
                                
                                
                                
                            }
                            
                            
                        }

                            
                        }
                        
                    }
                
                
                
   


                       }


                         
                
            }
            
  
            
        }
    
    

       
   }
   

extension SignInViewController:ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding{
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        
                return self.view.window!
    }
    
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let appleIDCredentials = authorization.credential as? ASAuthorizationAppleIDCredential else { return }
        
        
        if Defaults.string(forKey: "AppleUserID") != nil{
            
            guard let userId = Defaults.string(forKey: "AppleUserID") else {return}
           // guard let user =  Defaults.string(forKey: "AppleUserName") else {return}
            
            if let email = Defaults.string(forKey: "AppleUserEmail"){
                self.userEmail = email
            }
            
            
        
            
            self.signWithApple(userId:userId, email:self.userEmail)
//            self.doSocialLogin(accessToken:userId , provider: "apple", name: user, userId: userId, email:email)
                                  
                 
                 
        }else{
            
            let userIdentifier = appleIDCredentials.user
            
            if let name = appleIDCredentials.fullName?.givenName{
                
                self.userName = name
                Defaults.setValue(self.userName, forKey: "AppleUserName")
            }
            
            if let email = appleIDCredentials.email{
                
                self.userEmail = email ?? ""
                Defaults.setValue(self.userEmail, forKey: "AppleUserEmail")
            }
           
           
            
                 self.userID = userIdentifier
                
                 
        
                   
            Defaults.setValue(self.userID, forKey: "AppleUserID")
           
            
                   
            self.signWithApple(userId: self.userID, email: self.userEmail)
            
            
                 //   self.doSocialLogin(accessToken:userIdentifier, provider: "apple", name: fullName, userId: userIdentifier, email:email)
            
            
            
        }
        
        
        
        
        
    }
    
    
}
