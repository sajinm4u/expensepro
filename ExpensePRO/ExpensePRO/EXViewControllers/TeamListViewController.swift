//
//  TeamListViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 09/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import TextFieldEffects
import SVProgressHUD
import Alertift


class TeamListViewController: BaseViewController {
    
    
    var Roles:[RolesData]?
    var DashBoard = ResponseHandler()
    
    @IBOutlet weak var addNewTeamView: UIView!
    @IBOutlet weak var teamTableView: UITableView!{
        didSet {
            teamTableView.tableFooterView = UIView(frame: .zero)
        }
    }
    
    
    @IBOutlet weak var newTeamView: UIView!
    @IBOutlet weak var txtTeamName: HoshiTextField!
    @IBOutlet weak var txtRole: HoshiTextField!
    
    @IBOutlet weak var revInView: CurvedView!
    @IBOutlet weak var manInvView: CurvedView!
    @IBOutlet weak var accInvView: CurvedView!
    @IBOutlet weak var memInvView: CurvedView!
    var teamId:String?
    var organisationId:String?
    var invitaionId:String?
    var pickedRole:String?
    
    struct memberStruct {
        var email:String
        var pending:Bool
    
    }
    @IBOutlet weak var reviewerTableViiew: UITableView!{
        didSet {
            reviewerTableViiew.tableFooterView = UIView(frame: .zero)
        }
    }
    
    @IBOutlet weak var managerTableView: UITableView!{
        didSet {
            managerTableView.tableFooterView = UIView(frame: .zero)
        }
        
    }
    @IBOutlet weak var accountantTableView: UITableView!{
        didSet {
            accountantTableView.tableFooterView = UIView(frame: .zero)
        }
        
    }
    @IBOutlet weak var empTableView: UITableView!{
        didSet {
            empTableView.tableFooterView = UIView(frame: .zero)
        }
        
    }
    
    @IBOutlet weak var txtEmail: HoshiTextField!
    
    @IBOutlet weak var inviteScrollView: UIScrollView!
    
    @IBOutlet weak var inviteView: UIView!
    
    var managerInvData = [RequestModel]()
    var accountInvantData = [RequestModel]()
    var employeeInvData = [RequestModel]()
    
    var invitaionData:[RequestModel]?
    
    var managerData = [memberStruct]()
    var accountantData = [memberStruct]()
    var employeeData = [memberStruct]()
    var reviewerData = [memberStruct]()
    
    var role:String?
    var team:String?
    var orgId:String?
    
    
    
    var rolePicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        self.inviteView.isHidden = true
        self.inviteScrollView.isHidden = true
        self.newTeamView.isHidden = true
        self.txtRole.inputView = rolePicker
        rolePicker.delegate = self
        rolePicker.dataSource = self
        
        if isConnected(){
            
            getRoleList()
            getInvitations()
            
        }
        
    }
    
    
    @IBAction func cancelAddTeamPressed(_ sender: Any) {
        self.newTeamView.isHidden = true
    }
    
    @IBAction func addNewTeamPressed(_ sender: Any) {
        
        self.txtRole.text = ""
        self.newTeamView.isHidden = false
    }
    
    
    @IBAction func cancelInvitationPressed(_ sender: Any) {
        
        self.inviteScrollView.isHidden = true
        
    }
    
    @IBAction func sendInvitation(_ sender: Any) {
        
        guard let email = self.txtEmail.text, !email.isEmpty else {
            
            Alertift.alert(title: AppTitle, message:validEmail)
                .action(.default("OK"))
                .show(on: self)
            
            
            return
        }
        
        if email.isValidEmail(){
            
            self.sendInvitation(role: self.role ?? "", team: self.team ?? "", email: email)
            
        }else{
            
            Alertift.alert(title: AppTitle, message:validEmail)
                .action(.default("OK"))
                .show(on: self)
            
            
        }
        
        
        
    }
    
    
    @IBAction func invitationCancelPressed(_ sender: Any) {
        self.inviteView.isHidden = true
    }
    
    @IBAction func mangerInvitePressed(_ sender: Any) {
        
        self.role = "supervisor"
        self.inviteView.isHidden = false
    }
    
    @IBAction func accountInvitePressed(_ sender: Any) {
        self.role = "accountant"
        self.inviteView.isHidden = false
    }
    @IBAction func empInvitePressed(_ sender: Any) {
        
        self.role = "employee"
        self.inviteView.isHidden = false
    }
    
    @IBAction func reviewerInvitePressed(_ sender: Any) {
        self.role = "reviewer"
        self.inviteView.isHidden = false
    }
    
    func TeamList(teamId:String){
        
        SVProgressHUD.show()
        
        DashBoard.getTeamRoleList(url:Api.TeamList + "/" + teamId ,withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.TeamTypeResponse?.data{
                    
               
                    if let member = info.team?.members{
                        
                        for data in member{
                            
                            if data.role == "supervisor" || data.role == "Supervisor" {
                                
                                if let data = data.user{
                                    
                                    let member = memberStruct(email:data.email ?? "", pending: true)
                                    self.managerData.append(member)
                                }
                          
                            }
                            
                            if data.role == "reviewer" || data.role == "Reviewer" {
                                
                                if let data = data.user{
                                    
                                    let member = memberStruct(email:data.email ?? "", pending: true)
                                    self.reviewerData.append(member)
                                }
                          
                            }
                            
                         
                            if data.role == "employee" || data.role == "Employee"{
                                
                                if let data = data.user{
                                    
                                    let member = memberStruct(email:data.email ?? "", pending: true)
                                    self.employeeData.append(member)
                                   
                                }
                            }
                            
                            if data.role == "accountant" || data.role == "Accountant" {
                                
                                if let data = data.user{
                                    
                                    let member = memberStruct(email:data.email ?? "", pending: true)
                                    self.accountantData.append(member)
                                    
                                }
                        
                            }
                            
                            
                        }
                        
                        
                        if let awaitData = info.awaitingAction{
                        
                        for data in awaitData{
                            
                            if data.role == "supervisor" || data.role == "Supervisor" {
                                
                                if let email = data.email{
                                    
                                    let member = memberStruct(email:email, pending: false)
                                    self.managerData.append(member)
                                }
                                
                                
                                
                            }
                            
                            
                            if data.role == "reviewer" || data.role == "Reviewer" {
                                
                                if let email = data.email{
                                    
                                    let member = memberStruct(email:email ?? "", pending: false)
                                    self.reviewerData.append(member)
                                }
                          
                            }
                            
                            
                            if data.role == "employee" || data.role == "Employee"{
                                
                                if let email = data.email{
                                    
                                    let member = memberStruct(email:email, pending: false)
                                    self.employeeData.append(member)
                                   
                                }
                                
                       
                            }
                            
                            if data.role == "accountant" || data.role == "Accountant" {
                                
                                if let email = data.email{
                                    
                                    let member = memberStruct(email:email, pending: false)
                                    self.accountantData.append(member)
                                    
                                }
                        
                            }
                            
                        }
                        }
                      
                        if self.managerData.count > 0{
                            
                            DispatchQueue.main.async {
                                self.managerTableView.delegate = self
                                self.managerTableView.dataSource = self
                                self.managerTableView.reloadData()
                            }
                            
                            
                            
                        }
                        
                        if self.employeeData.count > 0{
                            
                            DispatchQueue.main.async {
                            self.empTableView.delegate = self
                            self.empTableView.dataSource = self
                            self.empTableView.reloadData()
                            }
                        }
                        
                        if self.reviewerData.count > 0{
                            
                            DispatchQueue.main.async {
                            self.reviewerTableViiew.delegate = self
                            self.reviewerTableViiew.dataSource = self
                            self.reviewerTableViiew.reloadData()
                            }
                        }
                        if self.accountantData.count > 0{
                            
                            DispatchQueue.main.async {
                            self.accountantTableView.delegate = self
                            self.accountantTableView.dataSource = self
                            self.accountantTableView.reloadData()
                            }
                        }
                    }
                    }
                }
                
            }
        }
        
   
    
    func getInvitations(){
        
        SVProgressHUD.show()
        
        DashBoard.getAllInvitationRequests(withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.InvitationRequestResponse?.data{
                  
                    if info.count > 0 {
                        
                        self.invitaionData = info
                        
                        for data in info{
                            
                            print(data)
                            
                            if data.role == "supervisor"{
                                
                                self.managerInvData.append(data)
                                
                            }
                            
                            if data.role == "employee"{
                                
                                self.employeeInvData.append(data)
                                
                                
                            }
                            
                            if data.role == "accountant"{
                                
                                self.accountInvantData.append(data)
                                
                                
                            }
                            
                            
                        }
                        
                        
//                        if self.managerData.count > 0{
//
//                            self.managerTableView.delegate = self
//                            self.managerTableView.dataSource = self
//                            self.managerTableView.reloadData()
//
//
//                        }
//
//                        if self.employeeData.count > 0{
//
//                            self.empTableView.delegate = self
//                            self.empTableView.dataSource = self
//                            self.empTableView.reloadData()
//
//                        }
//
//                        if self.accountantData.count > 0{
//
//                            self.accountantTableView.delegate = self
//                            self.accountantTableView.dataSource = self
//                            self.accountantTableView.reloadData()
//
//                        }
                        
                        
                    }
                    
                    
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    func joinInvitation(id:String){
        
        
        
        SVProgressHUD.show()
        
        DashBoard.JoinRequests(requestId:id,withParameter: ["accepted":"true"]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
             
                self.Roles?.removeAll()
                self.getRoleList()
             
                
            }else{
                
                Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                
            }
            
        }
        
    }
    
    func sendInvitation(role:String,team:String,email:String){
        
        
        
        SVProgressHUD.show()
        
        DashBoard.inviteMember(withParameter: ["team":team,"role":role,"email":email]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                self.inviteView.isHidden = true
                guard let id = self.teamId else {
                    return
                }
                
                self.employeeData.removeAll()
                self.managerData.removeAll()
                self.accountantData.removeAll()
                self.reviewerData.removeAll()
                self.txtEmail.text = ""
                self.TeamList(teamId: id)
                
            }else{
                
                Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                
            }
            
        }
        
    }
    
    
    func getRoleList(){
        
        guard let id = organisationId else{     //Defaults.string(forKey: "primaryID") else {
           return
        }
        
        SVProgressHUD.show()
        
        let urlStr = Api.Roles + "/" + id
        
        DashBoard.getRolesList(url:urlStr,withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.RolesResponse?.data{
                    
                    if info.count > 0 {
                        
                        
                        self.Roles = info
                        self.teamTableView.delegate = self
                        self.teamTableView.dataSource = self
                        self.teamTableView.reloadData()
                        
                        
                    }
                    
                    
                }
                
            }
     
        }
        
        
    }
    
    
    @IBAction func newTeamSubmitPressed(_ sender: Any) {
        
        guard let teamName = self.txtTeamName.text, !teamName.isEmpty else {
            
            Alertift.alert(title: AppTitle, message:"Please Enter Team Name")
            .action(.default("OK"))
            .show(on: self)
            return
        }
        
        
        guard let role = self.pickedRole,!role.isEmpty else{
        
            Alertift.alert(title: AppTitle, message:"Please Select a Role")
            .action(.default("OK"))
            .show(on: self)
            return
        }
        
        guard let orgID = organisationId else{                 //Defaults.string(forKey: "primaryID") else{
            return
        }
            
            self.addNewTeam(teamName:teamName,teamRole:role,orgid: orgID)
            
    
        
    }
    
    
    func addNewTeam(teamName:String,teamRole:String,orgid:String){
        
        SVProgressHUD.show()
        
        DashBoard.addTeams(withParameter: ["name":teamName,"role":teamRole,"organisation":orgid]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                self.newTeamView.isHidden = true
                self.txtTeamName.text = ""
                self.getRoleList()
                
                
            }else{
                
                if message.contains("Please upgrade plan") {
                    
                    var msg = message + " Please use www.xpense.pro to upgrade your plan"
                    
                    Alertift.alert(title: AppTitle, message:msg)
                    .action(.default("OK"))
                    .show(on: self)
                    
                }else{
                    
                    Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                    
                }
                
               
                
            }
    
        }
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }

}
extension TeamListViewController : UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == rolePicker{
            return roleArray.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if row == 0{
            return
        }else{
            
            self.txtRole.text = roleArray[row]
            self.pickedRole = rolesType[row]
        }
        
        
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        return roleArray[row]
       
    }
    
}

extension TeamListViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.teamTableView{
            
            
            guard let id = self.Roles?[indexPath.row]._id else{
                return
            }
            
            if let isAdmin = self.Roles?[indexPath.row].isAdmin{
                
                self.manInvView.isHidden = false
                self.accInvView.isHidden = false
                self.memInvView.isHidden = false
                self.revInView.isHidden = false
                
            }else{
                
                self.manInvView.isHidden = true
                self.accInvView.isHidden = true
                self.memInvView.isHidden = true
                self.revInView.isHidden = true
            }
            self.managerData.removeAll()
            self.employeeData.removeAll()
            self.accountantData.removeAll()
            self.reviewerData.removeAll()
            self.teamId = id
            self.TeamList(teamId: id)
            self.inviteScrollView.isHidden = false
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == self.managerTableView{
            
            let cell = self.managerTableView.dequeueReusableCell(withIdentifier: "inviteList") as! RoleInviteCell
            
            if self.managerData[indexPath.row] != nil{
                
                let info = self.managerData[indexPath.row]
                
                if info.pending{
                    cell.ic_member.image = UIImage(named: "ic_manager")
                }else{
                    
                    cell.ic_member.image = UIImage(named: "ic_teamM")
                    
                }
                
                
                cell.lblTitle.text = info.email
                
          
            }
            
            return cell
            
            
        }
        
        if tableView == self.reviewerTableViiew{
            
            let cell = self.reviewerTableViiew.dequeueReusableCell(withIdentifier: "inviteList") as! RoleInviteCell
            
            if self.reviewerData[indexPath.row] != nil{
                
                    let info = self.reviewerData[indexPath.row]
                
                if info.pending{
                    cell.ic_member.image = UIImage(named: "ic_manager")
                }else{
                    
                    cell.ic_member.image = UIImage(named: "ic_teamM")
                    
                }
   
                cell.lblTitle.text = info.email
      
            }
            
            return cell
            
            
        }
        
        if tableView == self.accountantTableView{
            
            let cell = self.accountantTableView.dequeueReusableCell(withIdentifier: "inviteList") as! RoleInviteCell
            
            if self.accountantData[indexPath.row] != nil{
                
                let info = self.accountantData[indexPath.row]
                
                if info.pending{
                    cell.ic_member.image = UIImage(named: "ic_account")
                }else{
                    
                    cell.ic_member.image = UIImage(named: "ic_accountant")
                    
                }
                
                cell.lblTitle.text = info.email
            
            }
            
            return cell
            
            
        }
        
        if tableView == self.empTableView{
            
            let cell = self.empTableView.dequeueReusableCell(withIdentifier: "inviteList") as! RoleInviteCell
            
            if self.employeeData[indexPath.row] != nil{
                
                let info = self.employeeData[indexPath.row]
                
                if info.pending{
                    cell.ic_member.image = UIImage(named: "ic_emp")
                }else{
                    
                    cell.ic_member.image = UIImage(named: "member")
                    
                }
                
                
                cell.lblTitle.text = info.email
                
                //  }
                
            }
            
            return cell
            
            
        }
        

        let cell = self.teamTableView.dequeueReusableCell(withIdentifier: "teamCell") as! RoleCell
        
        cell.index = indexPath.row
        cell.delegate = self
        if let team = Roles?[indexPath.row].team{
            
            cell.lblTeam?.text = team
            
            if let role = Roles?[indexPath.row].role {
                
                cell.lblRole?.text = Utils.role(type: role)      //role
            }
            
            if let teamId = Roles?[indexPath.row]._id{
                
                self.team = teamId
            }
            
            if !(Roles?[indexPath.row].isAdmin ?? false){
                
                if !(Roles?[indexPath.row].accepted ?? false) &&  Roles?[indexPath.row].type == "request" {
                    
                    cell.joinView.isHidden = false
                }else{
                    
                    cell.joinView.isHidden = true
                }
                
            }else{
                
                cell.joinView.isHidden = true
            }
            
            
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        // 1
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete" , handler: { (action:UITableViewRowAction, indexPath: IndexPath) -> Void in
            
            
            // 2
            //        let shareMenu = UIAlertController(title: nil, message: "Share using", preferredStyle: .actionSheet)
            //
            //        let twitterAction = UIAlertAction(title: "Twitter", style: .default, handler: nil)
            //                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            //
            //        shareMenu.addAction(twitterAction)
            //        shareMenu.addAction(cancelAction)
            //
            //        self.present(shareMenu, animated: true, completion: nil)
        })
        // 3
        let editAction = UITableViewRowAction(style: .default, title: "Edit" , handler: { (action:UITableViewRowAction, indexPath:IndexPath) -> Void in
            
            
            
            
            // 4
            //        let rateMenu = UIAlertController(title: nil, message: "Rate this App", preferredStyle: .actionSheet)
            //
            //        let appRateAction = UIAlertAction(title: "Rate", style: .default, handler: nil)
            //        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            //
            //        rateMenu.addAction(appRateAction)
            //        rateMenu.addAction(cancelAction)
            //
            //        self.present(rateMenu, animated: true, completion: nil)
        })
        // 5
        editAction.backgroundColor = .darkGray
        return [deleteAction,editAction]
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.teamTableView {
            
            let count = Roles?.count ?? 0
            
            return count
            
        }
        
        if tableView == self.reviewerTableViiew {
            
            return self.reviewerData.count
            
        }
        
        if tableView == self.accountantTableView {
            
            return self.accountantData.count
            
        }
        
        if tableView == self.empTableView {
            
            return self.employeeData.count
            
        }
        
        if tableView == self.managerTableView {
            
            return self.managerData.count
            
        }
        
        return 0
    }
}

extension TeamListViewController:RoleDelegate{
    func roleJoin(at index: Int) {
       
        
        
        if let info = self.invitaionData{
            
            for data in info{
                
                if data.team?.name == Roles?[index].team{
                    
                    if let invId = data._id{
                       
                        invitaionId = invId
                    
                    }
                    
                }
                
                
                
            }
            
            
            
        }
        
        
        guard let invId = self.invitaionId else {
            return
            
        }
            
            self.joinInvitation(id: invId)
 
    }
    
    
    
}
