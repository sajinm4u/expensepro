//
//  MyAccountViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 15/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alertift
import TextFieldEffects


class MyAccountViewController: BaseViewController {
    
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var txtCompany: HoshiTextField!
    @IBOutlet weak var txtPhone: HoshiTextField!
    @IBOutlet weak var txtCurrency: HoshiTextField!
    @IBOutlet weak var txtPassword: HoshiTextField!
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var accountEmail: UILabel!
    @IBOutlet weak var lblAddAccount: UILabel!
    
    @IBOutlet weak var attachAccountView: UIView!
    
    var emailString:String = ""
    var roleString:String = ""
    var deviceString:String = "iOS"
    var notifyString:String = Defaults.string(forKey: "deviceToken")  ?? ""
    var userType:String = ""
    
    
      var Account = ResponseHandler()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        
    }
    
    
    func initialSetup(){
        
        
        if let configuredType = Defaults.string(forKey: "configuredType"){
            
            if configuredType == "both"{
                
                self.attachAccountView.isHidden = true
                
                
            }else{
                
                self.attachAccountView.isHidden = false
               
            }
            
            
        }
        
        if let primary = Defaults.string(forKey: "primary"){
            
            if primary == "business"{
            
                self.lblType.text = "Business"
                self.userType = "business"
                
                if let email = Defaults.string(forKey: "businessEmail"){
                    self.lblAddAccount.text = "Attach a personal account"
                    self.accountEmail.text = email
                }
                
               
            }else{
                
                self.lblType.text  = "Personal"
                self.userType = "personal"
                if let email = Defaults.string(forKey: "personalEmail"){
                    
                    self.lblAddAccount.text = "Attach a business account"
                    
                    self.accountEmail.text = email
                }
                
            }
        }
        
        
        
        getUserProfile()
    }
    
    
    func swichAccount(){
        
        SVProgressHUD.show()

        Account.ChangeAccount(withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                self.getUserProfile()
                
                DispatchQueue.main.async {
                
                    Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                    
                
                }
            
                self.backNavigation()

              
            }
            
            
            
        }
        
        
    }
    
    
    func changeAccount() {
        
        
        if let configuredType = Defaults.string(forKey: "configuredType"){
            
            if configuredType == "both"{
                
                Alertift.alert(title:AppTitle, message: "Do you want to switch account?" )
                    .action(.destructive("Yes")){_,_,_ in
                                      
                        self.swichAccount()
                                   
                                      
                              }.action(.cancel("No"))
                                  .show(on: self)

                
               
                
            }
        }
        

        
    }
    
    
    @IBAction func addAccountType(_ sender: Any) {
        
        if self.userType == "personal"{
            
            let NewScene = SetupBusinessAccountVC.instantiate(fromAppStoryboard: .Main)
            
            NewScene.isConfigure = true
        
                             if let navigator = self.navigationController {
                             
                                 navigator.pushViewController(NewScene, animated: true)
                             
                          }
            
            
        }else{
            
            
            let NewScene = SetupPersonalVC.instantiate(fromAppStoryboard: .Main)
            
            NewScene.isConfigure = true
            NewScene.name = self.txtName.text
        
                             if let navigator = self.navigationController {
                             
                                 navigator.pushViewController(NewScene, animated: true)
                             
                          }
            
     
        }
        

    }
    
    
    @IBAction func switchAccountPressed(_ sender: Any) {
        
        changeAccount()
    }
    
    
    
    
    func getUserProfile(){
        
        SVProgressHUD.show()
        
        Account.getUserProfile(withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.Account.UserResponse?.data{
                    
                    if let name = info.name{
                        
                        self.txtName.text = name
                    }
                    if let currency = info.currency{
                        
                        if self.userType == "business"{
                            
                            self.txtCurrency.text = currency
                        }else{
                            
                            if let personalCurrency = info.personalCurrency{
                                
                                self.txtCurrency.text = personalCurrency
                            }
                            
                            
                        }
                        
             
                    }
                   
                    if let mobile = info.mobile{
                        
                        self.txtPhone.text = mobile
                    }
                    
//                    if let company = info.organisation?.name{
//                        
//                        self.txtCompany.text = company
//                    }
                    if let email = info.email{
                        
                        self.emailString = email
                    }
                    if let role = info.role{
                        
                        self.roleString = role
                    }
                    if let device = info.device{
                        
                        self.deviceString = device
                    }
                    if let notify = info.notifyToken{
                        
                        self.notifyString = notify
                    }
                    
                    
                   
                }
                
            }
            
            
            
        }
        
        
    }
    
    @IBAction func updatePressed(_ sender: Any) {
        
        if isConnected(){
            
            
            guard let txtName = self.txtName.text, let txtCompany = self.txtCompany.text, let txtPhone = self.txtPhone.text, let txtPwd = self.txtPassword.text, !txtPhone.isEmpty,!txtName.isEmpty,!txtPwd.isEmpty,!txtCompany.isEmpty else{
                
                Alertift.alert(title: AppTitle, message:mandatoryFields)
                .action(.default("OK"))
                .show(on: self)
                return
            }
            
            if txtPhone.count < 6{
                           
                           self.showNotification(message: notValidPhone)
                           return
                       }
                       
            if txtPwd.count < 6{
                                      
                           self.showNotification(message: passwordMessage)
                           return
            }
            
            
            
            let params = ProfileParams(name: txtName, email: self.emailString, mobile: txtPhone, password: txtPwd, role: self.roleString, notifyToken: self.notifyString, device: self.deviceString, organisation: txtCompany).Values
            
            
            self.updateProfile(param: params)
            
            
            
        }
        
        
    }
    
    
    
    func updateProfile(param:[String:Any]){

        
        SVProgressHUD.show()
        
        Account.updateUserProfile(withParameter:param) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            Alertift.alert(title: AppTitle, message:message)
            .action(.default("OK"))
            .show(on: self)
            

                
            }
        
    }
    
    

    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    

}
