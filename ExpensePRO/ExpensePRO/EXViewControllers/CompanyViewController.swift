//
//  CompanyViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 20/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//

import UIKit

class CompanyViewController: BaseViewController {
    
    @IBOutlet weak var menuTableView: UITableView!{
        didSet {
            menuTableView.tableFooterView = UIView(frame: .zero)
        }
    }

    var organisation:OrganisationData?
    var bgColor:UIColor = .lightGray
    var menuArray:[String] = []
    var menuIconsArray:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        print(organisation)
        
        self.menuArray = menuCompany
        self.menuIconsArray = menuIconComapny
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.reloadData()
    }
    
    
    @IBAction func backpressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
}

extension CompanyViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 2
        
        
      }
    
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){

        let header = view as! UITableViewHeaderFooterView

        
          view.tintColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
          header.textLabel?.font =  header.textLabel?.font.withSize(14)
          header.textLabel?.textColor = .lightGray
        
      }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        

        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0{
        
                self.menuTableView.separatorStyle = .none
                let cell = menuTableView.dequeueReusableCell(withIdentifier: "profileMenuCell", for: indexPath) as! MenuCell
                
            
            if let title = organisation?.name{
                
             cell.lblUsername.text = title
             cell.lblFirst?.text = title[0].uppercased()
                
            }
            
            if let logo =  organisation?.logo{
                
                let url = URL(string:Utils.getImage(id: logo))
            
                let phImage = UIImage(named: "")
                
                cell.iconImageView.backgroundColor = self.bgColor
                cell.iconImageView?.layer.cornerRadius = cell.iconImageView.frame.height/2
                cell.iconImageView?.contentMode = .scaleToFill
                cell.iconImageView?.kf.setImage(with: url, placeholder: phImage)
            
            }
                return cell
                
        
            
        }
        
     else{
                
                self.menuTableView.separatorStyle = .singleLine
                let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
                cell.menuTitle.text = menuArray[indexPath.row]
                cell.iconImageView.image = UIImage(named: menuIconsArray[indexPath.row])
            
                return cell
                
            }
            
        

        return UITableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 1{
        if indexPath.row == 0{
        let NewScene = MembersListViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            guard let orgId = organisation?._id else{
                
                return
                
            }
            NewScene.organisationId = orgId
            navigator.pushViewController(NewScene, animated: true)
           }
        }

        if indexPath.row == 1{

            let NewScene = TeamListViewController.instantiate(fromAppStoryboard: .Main)
            guard let orgId = organisation?._id else{
                
                return
                
            }
            NewScene.organisationId = orgId
            if let navigator = self.navigationController {
                navigator.pushViewController(NewScene, animated: true)
            }
        }

        if indexPath.row == 2{

            let NewScene = CategoriesListViewController.instantiate(fromAppStoryboard: .Main)
            
            guard let orgId = organisation?._id else{
                
                return
                
            }
            NewScene.organisationId = orgId

            if let navigator = self.navigationController {
                navigator.pushViewController(NewScene, animated: true)
            }
        }
        }

        }
    
}
