//
//  AccountSelectionViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 17/11/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

class AccountSelectionViewController: UIViewController {
    
    @IBOutlet weak var personalView: CurvedView!
    @IBOutlet weak var businessView: CurvedView!
    
    var userName:String?
    var userEmail:String?
    var appleId:String?
    var isApple:Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.personalView.layer.borderWidth = 1
        self.personalView.layer.borderColor = #colorLiteral(red: 0.1277076304, green: 0.592394948, blue: 0.6223913431, alpha: 1)
        self.businessView.layer.borderWidth = 1
        self.businessView.layer.borderColor = #colorLiteral(red: 0.1277076304, green: 0.592394948, blue: 0.6223913431, alpha: 1)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func personalPressed(_ sender: Any) {
        
        let NewScene = SetupPersonalVC.instantiate(fromAppStoryboard: .Main)
        
        NewScene.name = self.userName
        NewScene.email = self.userEmail
        NewScene.appleId = self.appleId
        NewScene.isApple = self.isApple
        
                         if let navigator = self.navigationController {
                        
                         
                             navigator.pushViewController(NewScene, animated: true)
                         
                      }
        
        
    }
    

    
    
    @IBAction func haveAccount(_ sender: Any) {
        
        
        let NewScene = SignInViewController.instantiate(fromAppStoryboard: .Main)
        
                         if let navigator = self.navigationController {
                        
                         
                             navigator.pushViewController(NewScene, animated: true)
                         
                  
                      }
        
        
    }
    
    
    @IBAction func businessPressed(_ sender: Any) {
        
        
        let NewScene = SetupBusinessAccountVC.instantiate(fromAppStoryboard: .Main)
        
        NewScene.name = self.userName
        NewScene.email = self.userEmail
        NewScene.appleId = self.appleId
        NewScene.isApple = self.isApple
        
        
        
                         if let navigator = self.navigationController {
                        
                         
                             navigator.pushViewController(NewScene, animated: true)
                         
                  
                      }
        
    }
    
}
