//
//  InboxViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 04/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alertift

class InboxViewController: BaseViewController {
    
    @IBOutlet weak var inboxTableView: UITableView!
   
    
     var Inbox = ResponseHandler()
     var inboxData:[NotificationStructData]?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
       
        // Do any additional setup after loading the view.
    }
    
    func initialSetup(){
        
        
        self.inboxTableView.rowHeight = UITableView.automaticDimension
        self.inboxTableView.estimatedRowHeight = 70.0
        getInboxList()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        initialSetup()
    }
    
    func arichiveNotification(id:String){
        
        SVProgressHUD.show()
        
        Inbox.archiveInbox(id:id,withParameter:[:]) { (isSuccess, message) in
               SVProgressHUD.dismiss()
               if (isSuccess){
                   
                self.getInboxList()
                   
               }else{
                
                Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                
               }
        
    }
        
        
    }
    
    
    func changeImberseMentStatus(id:String,status:String){
        
        SVProgressHUD.show()
        
        let url = Api.ChangeStatusImbersement + "/" + id

        Inbox.updateImbersmentStatus(urlString:url, withParameter:["status":status]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                self.getInboxList()
                
            }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
            }
           
               
            }
            
            
            
        }
    
    
   func getInboxList(){
       
       SVProgressHUD.show()
       
    
    guard let usertype = Defaults.string(forKey: "primaryID") else{
        
       return
    }
    
    let url = Api.Inbox + "/" + usertype
       
    Inbox.getInbox(urlString:url,withParameter:[:]) { (isSuccess, message) in
           SVProgressHUD.dismiss()
           if (isSuccess){
               
            if let info = self.Inbox.InboxResponse?.data?.notifications{
                
                   
                   if info.count > 0 {
                       
                    self.inboxData?.removeAll()
                     
                       self.inboxData = info
                       self.inboxTableView.delegate = self
                       self.inboxTableView.dataSource = self
                       self.inboxTableView.reloadData()
                   
                       
                       
                   }
                   
                  
               }
               
           }
           
           
           
       }
       
       
   }

}
extension InboxViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.inboxData?.count ?? 0
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let info = self.inboxData?[indexPath.row] else{
            return
        }
        
        if info.data?.type == "expense"{
            
            guard let expId = info.data?._id else{
                return
            }
                    let NewScene = AddNewImbersementViewController.instantiate(fromAppStoryboard: .Main)
            
                    var currency = ""
                    if let curr = Defaults.string(forKey: "Currency"){
            
                        currency = curr
                    }
            
                   if let navigator = self.navigationController {
            
                      NewScene.isInbox = true
                      NewScene.expenseId = expId
                      NewScene.currency = currency
            
                       navigator.pushViewController(NewScene, animated: true)
            
            
                }
            
        }
        

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = self.inboxTableView.dequeueReusableCell(withIdentifier: InboxCell, for: indexPath) as! InboxViewCell
                  
        if let info = inboxData?[indexPath.row]{
            
            if let buttons = info.buttons{

                if (buttons.contains{ $0.id == "review" && info.currentStatus == "" && !info.isRead }) {

                    cell.setViews(isApprove: false, isUpgrade: true, isAdd: true, isArchive: true, isPay: true)

                    }else if (buttons.contains{ $0.id == "upgrade" && !info.isRead }){

                        cell.setViews(isApprove: true, isUpgrade: false, isAdd: true, isArchive: false, isPay: true)
                    }else if(buttons.contains{ $0.id == "approve" && info.currentStatus == "" && !info.isRead }) {
                
                        cell.setViews(isApprove: false, isUpgrade: true, isAdd: true, isArchive: true, isPay: true)
                    }else if (buttons.contains{ $0.id == "review"  && !info.isRead}){
                        
                        cell.setViews(isApprove: false, isUpgrade: true, isAdd: true, isArchive: true, isPay: true)
                    }
                
                    else if (buttons.contains{ $0.id == "addTeam"  && !info.isRead}){

                        cell.setViews(isApprove: true, isUpgrade: true, isAdd: false, isArchive: false, isPay: true)
                    }
                    else if (buttons.contains{ $0.id == "pay" && info.currentStatus == ""  && !info.isRead}){
                    
                    cell.setViews(isApprove: true, isUpgrade: true, isAdd: true, isArchive: true, isPay: false)
                   }
                   else if (!info.archive) {

                      cell.setViews(isApprove: true, isUpgrade: true, isAdd: true, isArchive: false, isPay: true)

                    }
                else{
                    cell.setViews(isApprove: true, isUpgrade: true, isAdd: true, isArchive: true, isPay: true)
                }

                }

   
            if let name = info.sender?.name{
                
                cell.lblTitle.text = name
            }
           
            if let message = info.messageText{
                
                cell.lblMessage.text = message
            }
            
            if let date = info.created_at{
                           
                cell.lblDate.text = date.getDate()
                       }
            
        }
              
        
        cell.index = indexPath.row
        cell.delegate = self
                  
        return cell

        
    }
    
    
}

extension InboxViewController:InboxDelegate{
    func pay(index at: Int) {
        
        if let info = self.inboxData?[at]{
            guard let id = info.data?._id else{
                return
            }
            self.changeImberseMentStatus(id:id,status:"Pay")
        }
        
    }
    
    func payReject(index at: Int) {
        
        if let info = self.inboxData?[at]{
            guard let id = info.data?._id else{
                return
            }
            self.changeImberseMentStatus(id:id,status:"Reject")
        }
    
    }
    
    func archiveMessage(index at: Int) {
        
        if let info = self.inboxData?[at]{
            
            if let id = info._id{
                
                self.arichiveNotification(id: id)
            }

            
        }
        
        
    }
    
    func addTeam(index at: Int) {
        
    }
    
    func upgrade(index at: Int) {
        
        let NewScene = PackageBillingViewController.instantiate(fromAppStoryboard: .Main)
        
                         if let navigator = self.navigationController {
                     
                             navigator.pushViewController(NewScene, animated: true)
                    
                      }
        
        
    }
    
    func reject(index at: Int) {
        if let info = self.inboxData?[at]{
            guard let id = info.data?._id else{
                return
            }
            self.changeImberseMentStatus(id:id,status:"Reject")
        }
    }
    
    func approve(index at: Int) {
        
        if let info = self.inboxData?[at]{
            guard let id = info.data?._id else{
                return
            }
            
            
            if (info.buttons!.contains{ $0.id == "review"}){
                self.changeImberseMentStatus(id:id,status:"Review")
            }else{
                self.changeImberseMentStatus(id:id,status:"Approve")
            }
           
        }
        
    }
    
   
    
    
    
}
