//
//  SetupPersonalVC.swift
//  ExpensePRO
//
//  Created by Sajin M on 17/11/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alertift
import SVProgressHUD
import MRCountryPicker

class SetupPersonalVC: BaseViewController {
    
    
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var txtPhone: HoshiTextField!
    @IBOutlet weak var txtCurrency: HoshiTextField!
    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtOtp: HoshiTextField!
    
    @IBOutlet weak var otpView: UIView!
    var Register = ResponseHandler()
    
    var codePicker = MRCountryPicker()
    var tokenStr:String? = nil
    var registerParams = [String:Any]()
    var name:String?
    var email:String?
    var isConfigure:Bool = false
    var appleId:String?
    var isApple:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let name = self.name{
            
            self.txtName.text = name
        }
        
        if let email = self.email{
            
            self.txtEmail.text = email
        }
        
        
//        if isApple{
//            
//            self.txtPassword.isHidden = true
//        }else{
//            
//            self.txtPassword.isHidden = false
//        }
        
        if isConfigure  {
            
            txtPassword.isHidden = true
            txtPhone.isHidden = true
            
        }else{
            
            txtPassword.isHidden = false
            txtPhone.isHidden = false
            
        }
        
        
        if isApple{
            
            txtPassword.isHidden = true
            txtPhone.isHidden = false
            
        }

     
        otpView.isHidden = true
        codePicker.countryPickerDelegate = self
        codePicker.showPhoneNumbers = false
        self.txtCurrency.inputView = codePicker
        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        
        guard let txtCurrency = self.txtCurrency.text, !txtCurrency.isEmpty else{
            
            self.showNotification(message: "Please select a currency")
            
        return
        }
        if !isConfigure{
            
            if !isApple{
            
            guard let txtPwd = self.txtPassword.text, !txtPwd.isEmpty else{
                
                self.showNotification(message: passwordMessage)
                
            return
            }
            }else{
                
                self.txtPassword.text = " "
            }
         
            
            guard let txtPhone = self.txtPhone.text, !txtPhone.isEmpty else{
                
                self.showNotification(message: notValidPhone)
                
            return
            }
            
        }
       
      
        
        guard let txtName = self.txtName.text, !txtName.isEmpty else{
            
            self.showNotification(message: "Please enter your name")
            
        return
        }
        
        guard let txtEmail = self.txtEmail.text, !txtEmail.isEmpty else{
            
            self.showNotification(message: "Please enter a valid email")
            
        return
        }
        
        
        if isConnected(){
            
           
            if isConfigure {
                
                guard let txtCurrency = self.txtCurrency.text,let txtEmail = self.txtEmail.text,let txtName = self.txtName.text,!txtName.isEmpty,!txtEmail.isEmpty,!txtCurrency.isEmpty else {
                             
                    self.showNotification(message:mandatoryFields)
                             
                              return
                          }
                
                let params = configAccount(currency:txtCurrency, email: txtEmail, organisation:"").Values
                
                self.registerParams = params
                
                self.updateProfile()
       

            
                
                
            }else{
                
                guard let txtCurrency = self.txtCurrency.text,let txtEmail = self.txtEmail.text,let txtPhone = self.txtPhone.text, let txtPwd = self.txtPassword.text,let txtName = self.txtName.text, !txtPhone.isEmpty,!txtName.isEmpty,!txtEmail.isEmpty,!txtPwd.isEmpty,!txtCurrency.isEmpty else {
                             
                    self.showNotification(message:mandatoryFields)
                             
                              return
                          }
                
                if txtEmail.isValidEmail(){
                    
                    if !isApple{
                    
                   if txtPhone.count < 5 {
                                   
                       DispatchQueue.main.async {
                           self.showNotification(message: "Please enter a valid phone number")
                           return
                       }
                                  
                               }
                               
                    if txtPwd.count < 6{
                                            
                        DispatchQueue.main.async {
                                   self.showNotification(message: passwordMessage)
                                   return
                        }
                      }
                    }
                    
                    var params = userRegistration(name: txtName,email:txtEmail, mobile:txtPhone, password:txtPwd, currency:txtCurrency,appleId: "").Values
                    
                    if isApple{
                        
                        if let id = self.appleId{
                            
                            params = userRegistration(name: txtName,email:txtEmail, mobile:txtPhone, password:txtPwd, currency:txtCurrency,appleId:id).Values
                        }
                        
                    }
                    self.registerParams = params
                    
                    self.updateProfile()
                    
            }else{
                
                DispatchQueue.main.async {
                
                self.showNotification(message:validEmail)
                   
               }
            }
            }
            
        }

    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
    func updateProfile(){
  
                
                guard let email = self.txtEmail.text else {
                    
                    return
                    
                }
         
                self.generateOtp(email: email)
                
            }
    
    @IBAction func submitOtpPressed(_ sender: Any) {
        
        
        guard let otp = self.txtOtp.text, !otp.isEmpty else{
            
            Alertift.alert(title: AppTitle, message:otpNotValid)
            .action(.default("OK"))
            .show(on: self)
            return
            
        }
        
        guard let email = self.txtEmail.text else {
            
            return
            
        }
        

        self.verifyOtp(email: email, otp: otp)
    }
    
    @IBAction func resendOtpPressed(_ sender: Any) {
        
        guard let email = self.txtEmail.text else {
            
            return
            
        }
        
        self.generateOtp(email: email)
        
    }
    
    
    
    func generateOtp(email:String){
        
        SVProgressHUD.show()
        
        Register.SendOtp(withParameter:["email":email]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            
        if isSuccess{
                
        self.otpView.isHidden = false
                    
               
            }else{
                
                
                
                if let isRegistered = self.Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        DispatchQueue.main.async {
                            
                            Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
                            
                        }
                        
                       // self.backNavigation()
                       
                    }
                        
                    }else{
                        
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                    }
                
               
                

            }
            
            
                
            }
        
    }
    
    
    func verifyOtp(email:String,otp:String){
        
      
        
        SVProgressHUD.show()
        
        Register.VerifyOtp(withParameter:["email":email,"otp":otp]) { [self] (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
                self.txtOtp.text = ""
                self.otpView.isHidden = true
                
                if isConfigure{
                    
                    self.configureAccount(param: self.registerParams)
                    
                }else{
                    
                    self.doRegister(param: self.registerParams)
                    
                }
              
               
            }else{
                
             
                
              if let isRegistered = Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        self.txtOtp.text = ""
                        self.otpView.isHidden = true
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                        
                        self.backNavigation()
                        
                    }
                    
                    
              }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
                
              }
                

            }
            
            
                
            }
        
    }
    
    
    func configureAccount(param:[String:Any]){
        
        
                SVProgressHUD.show()
        
                Register.ConfigureAnotherAccount(withParameter:param) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
        
                    if isSuccess{
                        
                        DispatchQueue.main.async {
                            
                            Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
                        }
                        
                        self.backNavigation()
        
                        
                        
                                        }
          
                        else{
                    
                                    Alertift.alert(title: AppTitle, message:message)
                                    .action(.default("OK"))
                                    .show(on: self)
                    
                    
                                }
                    
                                
                    
                                }
                    
                        }
    
    
    
    
    func doRegister(param:[String:Any]){
        
        
                SVProgressHUD.show()
        
                Register.RegisterUserProfile(withParameter:param) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
        
                    if isSuccess{
                        
                                        if let info = self.Register.loginResponse?.data{
                        
                                    
                                            if let token = info.token{
                        
                        
                                                self.tokenStr = token
                                                Defaults.set(self.tokenStr,forKey:UserToken)
                                                Switcher.updateRootVC()

                        
                        
                                            }
                        
                        
                                        }
          
                                 }
                        else{
                    
                            
                            DispatchQueue.main.async {
                                Alertift.alert(title: AppTitle, message:message)
                                .action(.default("OK"))
                                .show(on: self)
                            }
                            self.backNavigation()
                    
                    
                                }
                    
                                
                    
                                }
                    
                        }
    

}

extension SetupPersonalVC: MRCountryPickerDelegate {
    

      func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
       
        
        if let currency = Locale.currency[countryCode]?.code{
            
            
            self.txtCurrency.text = currency
            
        }
        
      
             
         }
         
    
    
}
