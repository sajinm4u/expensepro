//
//  HomeViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 21/07/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import HAActionSheet
import TextFieldEffects
import Alertift
import MessageUI
import DynamicColor
import Kingfisher




class HomeViewController: BaseViewController {
    
    
    var DashBoard = ResponseHandler()
    var CatType:[TypeData]?
    var Roles:[RolesData]?
    
    var imbersements:[reimberseArrayData]?
    var imbersementList:[reimberseArrayData] = []
    var statusArray:[String]?
    var dashboardTypes:[String]?
    var organisation:[OrganisationData]?
    var userProfile:ProfileData?
    var page = 1
    var pageSize = 10
    var totalCount = 10
    
    @IBOutlet weak var addNewTeamView: UIView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var expenseTableView: UITableView!
    @IBOutlet weak var teamTableView: UITableView!
    @IBOutlet weak var newTeamView: UIView!
    @IBOutlet weak var txtTeamName: HoshiTextField!
    @IBOutlet weak var txtRole: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var invitationView: UIView!
    @IBOutlet weak var inviteMembersView: UIView!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblSelectTeam: UILabel!
    @IBOutlet weak var listCompanyView: UIView!
    @IBOutlet weak var colorView: CurvedView!
    @IBOutlet weak var lblFirstCharacter: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnCloseTeam: UIButton!
    
    var primaryOrganisation:String?
    var role:String = ""
    var keyword:String = ""
    var status:String = ""
    var startDate:String?
    var endDate:String?
    var personalCurrency:String?
    var teamCurrency:String?
    var currency:String?
    var filterPressed = false
    var pickedRole:String?
    let originalColor = DynamicColor(hexString: "#c0392b")

    var gradient:DynamicGradient?
    var colors:[UIColor]?
    var rolePicker = UIPickerView()
    
@IBOutlet weak var listViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var companyListTableView: UITableView!
    @IBOutlet weak var companyListView: CurvedView!
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        
    }
    
    let companyTitle = ["Codelattice","AladdinPRO","Personal"]
    let expenseTitle = ["Travel Expense","Internet Expense","Travel Expense"]
    let icons = ["travel","internet","travel"]
    

    func initialSetup(){
        self.btnCloseTeam.isHidden = true
        self.listViewHeightConstant.constant = 0
        self.companyListView.isHidden = true
        imgLogo?.layer.cornerRadius = imgLogo.frame.height/2
        imgLogo?.contentMode = .scaleAspectFill
        self.listCompanyView.isHidden = true
        self.inviteMembersView.isHidden = true
        self.invitationView.isHidden = true
        self.lblSelectTeam.isHidden = true
        self.emptyView.isHidden = true
        self.newTeamView.isHidden = true
        self.filterView.isHidden = true
        self.teamTableView.tableFooterView = UIView(frame: .zero)
        self.filterTableView.tableFooterView = UIView(frame: .zero)
        self.addNewTeamView.isHidden = true
        self.filterTableView.dataSource = self
        self.filterTableView.delegate = self
        self.filterTableView.reloadData()
        self.companyListTableView.tableFooterView = UIView()
        self.gradient = DynamicGradient(colors: [blue, red, yellow])
        self.colorView.layer.cornerRadius = colorView.frame.height/2

        self.txtRole.inputView = rolePicker
        rolePicker.delegate = self
        rolePicker.dataSource = self
        updateProfile()
        
//         if isConnected(){
//
//        //getDashBoards()
//     //   getCatTypeBoards()
//
//
//
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.listCompanyView.isHidden = true
        self.btnCloseTeam.isHidden = true

    }
    
    @IBAction func changeAccount(_ sender: Any) {
        
        self.listCompanyView.isHidden = false
    }
    
    @IBAction func addNowPressed(_ sender: Any) {
        
        self.listCompanyView.isHidden = true
        self.btnCloseTeam.isHidden = true
        
        guard let email = Defaults.string(forKey: "email") else{
            return
        }
        
        let NewScene = NewOrganizationSetupVC.instantiate(fromAppStoryboard: .Main)
       
       if let navigator = self.navigationController {
          NewScene.email = email
          navigator.pushViewController(NewScene, animated: true)
       

    }
        
        
    }
    
    @IBAction func cancelListing(_ sender: Any) {
        self.listCompanyView.isHidden = true
        self.btnCloseTeam.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if isConnected(){
        page = 1
        pageSize = 10
        totalCount = 0

            if imbersements?.count ?? 0 > 0 {
                self.imbersements?.removeAll()
                self.imbersementList.removeAll()
            }
            
         getUserProfile()
         getRoleList()
        }
    }
    
    func swichAccount(id:String){
        
        SVProgressHUD.show()

        
        page = 1
        pageSize = 10
        totalCount = 0

        DashBoard.ChangeAccount(withParameter:["organisation":id]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
            
                self.getUserProfile()
                
                DispatchQueue.main.async {
                    
                    Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                
                    
                }
            

              
            }
            
            
            
        }
        
        
    }
    
    
    func getImbersementWithRole(){
        
        let nextMonth = Calendar.current.date(byAdding: .month, value: -16, to: Date())                 //Calendar.current.date(byAdding: .month, value: -12, to: Date())
       
        
        guard let startDate = nextMonth?.toString(dateFormat: "dd-MM-yyyy") else{
            
           return
            
        }
       // print(startDate)
        
        let endDt:Date? = Date()
        
        guard let endDate = endDt?.toString(dateFormat: "dd-MM-yyyy") else{
            
            return
        }
        

        
        guard let organisation = self.userProfile?.primary?._id else{
            return
        }
        
        let param = ImbersmentParams(status:self.status,role:self.role, category:"", keyword: "", start_date:startDate, end_date:endDate, organisation:organisation).Values
        
        
        
        getImbersementList(params:param)
        
        
        
    }
    
    @IBAction func sentInvitationPressed(_ sender: Any) {
    }
    
    @IBAction func invitationCancelPressed(_ sender: Any) {
        
        self.invitationView.isHidden = true
    }
    
    
    func getImbersementList(params:[String:String]){
        
        SVProgressHUD.show()
        
        var url = URL(string: Api.ImbersementList)!
        url.appendQueryItem(name: "page", value: "\(self.page)")
        url.appendQueryItem(name: "pageSize", value: "\(self.pageSize)")
        
        DashBoard.getImbersmentList(url:url,withParameter:params) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.MyImbersementListResponse?.data?.reimbursements{
                    
                    if info.count > 0 {
                        
                       // print(self.DashBoard.MyImbersementListResponse)
                        
                        if let total = self.DashBoard.MyImbersementListResponse?.data?.total{
                            
                            self.totalCount = total
                        }
                        

                        self.imbersementList.append(contentsOf: info)
                        self.imbersements = self.imbersementList
                        self.expenseTableView.delegate = self
                        self.expenseTableView.dataSource = self
                        self.expenseTableView.reloadData()
                        self.expenseTableView.isHidden = false
                        self.emptyView.isHidden = true
                        
                        
                    }else{
                        
                        if self.filterPressed{
                            
                            if self.imbersements?.count ?? 0 == 0{
                                self.emptyView.isHidden = false
                                self.expenseTableView.isHidden = true
                            }else{
                                self.emptyView.isHidden = true
                                self.expenseTableView.isHidden = false
                            }
                            
                            
//                            Alertift.alert(title: AppTitle, message:"Expense list looks empty!")
//                            .action(.default("OK"))
//                            .show(on: self)
                            
                        }else{
                            
                            self.expenseTableView.delegate = self
                            self.expenseTableView.dataSource = self
                            self.expenseTableView.reloadData()
                            self.expenseTableView.isHidden = false
                            self.emptyView.isHidden = true
                            
                            
                        }
                        
                        
//                        self.imbersements?.removeAll()
//                        self.imbersementList.removeAll()
                    }
                    

                   
                }else{
                    
                    self.imbersements?.removeAll()
                    self.imbersementList.removeAll()
                }
                

            }
            
            
            
        }
        
        
    }
    
    @IBAction func filterPressed(_ sender: Any) {
        
        self.filterView.isHidden = false
    }
    
    
    
    @IBAction func filterCancelPressed(_ sender: Any) {
        
        self.filterView.isHidden = true
    }
    
    @IBAction func cancelAddTeamPressed(_ sender: Any) {
        self.txtRole.text = ""
        self.txtTeamName.text = ""
        self.newTeamView.isHidden = true
        self.btnFilter.isHidden = false
        self.dismissKeyboard()
    }
    
    @IBAction func addNewTeamPressed(_ sender: Any) {
        
        self.btnFilter.isHidden = true
        self.newTeamView.isHidden = false
    
    }
    
    
    
    @IBAction func newTeamSubmitPressed(_ sender: Any) {
        
    
        guard let teamName = self.txtTeamName.text,!teamName.isEmpty else {
            
            Alertift.alert(title: AppTitle, message:"Please Enter Team Name")
            .action(.default("OK"))
            .show(on: self)
            return
        }
        
        
        
        guard let role = self.pickedRole else{
            
            Alertift.alert(title: AppTitle, message:"Please select a role")
            .action(.default("OK"))
            .show(on: self)
            return
        }
        
        self.addNewTeam(teamName:teamName,teamRole: role)

  
    }
    
    
    func getUserProfile(){
        
        SVProgressHUD.show()
        
        DashBoard.getUserProfile(withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.UserResponse?.data{
                    
                    self.userProfile = info
                    if let name = info.name{
                        
                     
                        Defaults.set(name, forKey: "userName")
                        self.lblProfile.text = name
                        
                    }
                    
                    if let email = info.email{
                        
                        Defaults.set(email, forKey: "email")
                        
                    }
                    
                    if let emailNot = info.emailNotification{
                        
                        Defaults.set(emailNot, forKey: "emailNotification")
                        
                    }
                    
                    if let mobileNot = info.enableNotification{
                        
                        Defaults.set(mobileNot, forKey: "mobNotification")
                        
                    }
                
                    
                    if let userId = info._id{
                        
                        Defaults.set(userId, forKey: "userID")
                    }
                    
                    if let avatar = info.avatar{
                        
                        Defaults.set(avatar, forKey: "userPic")
                        
                    }
                    
                    if let organisation = info.organisations{
                        self.organisation = organisation
                       if self.organisation?.count ?? 0 > 0{
                        DispatchQueue.main.async {
                            let screenHeight = UIScreen.main.bounds.height - 150
                            if CGFloat((self.organisation!.count + 1) * 90) < screenHeight {
                            
                                self.listViewHeightConstant.constant = CGFloat((self.organisation!.count + 1) * 90)
                            }else{
                                
                                self.listViewHeightConstant.constant = 300
                                
                            }
                            
                            self.companyListTableView.delegate = self
                            self.companyListTableView.dataSource = self
                            self.companyListTableView.reloadData()
                            self.companyListView.isHidden = false
                            self.colors = self.gradient?.colorPalette(amount:UInt((self.organisation!.count) + 1))
                        }
//
                        }
                    }
                    
                    
                    
                    if let primary = info.primary?.type{
                        
                        Defaults.set(primary, forKey: "primary")
                        
                        if primary == "personal"{
                            
                            if let id = info.personalOrgId{
                                Defaults.set(id, forKey: "primaryID")
                            }
                            
                            if let image = info.avatar{
                                let url = URL(string:Utils.getImage(id: image))
                                self.imgLogo?.kf.setImage(with: url,placeholder:UIImage(named: "profile_pic"))
                               
                            }
                            if let name = info.name{
                                self.lblProfile.text = name
                            }
                            self.colorView.backgroundColor = .clear
                            self.btnFilter.isHidden = true
                        }else{
                            
                            if let id = info.primary?._id{
                                Defaults.set(id, forKey: "primaryID")
                            }
                            
                            if let name = info.primary?.name{
                                Defaults.set(name, forKey: "primaryName")
                            }
                            
                            if let image = info.primary?.logo{
                                
                                if let name = info.primary?.name{
                                    self.lblProfile.text = name
                                }
                                if image == ""{
                                    
                                    self.lblFirstCharacter.text = info.primary?.name?[0].uppercased()
                                    self.colorView.backgroundColor = self.originalColor
                                    self.imgLogo.image = nil
                                }else{
                                    let url = URL(string:Utils.getImage(id: image))
                                    self.imgLogo?.kf.setImage(with: url,placeholder:UIImage(named: ""))
                                    
                                }
                                
                               
                               
                            }
                            self.btnFilter.isHidden = false
                        }
                        
                        if let Currency = info.primary?.currency{
                           
                            self.currency = Currency
                            Defaults.set(Currency, forKey: "Currency")
                        }

                        

                        
                        
                        self.getRoleList()
                        self.getImbersementWithRole()
                        
                    }
                    
//                    if let busEmail = info.businessEmail{
//
//                        Defaults.set(busEmail, forKey: "businessEmail")
//
//                    }
//
//                    if let personalEmail = info.personalEmail{
//
//                        Defaults.set(personalEmail, forKey: "personalEmail")
//
//                    }
                    
//                    if let configuredType = info.configuredAccount{
//
//                        Defaults.set(configuredType, forKey: "configuredType")
//                    }
                    
                   
                    

                   // self.getImbersementWithRole()
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    
    func addNewTeam(teamName:String,teamRole:String){
        
        guard let id = self.userProfile?.primary?._id else{
            return
        }
        
        SVProgressHUD.show()
        
        DashBoard.addTeams(withParameter: ["name":teamName,"role":teamRole,"organisation":id]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
               
                self.btnFilter.isHidden = false
                self.newTeamView.isHidden = true
                self.txtTeamName.text = ""
                self.txtRole.text = ""
                self.getRoleList()
           
                
            }else{
                
                
                if message.contains("Please upgrade plan") {
                    
                    var msg = message + " Please use www.xpense.pro to upgrade your plan"
                    
                    Alertift.alert(title: AppTitle, message:msg)
                    .action(.default("OK"))
                    .show(on: self)
                    
                }else{
                    
                    Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                    
                }
                
               
                
            }
            
            
            
        }
        
        
    }
    
    
    
    @IBAction func addNewPressed(_ sender: Any) {
        
        
        if let primary = Defaults.string(forKey: "primary"){
            
            if primary == "business"{
                self.btnAdd.isHidden = true
                self.btnCloseTeam.isHidden = false
                self.addNewTeamView.isHidden = false
                if let curr = Defaults.string(forKey: "Currency"){
                    
                    self.currency = curr
                }
                
            }else{
                
                
                let NewScene = AddNewImbersementViewController.instantiate(fromAppStoryboard: .Main)
    
                if let curr = Defaults.string(forKey: "Currency"){
                    
                    self.currency = curr
                }
               
               if let navigator = self.navigationController {
                
                  let teamId = ""
                 
                  NewScene.isEdit = false
                  NewScene.teamId = teamId
                  NewScene.currency = self.currency
               
                   navigator.pushViewController(NewScene, animated: true)
               
        
            }
                
                
                
            }
            
        }

    }
    
//    func getCatTypeBoards(){
//
//        SVProgressHUD.show()
//
//        DashBoard.getTypeList(withParameter: [:]) { (isSuccess, message) in
//            SVProgressHUD.dismiss()
//            if (isSuccess){
//
//                if let info = self.DashBoard.TeamTypeResponse?.data{
//
//                    if info.count > 0 {
//
//
//                        self.CatType = info
//
//
//
//                    }
//
//
//                }
//
//            }
//
//
//
//        }
//
//
//    }
//
    
    
    func updateProfile(){

        let deviceString:String = "iOS"
        let notifyString:String = Defaults.string(forKey: "deviceToken")  ?? ""


        DashBoard.updateDeviceToken(withParameter:["notifyToken": notifyString,"device":deviceString]) { (isSuccess, message) in

               //print("Token registered")
            }

    }
    
    func getRoleList(){
        
        guard let id = Defaults.string(forKey: "primaryID") else {
           return
        }
        SVProgressHUD.show()
        
        let urlStr = Api.Roles + "/" + id
        DashBoard.getRolesList(url:urlStr,withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.RolesResponse?.data{
                    self.Roles?.removeAll()
                    if info.count > 0 {
               
                        self.lblSelectTeam.isHidden = false
                        self.Roles = info
                        self.teamTableView.delegate = self
                        self.teamTableView.dataSource = self
                        self.teamTableView.reloadData()
                    

                    }else{
                      
                        self.lblSelectTeam.isHidden = true
                        self.teamTableView.delegate = self
                        self.teamTableView.dataSource = self
                        self.teamTableView.reloadData()
                        
                    }
                    
                   
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    
    
    
    
    func getDashBoards(){
        
        SVProgressHUD.show()
        
        DashBoard.getMyDashBoard(withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.DashBoard.MyDashBoardResponse?.data{
                    
                    if info.count > 0 {
                  
                        self.dashboardTypes = info
                 
                        
                    }
                    
                   
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    
    @IBAction func TeamAddCancel(_ sender: Any) {
        
        self.btnAdd.isHidden = false
        self.btnCloseTeam.isHidden = true
        self.addNewTeamView.isHidden = true
        
        
    }
    


}
extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if tableView == self.companyListTableView{
            
            self.listCompanyView.isHidden = true
           
            if indexPath.section == 0{
            
                guard let id = self.userProfile?.personalOrgId else{
                    
                    return
                }
                self.role = ""
                self.status = ""
                self.imbersements?.removeAll()
                self.imbersementList.removeAll()
                self.swichAccount(id: id)
                
            }else{
                
                guard let id = self.organisation?[indexPath.row]._id else{
                    
                    return
                }
                self.imbersements?.removeAll()
                self.imbersementList.removeAll()
                self.swichAccount(id: id)
                
            }
    
        }
        
        
        if tableView == self.filterTableView{
            
            
            if filterArray[indexPath.row] != nil{
                
                self.btnCloseTeam.isHidden = true
                
                if indexPath.row == 0{
                    
                    self.role = ""
                    self.status = ""
                }else{
                    
                    self.keyword = ""
                    self.status = filterArray[indexPath.row]
                    self.role = ""
                }
                
               
            }
            page = 1
            pageSize = 10
            totalCount = 0
            self.imbersements?.removeAll()
            self.imbersementList.removeAll()
            self.filterPressed = true
            self.filterView.isHidden = true
           
            
            getImbersementWithRole()
            
        }
        
        self.addNewTeamView.isHidden = true
        self.btnAdd.isHidden = false
        var teamId = ""
       
    
        if tableView == teamTableView {
            
            self.btnCloseTeam.isHidden = true

                if let team = self.Roles?[indexPath.row]{
                    
                    teamId = team._id!
                    //self.currency = self.teamCurrency 
                    
                }
            
            if let admin = self.Roles?[indexPath.row].isAdmin  {
                
                if admin || self.Roles?[indexPath.row].accepted ?? false  {
                    
                    
                    let NewScene = AddNewImbersementViewController.instantiate(fromAppStoryboard: .Main)
                   
                   if let navigator = self.navigationController {
                    
                      NewScene.isEdit = false
                      NewScene.teamId = teamId
                      NewScene.currency = self.currency
                    
                       navigator.pushViewController(NewScene, animated: true)
                   
            
                }
                
                    
                }else{
                    
                                Alertift.alert(title: AppTitle, message:"Please join to start add expenses")
                                .action(.default("OK"))
                                .show(on: self)
                    
                }
                
                //let accepted = self.Roles?[indexPath.row].accepted else{
            
             
            }
            
        
        }
        
        if tableView == expenseTableView{
            
            let NewScene = AddNewImbersementViewController.instantiate(fromAppStoryboard: .Main)
            
            if imbersements?.count ?? 0 > 0{
            
            if let info = imbersements?[indexPath.row]{
                
                self.imbersements?.removeAll()
                self.imbersementList.removeAll()
                             
                             if let navigator = self.navigationController {
                                
                                NewScene.isEdit = true
                                NewScene.imbersement = info
                             
                                 navigator.pushViewController(NewScene, animated: true)
                             
                      
                          }
            }
            }
            
        }
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

        if tableView == companyListTableView{
            return 2
        }
        
        return 1
      }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        
        if tableView == companyListTableView{
        if section == 0{
            return "Personal"
        }
        
        if section == 1{
            return "Business"
        }
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.teamTableView {
            
            let count = Roles?.count ?? 0
           
            return count
            
        }
        
        if tableView == self.companyListTableView {
            
            if section == 0{
                return 1
            }
            
            let count = self.organisation?.count ?? 0
           
            return count
            
        }

        
        if tableView == self.filterTableView{
            
            return filterArray.count
        }
        
        if tableView == self.expenseTableView{
  
            if imbersements?.count ?? 0 > 0 {
            
                return imbersements?.count ?? 0
            }else{
                
                if !self.filterPressed{
                    
                    return companyTitle.count
                    
                }
                
                
            }
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){

        if tableView == companyListTableView{
        let header = view as! UITableViewHeaderFooterView

          view.tintColor = .white
          header.textLabel?.font =  header.textLabel?.font.withSize(14)
          header.textLabel?.textColor = .lightGray
        }
      }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == companyListTableView{
            
            let cell = self.companyListTableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! CompanyTableViewCell
            
            if indexPath.section == 0{
                
            
                if let image = self.userProfile?.avatar{
                    let url = URL(string:Utils.getImage(id: image))
                    cell.imgLogo?.kf.setImage(with: url,placeholder:UIImage(named: "profile_pic"))
                   
                }
                if let name = self.userProfile?.name{
                    cell.lblTitle.text = name
                }
                
                
            }else{
                
                if let info = self.organisation?[indexPath.row]{
                    
                    if let image = info.logo{
                        let url = URL(string:Utils.getImage(id: image))
                        cell.imgLogo?.kf.setImage(with: url)
                    }
                    
                    
                    
                    cell.lblTitle.text = info.name
                    cell.lblFirstLetter.text = info.name?[0].uppercased()
                    cell.circleView.backgroundColor = self.colors?[indexPath.row]
                }
               
                
            }
     
            return cell
        }
        
        
        
        
        
        if tableView == filterTableView{
            
            let cell = self.filterTableView.dequeueReusableCell(withIdentifier: FilterTableCell, for: indexPath) as! FilterCell
            
            cell.lblTitle.text = filterArray[indexPath.row]
            cell.colorView.backgroundColor = Utils.setType(type: filterArray[indexPath.row])
            
            return cell
        }
        
        
        if tableView == teamTableView{
            
            let cell = self.teamTableView.dequeueReusableCell(withIdentifier: "teamCell") as! RoleCell
        
               
                if let role = Roles?[indexPath.row].team{
                    
                   cell.lblRole.text = role
              
            }
            
             return cell
            
        }
        
        
        if tableView == self.expenseTableView{
            
            if self.imbersements?.count ?? 0 > 0{
                
                let cell = self.expenseTableView.dequeueReusableCell(withIdentifier:ExpenseCell, for: indexPath) as! ExpenseTableViewCell
                
                
                cell.imgSample.isHidden = true
                
                if let info = self.imbersements?[indexPath.row]{
                    
                    cell.lblTitle.text = info.title?.uppercased()
                    
                    if let amount = info.amount{
                     
                        if let currency = info.currency{
                            
                            
                            if currency == "INR"{
                                
                                Defaults.setValue(currency, forKey:"currency")
                                
                                cell.lblAmount.text = currency.getCurrencySymbol(currencyCode: currency) + String(format: "%.2f", amount)
                            }else{
                                
                                cell.lblAmount.text = currency.getCurrencySymbol(currencyCode: currency) + String(format: "%.2f", amount)
                            }
                            
                            
                        }
                        
                    }
             
                    if let status = info.reimbursementStatus{
                        
                        if status == "Draft"{
                            
                            cell.bgView.backgroundColor = #colorLiteral(red: 0.9121361375, green: 0.9121361375, blue: 0.9121361375, alpha: 1)
                            cell.statusView.backgroundColor = Utils.setType(type: status)
                            
                        }else{
                            
                            cell.bgView.backgroundColor = .white
                            cell.statusView.backgroundColor = Utils.setType(type: status)
                        }
                        
                    }
                    
                    if let icon = info.category?.icon{
                        
                        let url = URL(string:getIcon(iconName:icon))
                        
                        cell.imgIcon.sd_setImage(with: url)
                        
                    }
                    
                    
                    if let teamName = info.team?.name{
                        
                        if teamName == "NA" {
                            
                            cell.lblType.text = "Personal"
                        }else{
                            
                            cell.lblType.text = teamName
                        }
                        
                        
                    }
                    
                    cell.lblDate.text = info.incurredDate?.getDate()
                
                }
                
             
                
                if indexPath.row == self.imbersementList.count - 1 {
                
                    if self.imbersementList.count < self.totalCount{
                        self.page = self.page + 1
                        getImbersementWithRole()
                    }
                    
                }
                
           return cell
                
                
            }else{
                
                
                let cell = self.expenseTableView.dequeueReusableCell(withIdentifier:ExpenseCell, for: indexPath) as! ExpenseTableViewCell
                
                
                cell.imgSample.isHidden = false
//                print(indexPath.row)
//                print(self.expenseTitle.count)
                if indexPath.row < self.expenseTitle.count{
                    
                    cell.lblTitle.text = self.expenseTitle[indexPath.row]
                    cell.lblType.text = self.companyTitle[indexPath.row]
                    cell.imgIcon.image = UIImage(named:self.icons[indexPath.row])
                    
                }
               
                
                if indexPath.row == 1{
                    
                    cell.statusView.backgroundColor = Utils.setType(type: "Draft")
                }
                
                if indexPath.row == 2{
                    
                    cell.statusView.backgroundColor = Utils.setType(type: "Requested")
                }
               
            
        return cell
        }
        }
        
        return UITableViewCell()
        
        
    }
    
}

extension HomeViewController : UIPickerViewDelegate,UIPickerViewDataSource{

func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
}

func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    if pickerView == rolePicker{
        return roleArray.count
    }
    
    return 0
}
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          
        if row == 0{
            return
        }else{
            
            self.txtRole.text = roleArray[row]
            self.pickedRole = rolesType[row]
        }
          
         
      }
      
      

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

                return roleArray[row]
                }
    
    
    
}

