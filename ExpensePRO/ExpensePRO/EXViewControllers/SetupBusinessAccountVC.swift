//
//  SetupBusinessAccountVC.swift
//  ExpensePRO
//
//  Created by Sajin M on 17/11/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alertift
import SVProgressHUD

class SetupBusinessAccountVC: BaseViewController {
    
    @IBOutlet weak var txtReferalCode: HoshiTextField!
    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var txtConfirmPassword: HoshiTextField!
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtOtp: HoshiTextField!
    
    var DashBoard = ResponseHandler()
    var tokenStr:String? = nil
    var registerParams = [String:Any]()
    var name:String?
    var email:String?
    var isConfigure:Bool = false
    var appleId:String?
    var isApple:Bool = false
    var isUsingCode:Bool = false
    var teamId:String?
    
    
    @IBOutlet weak var otpView: UIView!
    
    var Register = ResponseHandler()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let name = self.name{
            
            txtName.text = name
        }
        if let email = self.email{
            
            txtEmail.text = email
        }


        otpView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func setupNewBusiness(_ sender: Any) {
        
        
        let NewScene = NewOrganizationSetupVC.instantiate(fromAppStoryboard: .Main)
        
                         if let navigator = self.navigationController {
                        
                         
                             navigator.pushViewController(NewScene, animated: true)
                         
                  
                      }
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    

    @IBAction func submitPressed(_ sender: Any) {
        
        guard let txtCode = self.txtReferalCode.text, !txtCode.isEmpty else{
            
            self.showNotification(message: "Referal code should not be empty")
            
        return
        }
        
        guard let txtPwd = self.txtPassword.text, !txtPwd.isEmpty else{

            self.showNotification(message: passwordMessage)

        return
        }
        guard let txtConfirmPwd = self.txtConfirmPassword.text, !txtConfirmPwd.isEmpty else{

            self.showNotification(message: passwordMessage)

        return
        }
        
        guard let txtName = self.txtName.text, !txtName.isEmpty else{
            
            self.showNotification(message: "Please enter your name")
            
        return
        }
        
        guard let txtEmail = self.txtEmail.text, !txtEmail.isEmpty else{
            
            self.showNotification(message: "Please enter a valid email")
            
        return
        }
        
        guard txtPwd == txtConfirmPwd else{
            
            self.showNotification(message: "Password and Confirm Password should be same")
            return
        }
        
        self.isUsingCode = true
        
        if isConnected(){
            
           
         guard let txtCode = self.txtReferalCode.text,let txtEmail = self.txtEmail.text,let txtName = self.txtName.text,let txtConfirmPwd = self.txtConfirmPassword.text, let txtPwd = self.txtPassword.text,
            !txtConfirmPwd.isEmpty,!txtPwd.isEmpty, !txtName.isEmpty,!txtEmail.isEmpty,!txtCode.isEmpty else {
                      
             self.showNotification(message:mandatoryFields)
                      
                       return
                   }
            
         if txtEmail.isValidEmail(){

             
            let params = AccountRegistration(name: txtName,currency: "").Values
             
             self.registerParams = params
             
             self.updateProfile()
    
            }else{
                
             self.showNotification(message:validEmail)
                
            }
            
            
            
        }
        
  
        
    }
    
    func joinInvitation(id:String){
        
        
        
        SVProgressHUD.show()
        
        DashBoard.JoinRequests(requestId:id,withParameter: ["accepted":"true"]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                Switcher.updateRootVC()
                
            }else{
                
                Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                
            }
            
        }
        
    }
    
    func updateProfile(){
  
                
                guard let email = self.txtEmail.text else {
                    
                    return
                    
                }
         
                self.generateOtp(email: email)
                
            }
    
    @IBAction func submitOtpPressed(_ sender: Any) {
        
        
        guard let otp = self.txtOtp.text, !otp.isEmpty else{
            
            Alertift.alert(title: AppTitle, message:otpNotValid)
            .action(.default("OK"))
            .show(on: self)
            return
            
        }
        
        guard let email = self.txtEmail.text else {
            
            return
            
        }
        
   
        
        self.verifyOtp(email: email, otp: otp)
    }
    
    @IBAction func resendOtpPressed(_ sender: Any) {
        
        guard let email = self.txtEmail.text else {
            
            return
            
        }
        
        self.generateOtp(email: email)
        
    }
    
    
    
    func generateOtp(email:String){
        
   
        
        SVProgressHUD.show()
        
        Register.SendOtp(withParameter:["email":email,"accountType":"business"]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
                        self.otpView.isHidden = false
                    
               
            }else{
                
                
                
                if let isRegistered = self.Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        DispatchQueue.main.async {
                            
                            Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
                    
                        }
                        
                        self.backNavigation()
                       
                    }
                        
                    }else{
                        
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                    }
                
               
                

            }
            
            
                
            }
        
    }
    
    
    func verifyOtp(email:String,otp:String){
        
      
        
        SVProgressHUD.show()
        
        Register.VerifyOtp(withParameter:["email":email,"otp":otp]) { [self] (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
                self.txtOtp.text = ""
                self.otpView.isHidden = true
                
//                if let info = self.Register.OtpVerifyResponse?.data{
//
//                    if let id = info._id{
//                        self.teamId = id
//                    }
//                }
//
                
             
                
                if isConfigure{
                    
                    self.configureAccount(param: self.registerParams)
                    
                }else{
                    
                    self.doRegister(param: self.registerParams)
                    
                }
                
               
               
            }else{
                
             
                
              if let isRegistered = Register.OtpVerifyResponse?.data?.isRegistered{
                    
                    if isRegistered{
                        
                        self.txtOtp.text = ""
                        self.otpView.isHidden = true
                        Alertift.alert(title: AppTitle, message:message)
                        .action(.default("OK"))
                        .show(on: self)
                        
                        self.backNavigation()
                        
                    }
                    
                    
              }else{
                
                Alertift.alert(title: AppTitle, message:message)
                .action(.default("OK"))
                .show(on: self)
                
                
              }
                

            }
            
            
                
            }
        
    }
    
    
    func configureAccount(param:[String:Any]){
        
        
                SVProgressHUD.show()
        
                Register.ConfigureAnotherAccount(withParameter:param) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
        
                    if isSuccess{
                        
                                        if let info = self.Register.loginResponse?.data{
                        
                                            if let token = info.token{
                        
                        
                                                self.tokenStr = token
                                                Defaults.set(self.tokenStr,forKey:UserToken)
                                                Switcher.updateRootVC()
                                              
//                                                if let id = self.teamId{
//
//                                                    if self.isUsingCode{
//
//                                                        self.joinInvitation(id: id)
//                                                    }else{
//
//                                                        Switcher.updateRootVC()
//                                                    }
//
//
//                                                }else{
//
//                                                    Switcher.updateRootVC()
//
//                                                }
                                                
                                   
                                            }
                        
                        
                                        }
          
                                 }
                        else{
                    
                                    Alertift.alert(title: AppTitle, message:message)
                                    .action(.default("OK"))
                                    .show(on: self)
                    
                    
                                }
                    
                                
                    
                                }
                    
                        }
    
    
    
    func doRegister(param:[String:Any]){
        
        
                SVProgressHUD.show()
        
                Register.RegisterBusiness(withParameter:param) { (isSuccess, message) in
                    SVProgressHUD.dismiss()
        
                    if isSuccess{
                        
                                        if let info = self.Register.loginResponse?.data{
                        
                                            if let token = info.token{
                        
                        
                                                self.tokenStr = token
                                                Defaults.set(self.tokenStr,forKey:UserToken)
                                                Switcher.updateRootVC()

                        
                        
                                            }
                        
                        
                                        }
          
                                 }
                        else{
                    
                                    Alertift.alert(title: AppTitle, message:message)
                                    .action(.default("OK"))
                                    .show(on: self)
                    
                    
                                }
                    
                                
                    
                                }
                    
                        }
    

}
