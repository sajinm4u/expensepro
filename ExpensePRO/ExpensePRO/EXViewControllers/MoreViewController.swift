//
//  MoreViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 23/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import MessageUI
import SVProgressHUD
import Alertift
import Kingfisher
import DynamicColor

class MoreViewController: BaseViewController {
    
    @IBOutlet weak var menuTableView: UITableView!{
        didSet {
            menuTableView.tableFooterView = UIView(frame: .zero)
        }
    }
    
    
    var menuArray:[String] = []
    var menuIconsArray:[String] = []
    var userType:String?
    var organisation:[OrganisationData]?
    var userProfile:ProfileData?
    
    let originalColor = DynamicColor(hexString: "#c0392b")

    var gradient:DynamicGradient?
    var colors:[UIColor]?
    
    @IBOutlet weak var lblUserName: UILabel!
    var userPic:UIImage?
   
    
    var ApiRequest = ResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
        // Do any additional setup after loading the view.
    }
    
 
    
    override func viewDidAppear(_ animated: Bool) {
        
        initialSetup()
    }
    
    func initialSetup(){
        

        getUserProfile()
        self.gradient = DynamicGradient(colors: [blue, red, yellow])
        self.menuArray = menuTitle
        self.menuIconsArray = menuIcons
      
        
       // self.menuTableView.delegate = self
       // self.menuTableView.dataSource = self
        
    }
    
    
    func logOut(){
        
        Defaults.setValue(nil, forKey: UserToken)
        
        let NewScene = SignInViewController.instantiate(fromAppStoryboard: .Main)
        
        NewScene.hidesBottomBarWhenPushed = true

        if let navigator = self.navigationController {

            navigator.pushViewController(NewScene, animated: true)


        }

        
    }
    
    func confirmLogout() {

        let optionMenu = UIAlertController(title: nil, message: "Do you want to log out", preferredStyle: .alert)

      
            let yesAction = UIAlertAction(title: "Yes", style: .default) { _ in

                self.logOut()

            }

            let cancelAction = UIAlertAction(title: "No", style: .cancel)

            // 4
          
            optionMenu.addAction(yesAction)
            optionMenu.addAction(cancelAction)

            // 5
            self.present(optionMenu, animated: true, completion: nil)

    }
    
    
    func sendEmail() {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([customerCareEmail])
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    
    func getUserProfile(){
        
        SVProgressHUD.show()
        
        ApiRequest.getUserProfile(withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                if let info = self.ApiRequest.UserResponse?.data{
                    
                    self.userProfile = info
                
                    
                    if let email = info.email{
                        
                        Defaults.set(email, forKey: "email")
                        
                    }
                    
                    if let emailNot = info.emailNotification{
                        
                        Defaults.set(emailNot, forKey: "emailNotification")
                        
                    }
                    
                    if let mobileNot = info.enableNotification{
                        
                        Defaults.set(mobileNot, forKey: "mobNotification")
                        
                    }
                
                    
                    if let userId = info._id{
                        
                        Defaults.set(userId, forKey: "userID")
                    }
                    
                    if let avatar = info.avatar{
                        
                        Defaults.set(avatar, forKey: "userPic")
                        
                    }
                    
                    if let organisation = info.organisations{
                        self.organisation = organisation
                       if self.organisation?.count ?? 0 > 0{
                       
                            self.colors = self.gradient?.colorPalette(amount:UInt((self.organisation!.count) + 1))

                    
                        }
                    }
                    
                    

                    
                    self.menuTableView.delegate = self
                    self.menuTableView.dataSource = self
                    self.menuTableView.reloadData()
                    
                }
                
            }
            
            
            
        }
        
        
    }
    
    func uploadProfileImage(images:UIImage){
        
        guard let userId = Defaults.string(forKey: "userID") else{
            return
        }
        
        SVProgressHUD.show()
        

        let url = Api.profilePic + userId
        
        ApiRequest.UploadProfileImage(urlString:url,image: images) { (isSuccess, message)  in
            SVProgressHUD.dismiss()
            if (isSuccess){
               
                self.getUserProfile()
               
            }else{
                
               
               
            }
            
            
            
        }
        
        
    }
    
    func swichAccount(){
        
        SVProgressHUD.show()

        ApiRequest.ChangeAccount(withParameter:[:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                self.getUserProfile()
                
                DispatchQueue.main.async {
                    
                   
                
                    Alertift.alert(title: AppTitle, message:message)
                    .action(.default("OK"))
                    .show(on: self)
                    
                   
                    
                }
            

              
            }
            
            
            
        }
        
        
    }
    
    
    
}
extension MoreViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

        if self.organisation?.count ?? 0 > 0{
            
            return 4
            
        }
        
        return 3
        
        
      }
    
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){

        let header = view as! UITableViewHeaderFooterView

        
          view.tintColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
          header.textLabel?.font =  header.textLabel?.font.withSize(14)
          header.textLabel?.textColor = .lightGray
        
      }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if organisation?.count ?? 0 > 0{
            
            if section == 1{
                
                return "Business Accounts"
            }
            
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if organisation?.count ?? 0 > 0{
            if section == 1 || section == 3{
                return 40.0
            }
            
        }else{
            
            if section == 2{
                return 40.0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }
        if organisation?.count ?? 0 > 0{
            
            if section == 1{
                return self.organisation?.count ?? 0
            }
            
            if section == 2{
                return 1
            }
            
            if section == 3{
                
                return 3
            }
            
        }else{
            if section == 1{
                return 1
            }else{
                return menuArray.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0{
        
                self.menuTableView.separatorStyle = .none
                let cell = menuTableView.dequeueReusableCell(withIdentifier: "profileMenuCell", for: indexPath) as! MenuCell
                
                        if let name =  Defaults.string(forKey: "userName"){
                
                            cell.lblUsername.text =  "Hola " + name
                
                        }
                
                if let avatar =  Defaults.string(forKey: "userPic"){
                    
                    let url = URL(string:Utils.getImage(id: avatar))
                
                    //print(url)
                    
                    let phImage = UIImage(named: "profile_pic")
                    cell.iconImageView?.layer.cornerRadius = cell.iconImageView.frame.height/2
                    cell.iconImageView?.contentMode = .scaleToFill
                    cell.iconImageView?.kf.setImage(with: url, placeholder: phImage)
                
                }
                
                if let email = Defaults.string(forKey: "email"){
                    
                    cell.lblEmail.text = email
                }
                
                
          
    
                
                cell.delegate = self
                
                return cell
                
        
            
        }
        
        if organisation?.count ?? 0 > 0 {
            
            if indexPath.section == 1{
                
                let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! CompanyTableViewCell
                if let info = self.organisation?[indexPath.row]{
                    
                    if let image = info.logo{
                        let url = URL(string:Utils.getImage(id: image))
                        cell.imgLogo?.kf.setImage(with: url)
                    }
                
                    cell.lblTitle.text = info.name
                    cell.lblFirstLetter.text = info.name?[0].uppercased()
                    cell.circleView.backgroundColor = self.colors?[indexPath.row]
                }
                return cell
                
            }else if indexPath.section == 2{
                
                let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "addCell")!
               
                return cell
            }else{
                
                self.menuTableView.separatorStyle = .singleLine
                let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
                cell.menuTitle.text = menuArray[indexPath.row]
                cell.iconImageView.image = UIImage(named: menuIconsArray[indexPath.row])
            
                return cell
                
            }
            
        }else{
            
            if indexPath.section == 1{
                
                let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "addCell")!
               
                return cell
            }else{
                
                self.menuTableView.separatorStyle = .singleLine
                let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
                cell.menuTitle.text = menuArray[indexPath.row]
                cell.iconImageView.image = UIImage(named: menuIconsArray[indexPath.row])
            
                return cell
                
            }
            
        }
        
        

        return UITableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.organisation?.count ?? 0 > 0{
            
            print(indexPath.section)
            
            
            if indexPath.section == 1{
                
                if let info = self.organisation?[indexPath.row]{
                   
                let NewScene = CompanyViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    NewScene.organisation = info
                    if let color = self.colors?[indexPath.row]{
                        NewScene.bgColor = color
                    }
                   
                    navigator.pushViewController(NewScene, animated: true)
                    
                }
                
                }
                
            }
            
            if indexPath.section == 2{
                
                let NewScene = NewOrganizationSetupVC.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    navigator.pushViewController(NewScene, animated: true)
                    
                }
                
            }
            
            if indexPath.section == 3{
                
                if indexPath.row == 0{
                    
                    let NewScene = NotificationSettingsViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        navigator.pushViewController(NewScene, animated: true)
                        
                        
                    }

                }
           
            if indexPath.row == 1{
                
                self.sendEmail()
            }
            
            if indexPath.row == 2{
                
                self.confirmLogout()
            
            }
            }
            
       
        }else{
            
            if indexPath.section == 1{
                
                let NewScene = NewOrganizationSetupVC.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    navigator.pushViewController(NewScene, animated: true)
                    
                }
                
            }
            
            if indexPath.section == 2{
                
                if indexPath.row == 0{
                    
                    let NewScene = NotificationSettingsViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        navigator.pushViewController(NewScene, animated: true)
                        
                        
                    }

                }
           
            if indexPath.row == 1{
                
                self.sendEmail()
            }
            
            if indexPath.row == 2{
                
                self.confirmLogout()
            
            }
            }
            
        }
        
        /*
        if self.userType == "business"{
        
        if indexPath.row == 1{
            
            let NewScene = MyAccountViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                navigator.pushViewController(NewScene, animated: true)
                
            }
        }
        
        
        
        if indexPath.row == 2{
            
            let NewScene = PackageBillingViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                
                
                navigator.pushViewController(NewScene, animated: true)
                
                
            }
        }
        
        if indexPath.row == 3{
            
            let NewScene = TeamListViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
               
                navigator.pushViewController(NewScene, animated: true)
            
            }
        }
        
        if indexPath.row == 4{
            
            let NewScene = CategoriesListViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                
                navigator.pushViewController(NewScene, animated: true)
                
                
            }
        }
        
        
        if indexPath.row == 5{
            
            let NewScene = NotificationSettingsViewController.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                navigator.pushViewController(NewScene, animated: true)
                
                
            }
        }
        
        
        if indexPath.row == 6 {
            
            self.sendEmail()
            
        }
         
            
            if indexPath.row == 7{
                
                
                
                Defaults.setValue(nil, forKey: UserToken)         
                
                let NewScene = SignInViewController.instantiate(fromAppStoryboard: .Main)
                
                NewScene.hidesBottomBarWhenPushed = true

                if let navigator = self.navigationController {

                    navigator.pushViewController(NewScene, animated: true)


                }
            
            
        }
        
        }else{
            
            if indexPath.row == 1{
                
                let NewScene = MyAccountViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    navigator.pushViewController(NewScene, animated: true)
                    
                }
            }
            
            
            
            if indexPath.row == 2{
                
                let NewScene = PackageBillingViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    
                    
                    navigator.pushViewController(NewScene, animated: true)
                    
                    
                }
            }
            

            
            if indexPath.row == 3{
                
                let NewScene = CategoriesListViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    
                    navigator.pushViewController(NewScene, animated: true)
                    
                    
                }
            }
            
            
            if indexPath.row == 4{
                
                let NewScene = NotificationSettingsViewController.instantiate(fromAppStoryboard: .Main)
                
                if let navigator = self.navigationController {
                    
                    navigator.pushViewController(NewScene, animated: true)
                    
                    
                }
            }
            
            
            if indexPath.row == 5 {
                
                self.sendEmail()
                
            }
             
                
                if indexPath.row == 6{
                    
                    
                    
                    Defaults.setValue(nil, forKey: UserToken)
                    
                    let NewScene = SignInViewController.instantiate(fromAppStoryboard: .Main)
                    
                    NewScene.hidesBottomBarWhenPushed = true

                    if let navigator = self.navigationController {

                        navigator.pushViewController(NewScene, animated: true)


                    }
                
                
            }

    
        }
         */
    }
   
}

extension MoreViewController:MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
    
        controller.dismiss(animated: true)
        
        
    }
    
}
extension MoreViewController:menuDelegate{
    func changeAccount() {
        
        
        if let configuredType = Defaults.string(forKey: "configuredType"){
            
            if configuredType == "both"{
                
                Alertift.alert(title:AppTitle, message: "Do you want to switch account?" )
                    .action(.destructive("Yes")){_,_,_ in
                                      
                        self.swichAccount()
                                   
                                      
                              }.action(.cancel("No"))
                                  .show(on: self)

                
               
                
            }
        }
        

        
    }
    
    func changeProfilePic() {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
        
        
    }
    
    
    
}
extension MoreViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("\(info)")
        if let image = info[.originalImage] as? UIImage {
     
            self.uploadProfileImage(images:image)

            dismiss(animated: true, completion: nil)
        }
    }
}
