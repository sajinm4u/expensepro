//
//  ForgotPasswordViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 20/10/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import TextFieldEffects
import SVProgressHUD
import Alertift

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtEmail: HoshiTextField!
    
    var Register = ResponseHandler()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        guard let email = self.txtEmail.text else {
            
            return
            
        }
        
        if email.isValidEmail(){
            
            
            generateOtp(email:email)
            
        }else{
            
            self.showNotification(message:validEmail)
               
           }
    }
    
    
    func generateOtp(email:String){
        
   
        
        SVProgressHUD.show()
        
        Register.SendOtpToken(withParameter:["email":email]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
                let NewScene = NewPasswordViewController.instantiate(fromAppStoryboard: .Main)
                
               
                                  NewScene.email = email
                                 if let navigator = self.navigationController {
                                
                                 
                                     navigator.pushViewController(NewScene, animated: true)
                                 
                          
                              }

                       
               
            }else{
                
           
                            Alertift.alert(title: AppTitle, message:message)
                            .action(.default("OK"))
                            .show(on: self)
              
                    }
                        
                    }
            }
            
            
                
            }
        
  
