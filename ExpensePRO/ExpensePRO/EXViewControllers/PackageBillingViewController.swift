//
//  PackageBillingViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 05/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alertift
import MessageUI

class PackageBillingViewController: BaseViewController {

    @IBOutlet weak var btnSegmentControl: UISegmentedControl!
       
      
       @IBOutlet weak var subscriptionTableView: UITableView!{
       didSet {
       subscriptionTableView.tableFooterView = UIView(frame: .zero)
       //
       }
       }
       
       var getPackage = ResponseHandler()
       var packageString:String? = ""
       var memberShipId:String?
       var purposeString:String = ""
       var plandata:personalPlanData?
       var organisationData:businessPlanData?
       var userType:String = "personal"
       var currentPlanId:String? = ""
       
       var subscriptionData:[String]? = []
       var subcriptionHeader = ["Plan Name","Subscription Date","Renew Date"]
       
      var packages:[PlanStruct]?
       
       @IBOutlet weak var billingTableView: UITableView!{
           didSet {
               billingTableView.tableFooterView = UIView(frame: .zero)
           
           }
       }
       
       
       
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
           initialSetup()
             
           

           // Do any additional setup after loading the view.
       }
       
       
       @IBAction func segmentPressed(_ sender: UISegmentedControl) {
           
           switch sender.selectedSegmentIndex {
           case 0:
               
               self.billingTableView.isHidden = true
               self.subscriptionTableView.isHidden = false
               break
                         
              
           case 1:
             
               self.billingTableView.isHidden = false
               self.subscriptionTableView.isHidden = true
               break
               
               
           default:
             break
           }
           
           
       }
       
       
       @IBAction func backPressed(_ sender: Any) {
           
           self.navigationController?.popViewController(animated: true)
       }
       
       func initialSetup(){
           
           
           let font = UIFont.systemFont(ofSize: 12)
           btnSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                   for: .normal)
           
            if  Connectivity.isConnectedToInternet {
               
                getUserProfile()
                
                guard let userType = Defaults.string(forKey: "primary") else{
                    
                    return
                    
                }
                
                self.userType = userType
                
                getPlans(type: userType)
               
                //getPlans()
                //getBillingDetails()

             
               
            }else {
               
               Alertift.alert(title: AppTitle, message: noInternet)
                   .action(.default("OK"))
                   .show(on: self)
               
           }
           
           
           
       }
    
    
    
    func getPlans(type:String){
        
        
        SVProgressHUD.show()
        
        let url = Api.Plans + type
        
        getPackage.getPlans(urlString:url,withParameter:[:]) { (isSuccess, Message) in
            
            SVProgressHUD.dismiss()
            
            if isSuccess{
                
        
             if let info = self.getPackage.PlanResponse?.data{
                
                 
                 if info.count > 0{
                     
                     self.packages = info
                     
                     self.billingTableView.delegate = self
                     self.billingTableView.dataSource = self
                     self.billingTableView.reloadData()
                     
                     
                 }
                 
                 
             }

                
            }
        
              SVProgressHUD.dismiss()
            
        }
        
     }
    
    
    func sendEmail() {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject("Xpense.PRO Corporate Plan Quote")
            mail.setToRecipients([customerCareEmail])
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func getUserProfile(){
        
        SVProgressHUD.show()
        
        self.getPackage.getUserProfile(withParameter: [:]) { (isSuccess, message) in
            SVProgressHUD.dismiss()
            if (isSuccess){
                
                
                if self.userType == "personal"{
                    
                    
                    if let info = self.getPackage.UserResponse?.data?.personalPlan{
                        
                        if let planId = info.plan?.plan_id{
                            
                            self.currentPlanId = planId
                        }
                        

                        self.plandata = info
                        self.subscriptionTableView.delegate = self
                        self.subscriptionTableView.dataSource = self
                        self.subscriptionTableView.reloadData()
                       
                    }

                    
                    
                }else{
                    
                    
                    
                    if let info = self.getPackage.UserResponse?.data?.businessPlan{
                        
                        if let planId = info.plan?.plan_id{
                            
                            self.currentPlanId = planId
                        }
                        

                        self.organisationData = info
                        self.subscriptionTableView.delegate = self
                        self.subscriptionTableView.dataSource = self
                        self.subscriptionTableView.reloadData()
                       
                    }

                    
                    
                    
                }
                
                                
            }
            
            
            
        }
    }
  

       func getBillingDetails(){

         self.subscriptionTableView.delegate = self
         self.subscriptionTableView.dataSource = self
         self.subscriptionTableView.reloadData()


       }

    }



   extension PackageBillingViewController: UITableViewDelegate,UITableViewDataSource {
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.billingTableView{
            
            return packages?.count ?? 0
        }
           
           return 3
           
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           
           let cell = self.billingTableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
           
          
           
           if tableView == self.billingTableView {
            
            if indexPath.row == 3{

                cell.btnUpgrade.setTitle("GET QUOTE", for: .normal)

            }
            
            if self.userType == "personal" {
            
            if let info = packages?[indexPath.row]{
                
                if indexPath.row == 0{
                    
                    cell.btnUpgrade.isHidden = true
                    
                }
                
                if let title = info.name{
                   
                     cell.menuTitle.text = title
                    
                }
                
                if let price = info.price{
                    
                    cell.lblPrice.text = price.text
                }
                
            }
                
            }else{
                
                if let info = packages?[indexPath.row]{
                    
                    if let id = info.planId{
                        
                        if self.currentPlanId == id{
                            cell.btnUpgrade.isHidden = true
                        }
                        
                    }else{
                        
                        cell.btnUpgrade.isHidden = false
                    }
                    
                    if let title = info.name{
                       
                         cell.menuTitle.text = title
                        
                    }
                    
                    if let price = info.price{
                        
                        cell.lblPrice.text = price.text
                    }
                    
                }
                
                
                
                
            }
               

           
           }else{
               
               if (cell.btnUpgrade != nil){

                   cell.btnUpgrade.isHidden = true

               }
               
              
            cell.menuTitle.text = subcriptionHeader[indexPath.row]
            
            if self.userType == "personal"{

            if indexPath.row  == 0{


                if let name = self.plandata?.plan?.description{

                    cell.lblPrice.text = name
                }

            }
//
            if indexPath.row  == 1{

                if let sdate = self.plandata?.plan?.startsAt{

                    cell.lblPrice.text = sdate.getDate()
                }

            }
//
            if indexPath.row  == 2{

                if let edate = self.plandata?.plan?.endsAt{

                    if edate == ""{
                        
                        cell.lblPrice.text = "FREE"
                        
                    }else{
                       
                        cell.lblPrice.text = edate.getDate()
                    }
                    
                   
                }

            }
           
            }else{
                
                if indexPath.row  == 0{

                   
                    if let name = self.organisationData?.plan?.description{

                        cell.lblPrice.text = name
                    }

                }
    //
                if indexPath.row  == 1{

                    if let sdate = self.organisationData?.plan?.startsAt{

                        cell.lblPrice.text = sdate.getDate()
                    }

                }
    //
                if indexPath.row  == 2{

                    if let edate = self.organisationData?.plan?.endsAt{

                        if edate == ""{
                            
                            cell.lblPrice.text = "FREE"
                            
                        }else{
                           
                            cell.lblPrice.text = edate.getDate()
                        }
                        
                       
                    }

                }
                
           
                
            }
               
               
           }
          
           
           return cell
       }
    
    
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        
//        if tableView == self.billingTableView{
//           
//            if indexPath.row != 3{
//            
//             let NewScene = PackageDetailsViewController.instantiate(fromAppStoryboard: .Main)
//            
//            
//            guard let info = packages?[indexPath.row] else{
//                
//                return
//                    
//                }
//            
//            NewScene.packages = info
//            
//            if let navigator = self.navigationController {
//                            
//            navigator.pushViewController(NewScene, animated: true)
//                             
//                      
//                          }
//            
//            
//            
//            }else{
//                
//            sendEmail()
//                
//            }
//            
//            
//            
//       }
        
       }

}

extension PackageBillingViewController:MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
    
        controller.dismiss(animated: true)
        
        
    }
    
}
