//
//  MemberModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 23/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//

import Foundation

struct MemberModel:Codable {
    
    var statusCode:Int
    var status:String
    var message:String
    var data:Members?
}

struct Members:Codable{
    
    var members:[MemberData]?
    var teams:[teamData]?
}

struct MemberData:Codable {
    
    var _id:String?
    var name:String?
    var email:String?
}



