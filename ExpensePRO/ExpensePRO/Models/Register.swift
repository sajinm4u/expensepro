//
//  Register.swift
//  ExpensePRO
//
//  Created by Sajin M on 22/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct Register:Codable{
    let statusCode:Int
    let status:String
    let message:String
    var data:RegisterData?
}




struct RegisterData:Codable{
    
    var token:String?
}
