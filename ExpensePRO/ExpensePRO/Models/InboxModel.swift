//
//  InboxModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 04/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation



struct InboxModel:Codable {
    
    var statusCode:Int
    var status:String
    var message:String
    var data:notificationsArrayData?
}


struct notificationsArrayData:Codable {
    
    var notifications:[NotificationStructData]?
}


struct buttonsData:Codable {
    
    var id:String?
    var text:String?
    
    
}

struct NotificationStructData:Codable{
    
    var message:String
    var messageText:String?
    var isRead:Bool
    var buttons:[buttonsData]?
    var showButton:Bool
    var archive:Bool
    var created_at:String?
    var _id:String?
    var sender:senderStuct?
    var user:String?
    var currentStatus:String
    var data:dataStruct?
    var __v:Int?
    
    
}



struct senderStuct:Codable {
    
    var avatar:String?
    var _id:String?
    var name:String?
    
}

struct dataStruct:Codable {

    var _id:String?
    var type:String?
    var actionType:String?
    var icon:String?

}

