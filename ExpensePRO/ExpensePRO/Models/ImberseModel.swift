//
//  ImberseModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 22/07/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct ImberseModel:Codable {
    
    var statusCode:Int
    var status:String
    var message:String
    var data:imberseRequest?

    
}


struct imberseRequest:Codable {

    var request:reimberseArrayData?
    var status:[String]?
    
}

struct reimberseArrayData:Codable{

    var images:[String]?
    var reimbursementStatus:String?
    var isSubmitted:Bool
    var isDeletable: Bool
    var _id:String?
    var claimId:String?
    var title:String?
    var currency:String?
    var amount:Double?
    var incurredDate:String?
    var created_at:String?
    var category:category?
    var team:team?
    var user:user?
    var supervisorApproved:Bool?
    var accountantApproved:Bool?
    var paid:Bool?
    var isEditable:Bool?
    

}


struct category:Codable {
    
    var icon:String?
    var _id:String?
    var name:String?
}
  
struct team:Codable {
    
    var _id:String?
    var name:String?
}

struct user:Codable {
    
    var _id:String?
    var name:String?
    
}


struct reimbersmentModel:Codable{
        
    var reimbursements:[reimberseArrayData]?
        
}


//"data": {
//    "request": {
//      "images": [
//        "image-16020589621135f7d7ad104dafd78e9db6ec10.6101631291414171.jpeg"
//      ],
//      "reimbursementStatus": "Requested",
//      "sortOrder": 3,
//      "supervisorApproved": false,
//      "accountantApproved": false,
//      "paid": false,
//      "status": true,
//      "isSubmitted": true,
//      "step": 2,
//      "receiptCount": 1,
//      "isDeletable": false,
//      "_id": "5f7d7ad104dafd78e9db6ec1",
//      "claimId": "4228427006",
//      "title": "fb mngr",
//      "description": "",
//      "currency": "USD",
//      "amount": 30,
//      "category": {
//        "icon": "drinks_icon.svg",
//        "_id": "5e721ab1a9fe5e1ac235eb10",
//        "name": "Beverages"
//      },
//      "team": {
//        "_id": "5f7d7a4604dafd78e9db6ebb",
//        "name": "facebook"
//      },
//      "organisation": "5f7d79ae04dafd78e9db6eb6",
//      "user": {
//        "_id": "5f7d79ae04dafd78e9db6eb5",
//        "name": "Nimisha Valsan",
//        "email": "nimisha.valsan@gmail.com",
//        "role": "supervisor"
//      },
//      "incurredDate": "2020-10-07T01:00:00.000Z",
//      "comments": [],
//      "created_at": "2020-10-07T08:22:41.242Z",
//      "updated_at": "2020-10-07T08:22:41.242Z",
//      "__v": 0
//    },
//    "status": [],
//    "isEditable": false,
//    "isDeletable": false
//  }
//}
