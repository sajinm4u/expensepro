//
//  ImberseListModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 13/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct ImberseListModel:Codable {
    
    var statusCode:Int
    var status:String
    var message:String
    var data:imbersementData?
   
    
}

struct imbersementData:Codable{
    

    var reimbursements:[reimberseArrayData]?
    var total:Int?
    var currentPage:Int?
    var pageSize:Int?

}
