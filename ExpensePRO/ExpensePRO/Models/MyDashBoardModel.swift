//
//  MyDashBoardModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 03/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


struct MyDashBoardModel:Codable {
    
    var statusCode:Int
    var status:String
    var message:String
    var data:[String]?
        
    
}

