//
//  CategoryModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 14/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


struct CategoryModel:Codable {
    
       var statusCode:Int
       var status:String
       var message:String
       var data:[CategoryData]?
    
}


struct CategoryData:Codable{
    
    var icon:String?
    var _id:String?
    var name:String?
    
    
}
