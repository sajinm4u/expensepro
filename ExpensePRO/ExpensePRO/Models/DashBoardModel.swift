//
//  DashBoardModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 17/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct DashBoardModel:Codable{
    
      var statusCode:Int
      var status:String?
      var message:String
      var data:dashData?

}

struct dashData:Codable{
    
    let reimbursements:reimbersments?
    let categoryOverview:[categoryType]?
    let teamOverview:[teamoverview]?
    let monthlyExpense:[monthlyexpense]?
    let totalReimbursements:Double?
    let totalExpense:Double?
    let approvedExpense:Double?
    
    
}

struct reimburseType:Codable{
    
    var items:Int
    var amount:Double
    
}


struct reimbersments:Codable{
    
    
    var drafts:reimburseType?
    var saved:reimburseType?
    var requested:reimburseType?
    var rejected:reimburseType?
    var approved:reimburseType?
    var paid:reimburseType?
    var reviewed:reimburseType?
     
    
}

struct categoryType:Codable{
    
    var categoryName:String?
    var spend:String?
    var amount:String?

    
}

struct teamoverview:Codable{
    
    let _id:String?
    let name:String
    let active:Bool
    let spend:String?
    let amount:String?

}


struct monthlyexpense:Codable{
    
    let xlabel2:Int
    let xlabel1:String
    let spend:String
    
}
