//
//  LoginModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 6/12/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import Foundation
import UIKit

struct LoginModel:Codable {
    
    var statusCode:Int
    var status:String?
    var data:LoginResponseModel?
    var message:String
  
    
}


struct LoginResponseModel:Codable {
    
    var token:String?
    var isRegistered:Bool?
    
}




