//
//  UserProfileModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 15/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


struct UserProfileModel:Codable {
    
    var statusCode:Int
    var status:String
    var message:String
    var data:ProfileData?
    
    
}


struct ProfileData:Codable{
    
    
    var personalPlan:personalPlanData?
    var businessPlan:businessPlanData?
    var mobile:String?
    var avatar:String?
    var token:String?
    var paid:Bool?
    var trial:Bool?
    var isAdmin:Bool?
    var notifyToken:String?
    var device:String?
    var country:String?
    var currency:String?
    var personalCurrency:String?
    var status:Bool?
    var notificaionCount:Int?
    var isPlanValid:Bool?
    var enableNotification:Bool?
    var emailNotification:Bool?
    var personalOrgId:String?
    var _id:String?
    var name:String?
    var email:String?
    var role:String?
    var primary:primaryData?
    var organisations:[OrganisationData]?
    var businessEmail:String?
    var personalEmail:String?
    var primaryEmail:String?
    var configuredAccount:String?
   
}


struct primaryData:Codable{
    
    var name:String?
    var plan:planData?
    var _id:String?
    var type:String?
    var currency:String?
    var logo:String?
    
}


struct OrganisationData:Codable{
    
  var name:String?
  var plan:planData?
  var startsAt:String?
  var endsAt:String?
  var logo:String?
  var _id:String?
}



var plan_id:String?
var customer_id:String?
var description:String?
var startsAt:String?
var endsAt:String?
var amount:planAmount?

struct planData:Codable{
    
    var accountType:String?
    var plan:plans?

}

struct planAmount:Codable {
    
    var currency_code:String?
    var value:Int?
    
}

struct planCriteriaData:Codable {
      let users:Int?
      let receipts:Int?
      let categories:Int?
      let team:Int?
      let teamFlow:Bool?
      let reimbursementFlow:Bool?
    }

struct plans:Codable{
    
    var plan_id:String?
    var customer_id:String?
    var description:String?
    var startsAt:String?
    var endsAt:String?
    var amount:planAmount?
    
}

struct businessPlanData:Codable{
    
    var plan:plans?
    var paid:Bool?
    var isPlanValid:Bool?
    
}


struct personalPlanData:Codable{
    
    var plan:plans?
    var paid:Bool?
    var isPlanValid:Bool?
    
}

