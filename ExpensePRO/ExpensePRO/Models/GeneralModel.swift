//
//  GeneralModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 16/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct GeneralModel:Codable {
    
    var statusCode:Int
    var status:String?
    var message:String
    var data:GenericData?
}

struct GenericData:Codable {

    let _id:String?
    let isRegistered:Bool?
    let email:String?
    
}


struct PrimaryChange:Codable {
    
    var statusCode:Int
    var status:String?
    var message:String
    var data:String?
}
