//
//  ArticleModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 30/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//

import Foundation



struct ArticleModel:Codable {
    let statusCode:Int
    var status:String?
    var message:String
    var data:[articleData]?
}


struct articleData:Codable {
    var log:String?
    var reimbursement:String?
    var logged_at:String?
    
}


