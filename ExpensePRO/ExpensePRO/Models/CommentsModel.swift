//
//  CommentsModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 29/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//

import Foundation


struct CommentsModel:Codable {
    let statusCode:Int
    var status:String?
    var message:String
    var data:commentData?
}


struct commentData:Codable{
    var _id:String?
    var comments:[commentsStruct]?
    
}

struct commentsStruct:Codable{
    var status:Bool
    var createdAt:String?
    var _id:String?
    var user:commentUserData?
    var comment:String?
    
}

struct commentUserData:Codable{
    var _id:String?
    var name:String?
    var email:String?
}


