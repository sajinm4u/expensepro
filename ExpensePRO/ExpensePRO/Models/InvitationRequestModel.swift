//
//  InvitationRequestModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 23/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct InvitationRequestModel:Codable {
    
    var statusCode:Int
    var status:String
    var message:String
    var data:[RequestModel]?
    
}


struct RequestModel:Codable{
    
    let accepted:Bool
    let status:String?
    let invitationCode:String?
    let _id:String?
    let requester:reqesterData?
    let email:String?
    let team:TeamsData?
    let organisation:String?
    let role:String?
    

    
}

struct TeamsData:Codable {
    let _id:String?
    let name:String?
}


struct reqesterData:Codable{
    let _id:String?
    let name:String?
    let email:String?
    let role:String?
    
}

