//
//  PlanModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 05/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


struct PlanModel:Codable{
    
    var statusCode:Int
    var status:String
    var message:String
    var data:[PlanStruct]?
    
}


struct PlanStruct:Codable {
    
    var currency:String?
    var name:String?
    var type:String?
    var primary:String?
    var price:price?
    var users:users?
    var receipts:users?
    var trial:users?
    var categories:users?
    var teams:users?
    var currencySelection:String?
    var dashboard:String?
    var notifications:String?
    var report:String?
    var teamFlow:String?
    var reimbursementFlow:String?
    var comments:String?
    var support:String?
    var planId:String?
    

}


 
struct price:Codable {
    var text:String?
    var value:Int?
    var check:Bool
    var period:String?
}

struct users:Codable {
    var text:String?
    var value:Int?
    var check:Bool
    
}


