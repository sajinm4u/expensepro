//
//  TypeListModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 14/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


struct TypeListModel:Codable{
    
    var statusCode:Int
    var status:String
    var message:String
    var data:teamData?
    
}



struct teamData:Codable{
    
    var team:TypeData?
    var awaitingAction:[action]?
    
}

struct TypeData:Codable{
    
    var status:Bool
    var isAdmin:Bool
    var _id:String?
    var name:String?
    var user:String?
    var organisation:String?
    var members:[memberData]?
    var created_at:String?
    var __v:Int?
    
}

struct action:Codable{
    
    let accepted:Bool?
    let status:String?
    let invitationCode:String?
    let _id:String?
    let requester:usersData?
    let team:usersData?
    let organisation:String?
    let email:String?
    let role:String?
    
}



struct memberData:Codable{
    
    var exist:Bool
    var user:usersData?
    var role:String
    
}

struct usersData:Codable{
    
    var _id:String?
    var name:String?
    var email:String?
    var role:String?
 
}


