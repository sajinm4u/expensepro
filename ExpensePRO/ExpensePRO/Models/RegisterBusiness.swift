//
//  RegisterBusiness.swift
//  ExpensePRO
//
//  Created by Sajin M on 13/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//

import Foundation


struct RegisterBusiness:Codable{
    
    
    let statusCode:Int
    let status:String
    let message:String
    var data:regData?
    
   
}


struct regData:Codable{
    
        var _id:String
        var name:String?
     
}
