//
//  RolesModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 08/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct RolesModel:Codable{
    
    var statusCode:Int
    var status:String
    var message:String
    var data:[RolesData]?
    
    
}


struct RolesData:Codable{
    
    var _id:String?
    var team:String?
    var role:String?
    var isAdmin:Bool?
    var accepted:Bool?
    var type:String?
    
    
}

