//
//  TeamModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 02/10/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


struct TeamModel:Codable {
    
   let statusCode:Int
   let status:String
   let message:String
   let data:TeamData?
  
}


struct TeamData:Codable{
    
    let status:Bool
    let isAdmin:Bool
    let _id:String
    let name:String?
    let user:String?
    let organisation:String?
    let members:[MemberDataModel]?
    
    
}
    
    
struct MemberDataModel:Codable{
    
    let exist:Bool
    let _id:String
    let user:String
    let role:String
    let added_at:String
    
}


    
