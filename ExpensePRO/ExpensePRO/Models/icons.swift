//
//  icons.swift
//  ExpensePRO
//
//  Created by Sajin M on 27/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation

struct icons:Codable {
    
    let statusCode:Int
    let status:String
    let message:String
    let data:[String]?
}
