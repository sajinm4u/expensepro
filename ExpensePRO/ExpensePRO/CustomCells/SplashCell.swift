//
//  SplashCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 21/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

class SplashCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    
    
    
}
