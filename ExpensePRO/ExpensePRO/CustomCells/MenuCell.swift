//
//  MenuCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 04/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

protocol menuDelegate {
    
    func changeAccount()
    func changeProfilePic()
}

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var lblUsername: UILabel!
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var menuTitle: UILabel!
    
    @IBOutlet weak var btnUpgrade: UIButton!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblFirst: UILabel?
    
    
   // var delegate:menuCellDelgate?
    
    var userImg:UIImage?
    var delegate:menuDelegate?
    
    @IBOutlet weak var lblPackage: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
    
    @IBAction func cameraPressed(_ sender: Any) {
        
        delegate?.changeProfilePic()
        
    }
    
    

    @IBAction func typePressed(_ sender: Any) {
        
        delegate?.changeAccount()
    }
    
   
}
