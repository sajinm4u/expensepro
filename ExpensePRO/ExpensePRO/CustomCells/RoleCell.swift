//
//  RoleCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 08/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

protocol RoleDelegate {
    func roleJoin(at index:Int)
}

class RoleCell: UITableViewCell {
    
    
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var lblTeam: UILabel?
    @IBOutlet weak var joinView: CurvedView!
    var index:Int?
    var delegate:RoleDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func joinPressed(_ sender: Any) {
        
        guard let index = self.index else {
            return
        }
        
        delegate?.roleJoin(at: index)
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
