//
//  NotificationSettingCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 29/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit
protocol NotificationSettingDelegate {
    func swtichSelected(tag:Int,isEnable:Bool)
}

class NotificationSettingCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnSwitch: UISwitch!
    var delegate:NotificationSettingDelegate?
    
    @IBAction func switchPressed(_ sender: UISwitch) {
        
        var isEnabled = false
        
        if sender.isOn{
            
            isEnabled = true
            
        }else{
            
            isEnabled = false
            
        }
        
        delegate?.swtichSelected(tag: btnSwitch.tag,isEnable: isEnabled)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
