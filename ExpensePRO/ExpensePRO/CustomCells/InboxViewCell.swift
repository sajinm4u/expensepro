//
//  InboxViewCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 04/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

protocol InboxDelegate{
    
    func archiveMessage(index at:Int)
    func addTeam(index at:Int)
    func upgrade(index at:Int)
    func reject(index at:Int)
    func approve(index at:Int)
    func pay(index at:Int)
    func payReject(index at:Int)
    
}

class InboxViewCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var curvedView: CurvedView!
    
    @IBOutlet weak var btnUpgrade: UIButton!
    @IBOutlet weak var btnTeam: UIButton!
    @IBOutlet weak var viewArchive: UIView!
    @IBOutlet weak var approveView: UIView!
    @IBOutlet weak var payView: UIView!
    
    var showUpgrade:Bool = false
    var showApprove:Bool = false
    var delegate:InboxDelegate?
    var index:Int?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.curvedView.layer.cornerRadius = self.curvedView.frame.height / 2
        
        
        // Initialization code
    }
    
   
    @IBAction func upgradePressed(_ sender: Any) {
        
        if let index = self.index{
            
            delegate?.upgrade(index: index)
        }
        
    }
    
    @IBAction func payRejectPressed(_ sender: Any) {
        
        if let index = self.index{
            delegate?.payReject(index: index)
        }
    }
    
    @IBAction func payPressed(_ sender: Any) {
        
        if let index = self.index{
            delegate?.pay(index: index)
        }
    }
    
    @IBAction func addTeamPressed(_ sender: Any) {
        if let index = self.index{
            
            delegate?.addTeam(index: index)
        }
    }
    
    @IBAction func archivePressed(_ sender: Any) {
        if let index = self.index{
            
            delegate?.archiveMessage(index: index)
        }
    }
    
    @IBAction func approvePressed(_ sender: Any) {
        if let index = self.index{
            
            delegate?.approve(index: index)
        }
    }
    
    @IBAction func rejectPressed(_ sender: Any) {
        if let index = self.index{
            
            delegate?.reject(index: index)
        }
    }
    
    
    
    func setViews(isApprove:Bool,isUpgrade:Bool,isAdd:Bool,isArchive:Bool,isPay:Bool){
    
            self.approveView.isHidden = isApprove
            self.viewArchive.isHidden = isArchive
            self.btnUpgrade.isHidden = isUpgrade
            self.btnTeam.isHidden = isAdd
            self.payView.isHidden = isPay
  
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
