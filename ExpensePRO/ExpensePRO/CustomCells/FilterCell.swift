//
//  FilterCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 21/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var colorView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.colorView.layer.cornerRadius = self.colorView.frame.height/2
        self.colorView.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
