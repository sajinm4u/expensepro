//
//  CompanyTableViewCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 13/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var circleView: CurvedView!
    @IBOutlet weak var lblFirstLetter: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgLogo.layer.cornerRadius = self.imgLogo.frame.height/2
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
