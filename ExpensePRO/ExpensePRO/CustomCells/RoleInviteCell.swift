//
//  RoleInviteCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 23/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

class RoleInviteCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ic_member: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
