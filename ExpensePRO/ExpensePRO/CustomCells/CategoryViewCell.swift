//
//  CategoryViewCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 15/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

class CategoryViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
}
