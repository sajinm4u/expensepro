//
//  ExpenseTableViewCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 10/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var bgView: CurvedView!
    @IBOutlet weak var imgSample: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        DispatchQueue.main.async {
           self.statusView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 3.0)
           self.statusView.layer.masksToBounds = true
        }
        
        
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
