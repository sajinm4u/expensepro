//
//  CurvedButton.swift
//  ExpensePRO
//
//  Created by Sajin M on 09/07/2021.
//  Copyright © 2021 Codelattice. All rights reserved.
//



import Foundation
import UIKit


@IBDesignable class CurvedButton: UIButton {
    
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.5 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
       var shadowRadius: CGFloat {
           get {
               return layer.shadowRadius
           }
           set {
               layer.masksToBounds = false
               layer.shadowRadius = newValue
           }
       }

       @IBInspectable
       var shadowOpacity: Float {
           get {
               return layer.shadowOpacity
           }
           set {
               layer.masksToBounds = false
               layer.shadowOpacity = newValue
           }
       }

       @IBInspectable
       var shadowOffset: CGSize {
           get {
               return layer.shadowOffset
           }
           set {
               layer.masksToBounds = false
               layer.shadowOffset = newValue
           }
       }

       @IBInspectable
       var shadowColor: UIColor? {
           get {
               if let color = layer.shadowColor {
                   return UIColor(cgColor: color)
               }
               return nil
           }
           set {
               if let color = newValue {
                   layer.shadowColor = color.cgColor
               } else {
                   layer.shadowColor = nil
               }
           }
       }
    
    
    override func awakeFromNib() {
          super.awakeFromNib()
         
      }
      override init(frame: CGRect) {
          super.init(frame: frame)
         
      }
      
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
      }
    
}

