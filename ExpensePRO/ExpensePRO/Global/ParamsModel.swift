//
//  ParamsModel.swift
//  ExpensePRO
//
//  Created by Sajin M on 03/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation
import UIKit


struct DashBoardParams {
    
    var startDate:String
    var endDate:String
    var type:String
    var organisation:String
    
    var Values: [String: String] {
           return [
               "startDate":startDate,
               "endDate":endDate,
               "type":type,
               "organisation":organisation
              
           ]
       }
    
    
}


struct ImbersmentParams {
    
    var status:String = ""
    var role:String = ""
    var category:String = ""
    var keyword:String = ""
    var start_date:String
    var end_date:String
    var organisation:String
    
    var Values: [String: String] {
           return [
               
               "start_date":start_date,
               "end_date":end_date,
               "role":role,
               "category":category,
               "keyword":keyword,
               "organisation":organisation,
               "status":status
              
           ]
       }
    
    
}




struct ProfileParams {
    
    var name:String
    var email:String
    var mobile:String
    var password:String
    var role:String
    var notifyToken:String = Defaults.string(forKey:"deviceToken") ?? ""
    var device:String = "iOS"
    var organisation:String
    
    
    
    var Values: [String: String] {
           return [
               "name":name,
               "email":email,
               "mobile":mobile,
               "password":password,
               "role":role,
               "notifyToken":notifyToken,
               "device":device,
               "organisation":organisation,
               
              
           ]
       }
    
    
}

struct userRegistration{
    
      var name:String = ""
      var email:String = ""
      var mobile:String = ""
      var password:String = ""
      var notifyToken:String = Defaults.string(forKey:"deviceToken") ?? ""
      var device:String = "iOS"
      var currency:String = ""
      var appleId:String = ""
    
    
    var Values: [String: String] {
           return [
        
               
               "name":name,
               "currency":currency,
               "email":email,
               "mobile":mobile,
               "password":password,
               "device":device,
               "notifyToken":notifyToken
            
            
              
           ]
       }

}

struct AccountRegistration{
    
 
     
      //var invitationCode:String = ""
      var name:String = ""
//      var email:String = ""
//      var mobile:String = ""
//      var password:String = ""
//      var notifyToken:String = "iOS"
//      var device:String = "iOS"
//      var organisation:String = ""
      var currency:String = ""
  //    var appleId:String = ""
    
    
    var Values: [String: String] {
           return [
        
               
               "name":name,
               "currency":currency,
              
           ]
       }

}


struct configAccount {
    
    
    var invitationCode:String = ""
    var currency:String = ""
    var email:String = ""
    var organisation:String = ""
   
    
    var Values: [String: String] {
           return [
            
               "invitationCode":invitationCode,
               "currency":currency,
               "organisation":organisation,
               "email":email
              
           ]
       }
    
}



struct NewImberseParams {
    

    var title:String
    var description:String
    var amount:String
    var images:[String] = []
    var category:String
    var team:String
    var isSubmitted:Bool
    var incurredDate:String
    var receiptCount:String
    var organisation:String
    
    var Values: [String: Any] {
           return [
               "title":title,
               "description":description,
               "amount":amount,
               "images":images,
               "category":category,
               "team":team,
               "isSubmitted":isSubmitted,
               "incurredDate":incurredDate,
               "receiptCount":receiptCount,
               "organisation":organisation
           ]
       }
    
    
}





