//
//  ApiManager.swift
//  ExpensePRO
//
//  Created by Sajin M on 6/12/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiManager {
    
    private init () {}
    static let shared = ApiManager()
    var usrToken:String?
    
       
    let ApiHandler = RestHandler()
    
    var userTokenString:String?
    
    
    
    
    
    
    func getHeader() -> [String:String] {
  
        if let userToken = Defaults.string(forKey: UserToken){
        
             return ["x-auth-token":userToken]
        }
        
        return ["":""]
       
    }
    
    
    
    func doLogin(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Int,LoginModel?,String) -> Void))  {
        
        

        //ApiHandler.requestRawData(toURL: url, method: .post,parameters: withParameter) { (response) in
        
        Alamofire.request(url , method: .post, parameters: withParameter, encoding: JSONEncoding.default, headers:nil ).responseData {
        (response) -> Void in



            do {
                
                print(withParameter)
                

                if (response.response!.statusCode >= 200 && response.response!.statusCode < 300) {

            

                let decoder = JSONDecoder()
                let model = try decoder.decode(LoginModel.self, from:
                    response.data!)


                DispatchQueue.main.async {
                    completion(model.statusCode,model,model.message)
                }


              }else{

                  DispatchQueue.main.async {
                      completion(400,nil,wentWrong)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:LoginModel? = nil

                DispatchQueue.main.async {
                    completion(400,model,wentWrong)
                }

            }



        }
        
}
    
    func getCategoryData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,CategoryModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     //let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(CategoryModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:CategoryModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    func getArticleData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,ArticleModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default,headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(ArticleModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:ArticleModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    func postCommentData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,CommentsModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.post, parameters: withParameter,encoding: URLEncoding.default,headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(CommentsModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:CommentsModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    func getCommentData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,CommentsModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default,headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(CommentsModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:CommentsModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    
    
    
    func getPlanData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,PlanModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     //let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(PlanModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:PlanModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    func getInboxListData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,InboxModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                  //  print(self.getHeader())

                     //let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(InboxModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:InboxModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    func archiveInboxData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,InboxModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.put, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     //let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(InboxModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:InboxModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    func getMembersListData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,MemberModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    
                   print(url)
                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    print(self.getHeader())
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(MemberModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:MemberModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    func getRolesListData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,RolesModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    print(self.getHeader())
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(RolesModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:RolesModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    func joinRequestsData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,GeneralModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.put, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(GeneralModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:GeneralModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    func getRequestsData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,InvitationRequestModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(InvitationRequestModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:InvitationRequestModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    
    
    
    func addTeamData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,TeamModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.post, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    
                    
//                    print(self.getHeader())
//                    print(withParameter)

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(TeamModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    var message = ""
                    
                    do{
                    let json = try JSONSerialization.jsonObject(with: responseData.data!, options: []) as? [String : Any]
                        
                        message = json?["message"] as? String ?? "Please contact your admin"
                       
                    }catch{ print("erroMsg") }
                    print(parsingError)
                    let model:TeamModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,message)
                    }
                    
                }
                
                
                
            }
            
    }
    
    func getCatIconsData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,icons?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(icons.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:icons? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    func getTypeListData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,TypeListModel?,String) -> Void))  {
            
            
        Alamofire.request(url, method:.get, parameters: withParameter,encoding: URLEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    
                    print(self.getHeader())

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(TypeListModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:TypeListModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    func getSingleImbersementData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,ImberseModel?,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter, headers:getHeader()).responseData {
            (responseData) -> Void in
            
            
            
            do {
                
                
                print(withParameter)
                
                print(self.getHeader())

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(ImberseModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                let model:ImberseModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
        func getImbersementData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,ImberseListModel?,String) -> Void))  {
            
            
            Alamofire.request(url, method:.post, parameters: withParameter,encoding:JSONEncoding.default, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    
          
                    print(withParameter)
//                    
//                    print(self.getHeader())

                    let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(ImberseListModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:ImberseListModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
        func addNewTeamData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,ImberseListModel?,String) -> Void))  {
            
            
            Alamofire.request(url, method:.post, parameters: withParameter, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                
                
                do {
                    

                   //  let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(ImberseListModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    let model:ImberseListModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    func addSecondaryAccount(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,LoginModel?,String) -> Void))  {
        
        
        Alamofire.request(url, method:.put, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
            
            print(withParameter)
           
            
            do {
                
                print(url,withParameter)

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(LoginModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:LoginModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    
    func AddBusinessData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,RegisterBusiness?,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
            
           
            
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(RegisterBusiness.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:RegisterBusiness? = nil
    
                DispatchQueue.main.async {
                    completion(400,model,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    
    func UserRegistrationData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,LoginModel?,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter).responseData {
            (responseData) -> Void in
            
           
            
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(LoginModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:LoginModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    func removeCategories(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.delete, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
            
           
            
            do {
            
                let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    func addCategoryName(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
            
           
            
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    
    func InviteMember(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
            
           
            
            do {
                
                print(withParameter)

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    
    
    func NewImberseData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String,GeneralModel?) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter,encoding:JSONEncoding.default, headers: getHeader()).responseData {
            (responseData) -> Void in
            
          // print(withParameter)
            
            do {
                
               // print(self.getHeader())

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message,model)
                }
                
                
            } catch let parsingError {
                
           
                
                var message = "Please contact your admin"
                
                do{
                let json = try JSONSerialization.jsonObject(with: responseData.data!, options: []) as? [String : Any]
                    
                    message = json?["message"] as? String ?? "Please contact your admin"
                   
                }catch{ print("erroMsg") }
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,message,model)
                }
                
            }
            
            
            
        }
        
}
    
    func statusChangeImbersement(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.put, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
           
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    func DetailImberseData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,ImberseModel?,String) -> Void))  {
        
        
        Alamofire.request(url, method:.get, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
            
        
            do {
                
//                print(self.getHeader())
//                print(url)

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(ImberseModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:ImberseModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    func DeleteImberseData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.delete, parameters: withParameter, headers: getHeader()).responseData {
            (responseData) -> Void in
            
           
            
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    func UpdateImberseData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String,GeneralModel?) -> Void))  {
        
        
        Alamofire.request(url, method:.put, parameters: withParameter,encoding:JSONEncoding.default, headers: getHeader()).responseData {
            (responseData) -> Void in
     
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message,model)
                }
                
                
            } catch let parsingError {
                
                var message = ""
                
                do{
                let json = try JSONSerialization.jsonObject(with: responseData.data!, options: []) as? [String : Any]
                    
                    message = json?["message"] as? String ?? "Please contact your admin"
                   
                }catch{ print("erroMsg") }
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model?.message ?? wentWrong,model)
                }
                
            }
            
            
            
        }
        
}
    
    
    func updateSetting(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((GeneralModel,Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.put, parameters: withParameter,headers: getHeader()).responseData {
            (responseData) -> Void in
            
           
            
            do {
                print(withParameter)
                print(self.getHeader())
                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model,model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(model!,400,model?.message ?? wentWrong)
                }
                
            }
   
        }
        
}

    
    func OtpForgotDataUpdate(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((GeneralModel,Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.put, parameters: withParameter).responseData {
            (responseData) -> Void in
            
           
            
            do {
                print(withParameter)
                print(self.getHeader())
                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model,model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(model!,400,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    func OtpUpdateData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((GeneralModel,Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter).responseData {
            (responseData) -> Void in
        
            
            do {
                

                  let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model,model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                

                let model:GeneralModel? = nil
    
                DispatchQueue.main.async {
                    completion(model!,400,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    
    func UpdateBillData(fromUrl url: URL, image:UIImage, completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                let imgData = image.jpegData(compressionQuality: 0.5)!
                
               
                   multipartFormData.append(imgData, withName:"image",fileName: "image.jpg", mimeType: "image/jpg")


        },
            to: url,
            method: .post,
            headers:getHeader(),
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                         print(progress)
                    })
                    
                    upload.responseJSON { response in
                        
                        // If the request to get activities is succesfull, store them
                        if response.result.isSuccess{
                            
                            
                            let jsonData = JSON(response.result.value!)
                            DispatchQueue.main.async {
                             completion(200,"success")
                           }
                      
                        } else {
                           
                            let jsonData = JSON(response.result.value!)
                            DispatchQueue.main.async {
                                                completion(400,wentWrong)
                                            }
                            
                            
                        }
                        
                        
                    }
                case .failure(let encodingError):
                    
                    print(encodingError)
                    //                    print(encodingError)
                }
        }
        )
     
}
    
    
        func updateUserProfileData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
            
            
            Alamofire.request(url, method:.put, parameters: withParameter, headers:getHeader()).responseData {
                (responseData) -> Void in
                
               
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(GeneralModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    
                    let model:GeneralModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
        func getUserProfileData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,UserProfileModel?,String) -> Void))  {
            
            
            Alamofire.request(url, method:.get, parameters: withParameter, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                print(self.getHeader())
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(UserProfileModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    
                    let model:UserProfileModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    func getDashboardData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,DashBoardModel?,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter, headers:getHeader()).responseData {
            (responseData) -> Void in
            
//            print(self.getHeader())
//            print(withParameter)
            
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(DashBoardModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model,model.message)
                }
                
                
            } catch let parsingError {
                print(parsingError)
                let model:DashBoardModel? = nil
    
                DispatchQueue.main.async {
                    completion(400,model,model?.message ?? wentWrong)
                }
                
            }
            
            
            
        }
        
}

    func changePrimaryType(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter, headers:getHeader()).responseData {
            (responseData) -> Void in
            
            print(self.getHeader())
            
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
               
                DispatchQueue.main.async {
                    completion(400,wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
    func changeAccountType(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,String) -> Void))  {
        
        
        Alamofire.request(url, method:.post, parameters: withParameter, headers:getHeader()).responseData {
            (responseData) -> Void in
            
            //print(self.getHeader())
            
            do {
                

                 let jsonString = String(data: responseData.data!, encoding: .utf8)
                 
                
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode(PrimaryChange.self, from:
                    responseData.data!) //Decode JSON Response Data
                //(model)
                
                DispatchQueue.main.async {
                    completion(model.statusCode,model.message)
                }
                
                
            } catch let parsingError {
                
                print(parsingError)
                
               
                DispatchQueue.main.async {
                    completion(400,wentWrong)
                }
                
            }
            
            
            
        }
        
}
    
        func getMyDashboardData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Int,MyDashBoardModel?,String) -> Void))  {
            
            
            Alamofire.request(url, method:.post, parameters: withParameter, headers:getHeader()).responseData {
                (responseData) -> Void in
                
                print(self.getHeader())
                
                do {
                    

                     let jsonString = String(data: responseData.data!, encoding: .utf8)
                     
                    
                    //here dataResponse received from a network request
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(MyDashBoardModel.self, from:
                        responseData.data!) //Decode JSON Response Data
                    //(model)
                    
                    DispatchQueue.main.async {
                        completion(model.statusCode,model,model.message)
                    }
                    
                    
                } catch let parsingError {
                    
                    print(parsingError)
                    
                    let model:MyDashBoardModel? = nil
        
                    DispatchQueue.main.async {
                        completion(400,model,model?.message ?? wentWrong)
                    }
                    
                }
                
                
                
            }
            
    }
    
    
    
    
}
