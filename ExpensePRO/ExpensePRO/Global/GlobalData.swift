//
//  GlobalData.swift
//  ExpensePRO
//
//  Created by Sajin M on 21/12/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


struct GlobalData {
    
     static private var splashArray:[String] = ["discussion","business","expense"]
     static private var splashHeaderArray:[String] = ["Account Selection","Business","Add Expense"]
     static private var splashTextArray:[String] = ["Track your personal expenditure by setting up your personal account                                                               Get reimbursement process simplified with your business account", "Get invited to a team or setup a new organization","Enter expense details,capture receipt and submit for approval."]
    
     static func SplashArray () -> [String]? {
           return splashArray
       }
    
    
    static func SplashHeaderArray () -> [String]? {
        return splashHeaderArray
    }
    
    static func SplashTextArray () -> [String]? {
        return splashTextArray
    }
    
}
