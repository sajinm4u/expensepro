//
//  RestHandler.swift
//  RestHandler
//
//  Created by Sajin M on 6/20/19.
//  Copyright © 2019 CL. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]
public typealias Parameters = [String: String]
public typealias Params = [String:Any]


class RestHandler {
    
    var requestHttpHeaders = RestEntity()

    var urlQueryParameters = RestEntity()
    
    var httpBodyParameters = RestEntity()
    
    var httpBody: Data?
    
    
    
    
    
    static func getPostString(params:[String:Any]) -> String
       {
           var data = [String]()
           for(key, value) in params
           {
               data.append(key + "=\(value)")
           }
           return data.map { String($0) }.joined(separator: "&")
       }

        func callPost(url:URL, params:[String:Any], finish: @escaping ((message:String, data:Data?)) -> Void)
       {
           var request = URLRequest(url: url)
           request.httpMethod = "POST"

        let postString = RestHandler.self.getPostString(params: params)
           request.httpBody = postString.data(using: .utf8)

           var result:(message:String, data:Data?) = (message: "Fail", data: nil)
           let task = URLSession.shared.dataTask(with: request) { data, response, error in

               if(error != nil)
               {
                   result.message = "Fail Error not null : \(error.debugDescription)"
               }
               else
               {
                   result.message = "Success"
                   result.data = data
               }

               finish(result)
           }
           task.resume()
       }

    
    
    func requestFormData ( toURL url: URL,method: HTTPMethod,
                   parameters: Parameters? = nil,
                   headers: HTTPHeaders? = nil, completion: @escaping (_ result: Results) -> Void ){
        
       
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                
               
                  var request = URLRequest(url: url)
                request.httpMethod = method.rawValue

                let postString = RestHandler.self.getPostString(params: parameters!)
                       request.httpBody = postString.data(using: .utf8)

                       var result:(message:String, data:Data?) = (message: "Fail", data: nil)
                       let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        
                        
                        if(error != nil)
                                      {
                                          result.message = "Fail Error not null : \(error.debugDescription)"
                                        completion(Results(withError: CustomError.failedToCreateRequest))
                                        return
                        }else{
                            
                            completion(Results(withData: data,
                                                                              response: Response(fromURLResponse: response),
                                                                              error: error))
                            
                        }
                        
                        
                               
                            }
                            task.resume()
                        }
            
            
            
            
            
       
        
    }
    
    
    func requestRawData (toURL url: URL,method: HTTPMethod,
                   parameters: Parameters? = nil,
                   headers: HTTPHeaders? = nil, completion: @escaping (_ result: Results) -> Void ){
        
       
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                
               
               let jsonData = try? JSONSerialization.data(withJSONObject: parameters)
                
                  var request = URLRequest(url: url)
                  request.httpMethod = method.rawValue
                  request.httpBody = jsonData

               

                       var result:(message:String, data:Data?) = (message: "Fail", data: nil)
                       let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                        
                        
                        if(error != nil)
                                      {
                                          result.message = "Fail Error not null : \(error.debugDescription)"
                                        completion(Results(withError: CustomError.failedToCreateRequest))
                                        return
                        }else{
                            
                            completion(Results(withData: data,
                                    response: Response(fromURLResponse: response),
                                    error: error))
                            
                        }
                        
                        
                               
                            }
                            task.resume()
                        }
            
            
       
        
    }
    
    
    
    func request ( toURL url: URL,method: HTTPMethod,
                   parameters: Parameters? = nil,
                   headers: HTTPHeaders? = nil, completion: @escaping (_ result: Results) -> Void ){
        
       
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                
               
                let targetURL = self?.addURLQueryParameters(toURL: url, with: parameters ?? [:])
                
                            let httpBody = self?.getHttpBody()
                
                guard let request = self?.prepareRequest(withURL: targetURL, httpBody: httpBody, httpMethod: method, header: headers) else
                            {
                                completion(Results(withError: CustomError.failedToCreateRequest))
                                return
                            }
                
                            let sessionConfiguration = URLSessionConfiguration.default
                            let session = URLSession(configuration: sessionConfiguration)
                            let task = session.dataTask(with: request) { (data, response, error) in
                                completion(Results(withData: data,
                                                   response: Response(fromURLResponse: response),
                                                   error: error))
                            }
                            task.resume()
                        }
            
            
            
            
            
       
        
    }
    
    
    private func prepareRequest(withURL url: URL?, httpBody: Data?, httpMethod: HTTPMethod,header:HTTPHeaders?) -> URLRequest? {
        guard let url = url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        if let headers = header{
          
            for (header, value) in headers{
                request.setValue(value, forHTTPHeaderField: header)
            }
            
        }
        
        
        
        request.httpBody = httpBody
        return request
    }
    
    
    private func addURLQueryParameters(toURL url: URL, with parameters:Parameters) -> URL {
        
        if parameters.count > 0 {
            guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return url }
            var queryItems = [URLQueryItem]()
            for (key, value) in parameters {
                
                //let item = URLQueryItem(name: key, value: value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                let item = URLQueryItem(name: key, value:value)
                
                queryItems.append(item)
            }
            
            urlComponents.queryItems = queryItems
            
            guard let updatedURL = urlComponents.url else { return url }
            return updatedURL
        }
        
        return url
    }
    
    
    private func getHttpBody() -> Data? {
        
        
        guard let contentType = requestHttpHeaders.value(forKey: "Content-Type") else {
            
            
            return nil
            
        }
        
    
        
        if contentType.contains("application/json") {
            return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [.prettyPrinted, .sortedKeys])
        } else if contentType.contains("application/x-www-form-urlencoded") {
            let bodyString = httpBodyParameters.allValues().map { "\($0)=\(String(describing: $1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)))" }.joined(separator: "&")
            return bodyString.data(using: .utf8)
        } else {
            return httpBody
        }
    }
    
    

    
    
    
}








extension RestHandler {
    
    public enum HTTPMethod: String {
        case options = "OPTIONS"
        case get     = "GET"
        case head    = "HEAD"
        case post    = "POST"
        case put     = "PUT"
        case patch   = "PATCH"
        case delete  = "DELETE"
        case trace   = "TRACE"
        case connect = "CONNECT"
    }
    
    
    
    
    
    
    
    struct RestEntity {
        private var values: [String: String] = [:]
        
       // print(values)
        
        
        mutating func add(value: String, forKey key: String) {
            values[key] = value
        }
        
        func value(forKey key: String) -> String? {
            return values[key]
        }
        
        func allValues() -> [String: String] {
            //print(values)
            return values
        }
        
        func totalItems() -> Int {
            return values.count
        }
    }
    
    
    struct Response {
        var response: URLResponse?
        var httpStatusCode: Int = 0
        var headers = RestEntity()
        
        init(fromURLResponse response: URLResponse?) {
            guard let response = response else { return }
            self.response = response
            httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            
            if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
                for (key, value) in headerFields {
                    headers.add(value: "\(value)", forKey: "\(key)")
                }
            }
        }
    }
    
    
    
    struct Results {
        var data: Data?
        var response: Response?
        var error: Error?
        
        init(withData data: Data?, response: Response?, error: Error?) {
            self.data = data
            self.response = response
            self.error = error
        }
        
        init(withError error: Error) {
            self.error = error
        }
    }
    
    
    
    enum CustomError: Error {
        case failedToCreateRequest
    }

    
    
}

extension RestHandler.CustomError: LocalizedError {
    public var localizedDescription: String {
        switch self {
        case .failedToCreateRequest: return NSLocalizedString("Unable to create the URLRequest object", comment: "")
        }
    }
}
