//
//  Constants.swift
//  ExpensePRO
//
//  Created by Sajin M on 6/12/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import Foundation
import UIKit

let AppTitle:String = "XpensePRO"
let noInternet:String = "No Internet Connection"
let validUsername:String = "Please enter a valid username and password"
let mandatoryFields:String = "Please fill all fields"
let validEmail:String = "Please enter a valid email id"
let validPhone:String = "Please enter a valid Phone Number"
let valideCategoryName:String = "Please enter a valid category name"
let selectCategory:String = "Please select a category"
let nodataFound:String = "No data found!"
let wentWrong = "Oops.. Something Went wrong"
let notValidPhone = "Phone number is not valid"
let passwordMessage = "Password must be at least 6 characters"
let otpNotValid = "OTP is not valid"
let confirmMessage = "Are you sure to delete ?"
let notRegister = "User not registered.Please setup your account to continue"
let companyName = "Company name should not be empty"
let validDate = "Not a valid date range"

let oneSignalId:String = "7b699c5f-c1f3-4136-9b50-c0a8cdfa5fc4"
let RazorPayKey:String =  "rzp_test_tL19ZxZTkyDswT"  //live - rzp_live_Muo2KelbkcuY33

let customerCareEmail:String = "askus@codelattice.com"

let googleUserKey:String = "508238327164-5dmpsm1lqmdbn813hj2thkmiv2jnsmb6.apps.googleusercontent.com"
var filterArray = ["All","Draft","Requested","Reviewed","Approved","Rejected","Paid"]

var menuTitle = ["Manage Notifications","Contact Us","Log Out"]
var menuIcons = ["notification_fill","contact_fill","logout_fill"]

var menuCompany = ["Members","Teams","Category"]
var menuIconComapny = ["user_fill","team_fill","cat_fill"]




//var roleArray = ["Select Role","Team Manager","Team Member","Accountant"]
var roleArray = ["Select Role","Approver","Reviewer","Requester","Accountant"]
var rolesType = ["","supervisor","employee","accountant","reviewer"]

//Custome cell

let ExpenseCell:String = "expenseCell"
let CategoryCell:String = "categoryCell"
let FilterTableCell:String = "filterCell"
let InboxCell:String = "inboxCell"

let blue   = UIColor(hexString: "#3498db")
let red    = UIColor(hexString: "#e74c3c")
let yellow = UIColor(hexString: "#f1c40f")

let Defaults = UserDefaults.standard

let rupee:String = "\u{20B9} "
let UserToken:String = "token"

let BaseUrl:String = "https://api.xpense.pro/api" //"https://staging-api.xpense.pro/api"
let BaseIconUrl:String = "https://api.xpense.pro/icons/"
let imageBaseUrl:String = "https://xpensepro-uploads.s3.ap-south-1.amazonaws.com/"                    //"https://staging-api.xpense.pro/api/image/" //"https://api.xpense.pro/"
let imageUploadUrl:String = "https://xpensepro-uploads.s3.ap-south-1.amazonaws.com/"





enum Api{
    
    static let Login = getApi(apiUrl: "/auth")
    static let Dasboard = getApi(apiUrl: "/user/dashboard")
    static let MyDashboard = getApi(apiUrl: "/user/my-dashboards")
    static let ImbersementList = getApi(apiUrl: "/reimbursements/list?")
    static let TeamList = getApi(apiUrl: "/teams")
    static let TeamRoleList = getApi(apiUrl: "/teams")
    static let Categories = getApi(apiUrl: "/categories/")
    static let Inbox = getApi(apiUrl: "/logs/notifications")
    static let Plans = getApi(apiUrl: "/pricing/plans/")
    static let Roles = getApi(apiUrl: "/teams/roles")
    static let AddTeam = getApi(apiUrl: "/teams")
    static let GetProfile = getApi(apiUrl: "/user/me")
    static let UpdateProfile = getApi(apiUrl: "/user")
    static let updateToken = getApi(apiUrl: "/account/update")
    static let GenerateOTP = getApi(apiUrl: "/otp/generate")
    static let VerifyOTP = getApi(apiUrl: "/otp/verify")
    static let CreateImbersement = getApi(apiUrl: "/reimbursements")
    static let ChangeStatusImbersement = getApi(apiUrl: "/reimbursements/status")
    static let UploadImage = getApi(apiUrl: "/upload/")
    static let InboxArchive = getApi(apiUrl: "/logs/notifications/archive/")
    static let getToken = getApi(apiUrl: "/password/generate-token")
    static let passwordUpdate = getApi(apiUrl: "/password/update-password")
    static let register = getApi(apiUrl: "/account/register")
    static let switchAccount = getApi(apiUrl: "/account/primary")
    static let configure = getApi(apiUrl: "/account/configure")
    static let requests = getApi(apiUrl: "/requests/my-requests")
    static let inviteRequest = getApi(apiUrl: "/requests")
    static let catIcons = getApi(apiUrl: "/icon")
    static let addCategory = getApi(apiUrl: "/categories")
    static let notify = getApi(apiUrl: "/user/update-notify")
    static let signInwithGoogle = getApi(apiUrl: "/user/google")
    static let signInwithApple = getApi(apiUrl: "/user/apple")
    static let profilePic = getApi(apiUrl: "/upload/avatar/")
    static let oraganisation = getApi(apiUrl: "/organisation")
    static let organisationLogo = getApi(apiUrl: "/upload/logo/")
    static let organisationDetails = getApi(apiUrl: "/organisation/")
    static let comments = getApi(apiUrl: "/comments/")
    static let activities = getApi(apiUrl: "/logs/")
    static let removeReceipts = getApi(apiUrl: "/reimbursements/receipt/")
    
   
}

func getApi(apiUrl:String) -> String{
    
    return BaseUrl + apiUrl
    
}

func getIcon(iconName:String) -> String{
    
     return BaseIconUrl + iconName
}
