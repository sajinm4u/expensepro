//
//  ResponseHandler.swift
//  ExpensePRO
//
//  Created by Sajin M on 6/12/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import Foundation
import UIKit




class ResponseHandler {
    
    var loginResponse:LoginModel?
    var MyDashBoardResponse:MyDashBoardModel?
    var MyImbersementListResponse:ImberseListModel?
    var TeamTypeResponse:TypeListModel?
    var CategoryResponse:CategoryModel?
    var InboxResponse:InboxModel?
    var PlanResponse:PlanModel?
    var RolesResponse:RolesModel?
    var UserResponse:UserProfileModel?
    var DashBoardResponse:DashBoardModel?
    var NewTeamResponse:TeamModel?
    var ImbersementAddedResponse:GeneralModel?
    var OtpVerifyResponse:GeneralModel?
    var InvitationRequestResponse:InvitationRequestModel?
    var CategoryIconResponse:icons?
    var ImberseDetailResponse:ImberseModel?
    var RegisterBusinessResponse:RegisterBusiness?
    var MembersResponse:Members?
    var CommentResponse:CommentsModel?
    var ArticleResponse:ArticleModel?
    
    
    public func doLogin(withParameter theParameter:[String : String], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.Login) else { return }
        ApiManager.shared.doLogin(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, loginModel, message) in
            
            if isSuccess == 200 {
                
                self.loginResponse = loginModel
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func LoginWithGoogle(withParameter theParameter:[String : String], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.signInwithGoogle) else { return }
        ApiManager.shared.doLogin(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, loginModel, message) in
            
            if isSuccess == 200 {
                
                self.loginResponse = loginModel
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func LoginWithApple(withParameter theParameter:[String : String], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.signInwithApple) else { return }
        ApiManager.shared.doLogin(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, loginModel, message) in
            
            if isSuccess == 200 {
                
                self.loginResponse = loginModel
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func getMyDashBoard(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.MyDashboard) else { return }
        ApiManager.shared.getMyDashboardData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, MyDashBoardModel, message) in
            
            if isSuccess == 200 {
                
                self.MyDashBoardResponse = MyDashBoardModel
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func getDashBoard(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.Dasboard) else { return }
        ApiManager.shared.getDashboardData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, DashBoardModel, message) in
            
            if isSuccess == 200 {
                
                self.DashBoardResponse = DashBoardModel
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func archiveInbox(id:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.InboxArchive + id) else { return }
           
          
           ApiManager.shared.archiveInboxData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,inboxData,message) in
               
               if isSuccess == 200 {
                   
                   self.InboxResponse = inboxData
                  
                   completion(true, "")
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func getInbox(urlString:String,withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
           ApiManager.shared.getInboxListData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,inboxData,message) in
               
               if isSuccess == 200 {
                   
                   self.InboxResponse = inboxData
                  
                   completion(true, "")
               } else {
                   completion(false, message)
               }
           })
       }
    
    public func getArticles(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: urlString) else { return }
        
       
        ApiManager.shared.getArticleData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,articleData,message) in
            
            if isSuccess == 200 {
                
                self.ArticleResponse = articleData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    public func postComments(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: urlString) else { return }
        
       
        ApiManager.shared.postCommentData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,commentsData,message) in
            
            if isSuccess == 200 {
                
                self.CommentResponse = commentsData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func getComments(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: urlString) else { return }
        
       
        ApiManager.shared.getCommentData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,commentsData,message) in
            
            if isSuccess == 200 {
                
                self.CommentResponse = commentsData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func getPlans(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: urlString) else { return }
        
       
        ApiManager.shared.getPlanData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,planData,message) in
            
            if isSuccess == 200 {
                
                self.PlanResponse = planData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func getUserProfile(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.GetProfile) else { return }
           
          
           ApiManager.shared.getUserProfileData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,userData,message) in
               
               if isSuccess == 200 {
                   
                   self.UserResponse = userData
                   completion(true, "")
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func updateUserProfile(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.UpdateProfile) else { return }
           
          
           ApiManager.shared.updateUserProfileData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                   
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func updateDeviceToken(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.updateToken) else { return }
           
          
           ApiManager.shared.updateUserProfileData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                   
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    
    public func AddNewImbersement(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.CreateImbersement) else { return }
           
          
           ApiManager.shared.NewImberseData(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message,responseData) in
               print(theParameter)
               if isSuccess == 200 {
                   
                  self.ImbersementAddedResponse = responseData
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func ChangePrimary(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.switchAccount) else { return }
           
          
           ApiManager.shared.changeAccountType(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                 // self.ImbersementAddedResponse = responseData
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    public func ChangeAccount(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.switchAccount) else { return }
           
          
           ApiManager.shared.changeAccountType(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                 // self.ImbersementAddedResponse = responseData
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    public func inviteMember(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.inviteRequest) else { return }
           
          
           ApiManager.shared.InviteMember(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                 // self.ImbersementAddedResponse = responseData
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    
    public func UpdateNewImbersement(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
           ApiManager.shared.UpdateImberseData(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message,responseData) in
               print(theParameter)
               if isSuccess == 200 {
                   
                  self.ImbersementAddedResponse = responseData
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    
    public func DeleteReceipt(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
           ApiManager.shared.DeleteImberseData(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                 
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func DeleteNewImbersement(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
           ApiManager.shared.DeleteImberseData(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                 
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    public func ImbersementDetails(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
           ApiManager.shared.DetailImberseData(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,data,message) in
               
               if isSuccess == 200 {
                   
                self.ImberseDetailResponse = data
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    public func updateImbersmentStatus(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
           ApiManager.shared.statusChangeImbersement(fromUrl:url, withParameter:theParameter, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func UploadProfileImage(urlString:String, image:UIImage, completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
        ApiManager.shared.UpdateBillData(fromUrl:url,image:image, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   
                   
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func UploadImbersementImage(urlString:String, image:UIImage, completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: urlString) else { return }
           
          
        ApiManager.shared.UpdateBillData(fromUrl:url,image:image, completion: { (isSuccess,message) in
               
               if isSuccess == 200 {
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    public func ConfigureAnotherAccount(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.configure) else { return }
           
          
           ApiManager.shared.addSecondaryAccount(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,userData,message) in
               
               if isSuccess == 200 {
                   
                self.loginResponse = userData
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    
    public func RegisterBusiness(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.oraganisation) else { return }
           
          
           ApiManager.shared.AddBusinessData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,data,message) in
               
               if isSuccess == 200 {
                   
                self.RegisterBusinessResponse = data
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    public func RegisterUserProfile(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.register) else { return }
           
          
           ApiManager.shared.UserRegistrationData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,userData,message) in
               
               if isSuccess == 200 {
                print(userData)
                self.loginResponse = userData
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }
    
    
    
    
    
    public func SendOtp(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.GenerateOTP) else { return }
           
          
           ApiManager.shared.OtpUpdateData(fromUrl:url,withParameter:theParameter, completion: { (otpData,isSuccess,message) in
               
               if isSuccess == 200 {
                   
                self.OtpVerifyResponse = otpData
                   completion(true,message)
                
               } else {
                self.OtpVerifyResponse = otpData
                   completion(false, message)
               }
           })
       }
    
    
    public func SendOtpToken(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.getToken) else { return }
           
          
           ApiManager.shared.OtpUpdateData(fromUrl:url,withParameter:theParameter, completion: { (otpData,isSuccess,message) in
               
               if isSuccess == 200 {
                   
                self.OtpVerifyResponse = otpData
                   completion(true,message)
                
               } else {
                self.OtpVerifyResponse = otpData
                   completion(false, message)
               }
           })
       }
    
    public func UpdatePasswordToken(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.passwordUpdate) else { return }
           
          
           ApiManager.shared.OtpForgotDataUpdate(fromUrl:url,withParameter:theParameter, completion: { (otpData,isSuccess,message) in
               
               if isSuccess == 200 {
                   
                self.OtpVerifyResponse = otpData
                   completion(true,message)
                
               } else {
                self.OtpVerifyResponse = otpData
                   completion(false, message)
               }
           })
       }
    
    public func EnableNotify(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.notify) else { return }
           
          
           ApiManager.shared.updateSetting(fromUrl:url,withParameter:theParameter, completion: { (otpData,isSuccess,message) in
               
               if isSuccess == 200 {
                   
               
                   completion(true,message)
                
               } else {
               
                   completion(false, message)
               }
           })
       }
    
    
    public func VerifyOtp(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
           
           guard let url = URL(string: Api.VerifyOTP) else { return }
           
          
           ApiManager.shared.OtpUpdateData(fromUrl:url,withParameter:theParameter, completion: { (data,isSuccess,message) in
               
               if isSuccess == 200 {
                   
                self.OtpVerifyResponse = data
                   completion(true,message)
                
               } else {
                   completion(false, message)
               }
           })
       }

    
    
    public func getCategories(withUrl:String,withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: withUrl) else { return }
        
       
        ApiManager.shared.getCategoryData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,categoryData,message) in
            
            if isSuccess == 200 {
                
                self.CategoryResponse = categoryData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func getRolesList(url:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: url) else { return }
        
       
        ApiManager.shared.getRolesListData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, rolesData, message) in
            
            if isSuccess == 200 {
                
                self.RolesResponse = rolesData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func getMembersList(url:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: url) else { return }
        
       
        ApiManager.shared.getMembersListData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, membersData, message) in
            
            if isSuccess == 200 {
                
                self.MembersResponse = membersData?.data
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func addTeams(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.AddTeam) else { return }
        
       
        ApiManager.shared.addTeamData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, teamData, message) in
            
            if isSuccess == 200 {
                
                self.NewTeamResponse = teamData
               
                completion(true, message)
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func addCategories(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.addCategory) else { return }
        
       
        ApiManager.shared.addCategoryName(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,message) in
            
            if isSuccess == 200 {
              
                completion(true, message)
            } else {
                completion(false, message)
            }
        })
    }
    
    public func removeCategories(withUrl:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string:withUrl) else { return }
        
       
        ApiManager.shared.removeCategories(fromUrl:url,withParameter:theParameter, completion: { (isSuccess,message) in
            
            if isSuccess == 200 {
              
                completion(true, message)
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func getTeams(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.AddTeam) else { return }
        
       
        ApiManager.shared.addTeamData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, teamData, message) in
            
            if isSuccess == 200 {
                
                self.NewTeamResponse = teamData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func getInvitationRequests(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.requests) else { return }
        
       
        ApiManager.shared.getRequestsData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, requestData, message) in
            
            if isSuccess == 200 {
                
                self.InvitationRequestResponse = requestData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    public func JoinRequests(requestId:String,withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        let urlStr = Api.inviteRequest + "/" + requestId
        
        guard let url = URL(string: urlStr ) else { return }
        
       
        ApiManager.shared.joinRequestsData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, requestData, message) in
            
            if isSuccess == 200 {
                
                completion(true, requestData?.message ?? "Joined Successfully")
            } else {
                completion(false, message)
            }
        })
    }
    
    public func getAllInvitationRequests(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.inviteRequest) else { return }
        
       
        ApiManager.shared.getRequestsData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, requestData, message) in
            
            if isSuccess == 200 {
                
                self.InvitationRequestResponse = requestData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    public func getTypeList(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.TeamList) else { return }
        
       
        ApiManager.shared.getTypeListData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, typeData, message) in
            
            if isSuccess == 200 {
                
                self.TeamTypeResponse = typeData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
     
    
    public func getTeamRoleList(url:String,withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string:url) else { return }
        
       
        ApiManager.shared.getTypeListData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, typeData, message) in
            
            if isSuccess == 200 {
                
                self.TeamTypeResponse = typeData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
    
    public func getCatIcons(withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
        guard let url = URL(string: Api.catIcons) else { return }
        
       
        ApiManager.shared.getCatIconsData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, typeData, message) in
            
            if isSuccess == 200 {
                
                self.CategoryIconResponse = typeData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
//    public func getSingleImbersment(urlString:String, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
//        
//        guard let url = URL(string: urlString) else { return }
//        
//       
//        ApiManager.shared.getImbersementData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, ImberseData, message) in
//            
//            if isSuccess == 200 {
//                
//                self.MyImbersementListResponse = ImberseData
//               
//                completion(true, "")
//            } else {
//                completion(false, message)
//            }
//        })
//    }
    
    public func getImbersmentList(url:URL, withParameter theParameter:[String : Any], completion: @escaping ((Bool, String) -> Void)) {
        
       // guard let url = url else { return }
        
       
        ApiManager.shared.getImbersementData(fromUrl:url,withParameter:theParameter, completion: { (isSuccess, ImberseData, message) in
            
            if isSuccess == 200 {
                
                self.MyImbersementListResponse = ImberseData
               
                completion(true, "")
            } else {
                completion(false, message)
            }
        })
    }
    
    
}





