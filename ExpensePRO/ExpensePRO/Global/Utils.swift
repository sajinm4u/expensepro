//
//  Utils.swift
//  ExpensePRO
//
//  Created by Sajin M on 14/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation
import UIKit

public enum Severity{
    
    case Draft
    case Requested
    case Saved
  
}


class Utils {
    
    
    static func getImage(id:String) -> String{
        
        return imageBaseUrl+id
        
    }
    
    static func role(type:String) -> String {
        
        switch type {
        case "supervisor":
            return "Approver"
        case "accountant":
            return "Accountant"
        case "employee":
             return "Requester"
        case "Employee":
             return "Requester"
        case "reviewer":
             return "Reviewer"
         
        default:
            break
        }
        return type
    }
    
    static func setType(type:String) -> UIColor{
        
        switch type {
            
        case "Draft":
            return UIColor(red: 98/255, green: 141/255, blue: 186/255, alpha: 1)
        case "Requested":
            return UIColor(red: 255/255, green: 204/255, blue: 0/255, alpha: 1)
        case "Saved":
             return UIColor(red: 103/255, green: 58/255, blue: 183/255, alpha: 1)
        case "Reviewed":
             return UIColor(red: 175/255, green: 82/255, blue: 122/255, alpha: 1)
        case "Approved":
               return UIColor(red: 52/255, green: 199/255, blue: 89/255, alpha: 1)
        case "Rejected":
              return UIColor(red: 254/255, green: 77/255, blue: 71/255, alpha: 1)
        case "Paid":
              return UIColor(red: 26/255, green: 150/255, blue: 246/255, alpha: 1)
        case "Personal":
              return UIColor(red: 103/255, green: 58/255, blue: 183/255, alpha: 1)
        case "All":
              return UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1)
        
            
            
            
            
        default:
            return UIColor(red: 98 / 255, green: 141 / 255, blue: 186 / 255, alpha: 1)
        }
    }
    
}
