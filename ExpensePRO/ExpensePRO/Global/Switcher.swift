//
//  Switcher.swift
//  ExpensePRO
//
//  Created by Sajin M on 13/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//


import Foundation
import UIKit

class Switcher {
    
    static func updateRootVC(){
    var rootVC : UIViewController?
    let status = Defaults.bool(forKey: "splashSkip")
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if(status != nil){
            
            
            if !status{
                
                
                 rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "splashLaunch") as! UINavigationController
                appDelegate.window?.rootViewController = rootVC

                
            }else{
                
                
                if Defaults.string(forKey:UserToken) != nil{
                        
                    
                            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                    
                                   
                }  else{
                    
                     rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "initialNavigation") as! UINavigationController
                    
            }
                
                
                
                
                
            }
            

        }else{
        
       
             
            if Defaults.string(forKey:UserToken) != nil{
                    
                
                        rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                
                               
            }  else{
                
                 rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "initialNavigation") as! UINavigationController
                
        }
        }
             
      
        appDelegate.window?.rootViewController = rootVC

    
}

}
