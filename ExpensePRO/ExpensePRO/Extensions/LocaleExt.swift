//
//  LocaleExt.swift
//  ExpensePRO
//
//  Created by Sajin M on 01/10/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation
import UIKit

extension Locale {
    static let currency: [String: (code: String?, symbol: String?)] = Locale.isoRegionCodes.reduce(into: [:]) {
        let locale = Locale(identifier: Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: $1]))
        $0[$1] = (locale.currencyCode, locale.currencySymbol)
    }
    
    
   
}
