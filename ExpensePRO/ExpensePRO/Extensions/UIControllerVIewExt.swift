//
//  UIControllerVIewExt.swift
//  ExpensePRO
//
//  Created by Sajin M on 03/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{

    func dismissKey()
    {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard()
    {
    view.endEditing(true)
    }
    
    
    func backNavigation() {
        self.navigationController?.popViewController(animated:true)
    }
    
}
