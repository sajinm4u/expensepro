//
//  DateExtension.swift
//  ExpensePRO
//
//  Created by Sajin M on 21/09/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation


extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
   

        func toString(format: String = "yyyy-MM-dd") -> String {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.dateFormat = format
            return formatter.string(from: self)
        }
        

}
